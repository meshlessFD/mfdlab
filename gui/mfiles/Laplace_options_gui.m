
classdef Laplace_options_gui < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                       matlab.ui.Figure
        ProblemPanel                   matlab.ui.container.Panel
        dimSpinnerLabel                matlab.ui.control.Label
        dimSpinner                     matlab.ui.control.Spinner
        domainDropDownLabel            matlab.ui.control.Label
        domainDropDown                 matlab.ui.control.DropDown
        BCDropDownLabel                matlab.ui.control.Label
        BCDropDown                     matlab.ui.control.DropDown
        problemDropDownLabel           matlab.ui.control.Label
        problemDropDown                matlab.ui.control.DropDown
        cDropDownLabel                 matlab.ui.control.Label
        cDropDown                      matlab.ui.control.DropDown
        NodesPanel                     matlab.ui.container.Panel
        distmeshparametersPanel        matlab.ui.container.Panel
        powDMLabel                     matlab.ui.control.Label
        powDMEditField                 matlab.ui.control.NumericEditField
        mfDMLabel                      matlab.ui.control.Label
        mfDMEditField                  matlab.ui.control.NumericEditField
        maxiterDMEditFieldLabel        matlab.ui.control.Label
        maxiterDMEditField             matlab.ui.control.NumericEditField
        typeDMDropDown                 matlab.ui.control.DropDown
        spacingEditFieldLabel          matlab.ui.control.Label
        spacing_nodesEditField         matlab.ui.control.NumericEditField
        interiorDropDownLabel          matlab.ui.control.Label
        interiorDropDown               matlab.ui.control.DropDown
        boundaryDropDownLabel          matlab.ui.control.Label
        boundaryDropDown               matlab.ui.control.DropDown
        refineSpinnerLabel             matlab.ui.control.Label
        refineSpinner                  matlab.ui.control.Spinner
        DiffMatrixTabGroup             matlab.ui.container.TabGroup
        LaplaceTab                     matlab.ui.container.Tab
        LsetsofInfluencePanel          matlab.ui.container.Panel
        LsizeSISpinnerLabel            matlab.ui.control.Label
        LsizeSISpinner                 matlab.ui.control.Spinner
        LselectionmethodDropDownLabel  matlab.ui.control.Label
        LselectionmethodDropDown       matlab.ui.control.DropDown
        Lsel_orderSpinnerLabel         matlab.ui.control.Label
        Lsel_orderSpinner              matlab.ui.control.Spinner
        Lsel_muLabel                   matlab.ui.control.Label
        Lsel_muEditField               matlab.ui.control.NumericEditField
        LuseweightsButton              matlab.ui.control.StateButton
        LinitcloudSpinnerLabel         matlab.ui.control.Label
        LinitcloudSpinner              matlab.ui.control.Spinner
        LtrypositiveButton             matlab.ui.control.StateButton
        LweightsPanel                  matlab.ui.container.Panel
        LwtypeLabel                    matlab.ui.control.Label
        LwtypeDropDown                 matlab.ui.control.DropDown
        LporderSpinnerLabel            matlab.ui.control.Label
        LporderSpinner                 matlab.ui.control.Spinner
        LrbfepLabel                    matlab.ui.control.Label
        LrbfepEditField                matlab.ui.control.NumericEditField
        LrbfpowLabel                   matlab.ui.control.Label
        LrbfpowEditField               matlab.ui.control.NumericEditField
        LpolymuLabel                   matlab.ui.control.Label
        LpolymuEditField               matlab.ui.control.NumericEditField
        LwenddSpinnerLabel             matlab.ui.control.Label
        LwenddSpinner                  matlab.ui.control.Spinner
        LwendkSpinnerLabel             matlab.ui.control.Label
        LwendkSpinner                  matlab.ui.control.Spinner
        LfdstarongridsButton           matlab.ui.control.StateButton
        NeumannTab                     matlab.ui.container.Tab
        NweightsPanel                  matlab.ui.container.Panel
        NwtypeLabel                    matlab.ui.control.Label
        NwtypeDropDown                 matlab.ui.control.DropDown
        NporderSpinnerLabel            matlab.ui.control.Label
        NporderSpinner                 matlab.ui.control.Spinner
        NrbfepLabel                    matlab.ui.control.Label
        NrbfepEditField                matlab.ui.control.NumericEditField
        NrbfpowLabel                   matlab.ui.control.Label
        NrbfpowEditField               matlab.ui.control.NumericEditField
        NpolymuLabel                   matlab.ui.control.Label
        NpolymuEditField               matlab.ui.control.NumericEditField
        NwenddSpinnerLabel             matlab.ui.control.Label
        NwenddSpinner                  matlab.ui.control.Spinner
        NwendkSpinnerLabel             matlab.ui.control.Label
        NwendkSpinner                  matlab.ui.control.Spinner
        NsetsofInfluencePanel          matlab.ui.container.Panel
        NsizeSISpinnerLabel            matlab.ui.control.Label
        NsizeSISpinner                 matlab.ui.control.Spinner
        NselectionmethodDropDownLabel  matlab.ui.control.Label
        NselectionmethodDropDown       matlab.ui.control.DropDown
        Nsel_orderSpinnerLabel         matlab.ui.control.Label
        Nsel_orderSpinner              matlab.ui.control.Spinner
        Nsel_muLabel                   matlab.ui.control.Label
        Nsel_muEditField               matlab.ui.control.NumericEditField
        NuseweightsButton              matlab.ui.control.StateButton
        NinitcloudSpinnerLabel         matlab.ui.control.Label
        NinitcloudSpinner              matlab.ui.control.Spinner
        DifferentiationMatrixLabel     matlab.ui.control.Label
    end

    % App Designer file Laplace_options_gui.mlapp: setup of options to run script_mFD_Laplace.m. 
    % This file is called from mFD_gui.mlapp
    %
    % Copyright (C) 2017-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
    %
    % This file is part of mFDlab released under GNU General Public License v2.0.
    % See the file README for further details.
    
    
    
    
    %user properties
    properties (Access = private)   
        MainApp % main app (set in startupFcn)
    end

    methods (Access = private)         
        
        
        %set/update the menus for boundary nodes
        function set_nodestype_bnd_options(app)
            
            %take into account current domain
            switch app.domainDropDown.Value
%                 case 'unit cube'
%                     app.boundaryDropDown.Items = {'grid'};
                case {'unit cube','unit ball','ball\ball','adaptmesh'}
                    if app.dimSpinner.Value == 2 %special for dimension 2
                        if ismember(app.domainDropDown.Value,{'unit ball','ball\ball'})
                            app.boundaryDropDown.Items = {'projection','isosurface','uniform'};
                        else
                            app.boundaryDropDown.Items = {'projection','isosurface'};
                        end
                    else
                        app.boundaryDropDown.Items = {'projection'};
                    end
                    %app.boundaryDropDown.Value = app.boundaryDropDown.Items(1);                    
            end
            
            %take into account current method for interior nodes
            switch app.interiorDropDown.Value
                case 'grid'
                    if strcmp( app.domainDropDown.Value,'unit cube')
                        app.boundaryDropDown.Items = {'grid'};
                    end
                case 'distmesh' %assuming app.dimSpinner.Value == 2
                                %since otherwise 'distmesh' would have not be selected
                    app.boundaryDropDown.Items = [app.boundaryDropDown.Items,'distmesh']; 
                    app.boundaryDropDown.Value = 'distmesh';
            end
            
            
        end
        
        %update the menu for interior nodes and set default values for given dim and problem
        function set_nodestype_int_options(app)
            
            app.interiorDropDown.Items = {'grid', 'Halton', 'Sobol', 'random'};
            if app.dimSpinner.Value == 2
                %app.interiorDropDown.Items = [app.interiorDropDown.Items,'distmesh'];
                app.interiorDropDown.Items = {'grid', 'hexgrid', 'Halton', 'Sobol', 'random','distmesh'};
            end
            
            
            
            
%             switch app.domainDropDown.Value
%                 case 'unit cube'
%                     app.interiorDropDown.Items = {'grid'};
%                     %app.interiorDropDown.Value = 'grid';
%                     if app.dimSpinner.Value == 2
%                         app.interiorDropDown.Items = [app.interiorDropDown.Items,'distmesh'];
%                     end                   
%                 case {'unit ball','adaptmesh'}
%                     app.interiorDropDown.Items = {'grid', 'Halton', 'Sobol', 'random'};
%                     if app.dimSpinner.Value == 2
%                         app.interiorDropDown.Items = [app.interiorDropDown.Items,'distmesh'];
%                     end
%                    
%             end
%             
%             %switch on/off distmesh parameters panel and add special options for distmesh
%             switch app.interiorDropDown.Value
%                 case 'distmesh'
%                     app.distmeshparametersPanel.Visible = 'on';
%                     switch app.problemDropDown.Value
%                         case 'adaptmesh'
%                             app.typeDMDropDown.Items = {'uniform','boundary','feature'};
%                         otherwise
%                             app.typeDMDropDown.Items = {'uniform','boundary'};
%                     end
%                 otherwise
%                     app.distmeshparametersPanel.Visible = 'off';
%             end
            
            interiorDropDownValueChanged(app,[])
            
        end
        
    end

    methods (Access = public)
    
        function start(app)
            
            %% 1) setup of problem
            
            %set the number of variables
            assignin('base','dim',app.dimSpinner.Value); 
            
            %set the domain
            assignin('base','domain_id',app.domainDropDown.Value);
            
            %set the problem
            assignin('base','problem_id',app.problemDropDown.Value);
            
            %set the value of the coefficient c
            try
                ccoef = eval(app.cDropDown.Value);
            catch %exception
                warning('c coefficient in wrong format')
                disp('computation aborted')
                return;
            end            
            assignin('base','ccoef',ccoef);
            
            %type of boundary conditions
            assignin('base','bctype',app.BCDropDown.Value);
            
            %% 2) setup of node generation
            
            %set method for interior node generation
            assignin('base','nodestype_int',app.interiorDropDown.Value);    
            %set method for boundary node generation
            assignin('base','nodestype_bnd',app.boundaryDropDown.Value);               
            %set target spacing of nodes
            assignin('base','spacing',app.spacing_nodesEditField.Value);            
            %set number of refinements
            assignin('base','nref',app.refineSpinner.Value);
            
            %read parameters for distmesh method if it is currently used
            if strcmp(app.interiorDropDown.Value,'distmesh')
                assignin('base','dm_maxiter',app.maxiterDMEditField.Value);
                assignin('base','dm_type',app.typeDMDropDown.Value);
                if ~strcmp(app.typeDMDropDown.Value,'uniform')
                    assignin('base','dm_pow',app.powDMEditField.Value);
                    assignin('base','dm_mf',app.mfDMEditField.Value);
                end
                
                
                
            end
            
            %% 3) setup of stencil computation for Laplacian
            assignin('base','Lstencil_type','simple');
            if app.LfdstarongridsButton.Value
                fd_on_grid = 'fd_star';
            else
                fd_on_grid = [];
            end
            assignin('base','fd_on_grid',fd_on_grid);
            assignin('base','Luse_selection_weights',app.LuseweightsButton.Value);
            assignin('base','Ltry_positive_stencil',app.LtrypositiveButton.Value);
            
            %% 3a) setup of sets of influence for Laplacian
            
            %set the size of initial cloud of nearest nodes chosen in advance
            if strcmp(app.LselectionmethodDropDown.Value,'nearest')
                assignin('base','Linitcloud_size',app.LsizeSISpinner.Value);
            else
                assignin('base','Linitcloud_size',app.LinitcloudSpinner.Value);
            end
            %set the size of the set of influence
            assignin('base','LsizeSI',app.LsizeSISpinner.Value);
            assignin('base','Lselection_method',app.LselectionmethodDropDown.Value);
            if ismember(app.LselectionmethodDropDown.Value,{'L1 selection'})
                assignin('base','Lsel_order',app.Lsel_orderSpinner.Value);
                assignin('base','Lsel_mu',app.Lsel_muEditField.Value);
            end
            
            %% 3b) setup of stencil weights computation for Laplacian
            
            if ~app.LuseweightsButton.Value
                
                %assigns to the base the variables rbftype, porder, ep
                assignin('base','Lwtype',app.LwtypeDropDown.Value);
                assignin('base','Lporder',app.LporderSpinner.Value);
                assignin('base','Lrbfep',app.LrbfepEditField.Value);
                assignin('base','Lmu',app.LpolymuEditField.Value);
                
                %assign rbfpar
                switch app.LwtypeDropDown.Value
                    case {'Power/TPS', 'Matern', 'MQ', 'IMQ'}
                        assignin('base','Lrbfpar',app.LrbfpowEditField.Value);
                    case {'Wendland'}
                        assignin('base','Lrbfpar',[app.LwenddSpinner.Value app.LwendkSpinner.Value]);
                    otherwise
                        assignin('base','Lrbfpar',[]);
                end
                
            end

            
            %% 4) setup of stencil computation for Neumann boundary conditions
            if ~isempty(app.NeumannTab.Title) %this checks that Neumann condition is needed
                
                assignin('base','Nstencil_type','simple'); %only simple type in the moment
                assignin('base','Nuse_selection_weights',app.NuseweightsButton.Value);    
                
                %% 4a) setup of sets of influence for Neumann boundary conditions
                
                %set the size of initial cloud of nearest nodes chosen in advance
                if strcmp(app.NselectionmethodDropDown.Value,'nearest')
                    assignin('base','Ninitcloud_size',app.NsizeSISpinner.Value);
                else
                    assignin('base','Ninitcloud_size',app.NinitcloudSpinner.Value);
                end
                %set the size of the set of influence
                assignin('base','NsizeSI',app.NsizeSISpinner.Value);
                assignin('base','Nselection_method',app.NselectionmethodDropDown.Value);
                if strcmp(app.NselectionmethodDropDown.Value,'L1 selection') 
                    assignin('base','Nsel_order',app.Nsel_orderSpinner.Value);
                    assignin('base','Nsel_mu',app.Nsel_muEditField.Value);
                end
                
                %% 4b) setup of stencil weights computation for Neumann boundary conditions
                
                if ~app.NuseweightsButton.Value
                    
                    %assigns to the base the variables rbftype, porder, ep
                    assignin('base','Nwtype',app.NwtypeDropDown.Value);
                    assignin('base','Nporder',app.NporderSpinner.Value);
                    assignin('base','Nrbfep',app.NrbfepEditField.Value);
                    assignin('base','Nmu',app.NpolymuEditField.Value);
                    
                    %assign rbfpar
                    switch app.NwtypeDropDown.Value
                        case {'Power/TPS', 'Matern', 'MQ', 'IMQ'}
                            assignin('base','Nrbfpar',app.NrbfpowEditField.Value);
                        case {'Wendland'}
                            assignin('base','Nrbfpar',[app.NwenddSpinner.Value app.NwendkSpinner.Value]);
                        otherwise
                            assignin('base','Nrbfpar',[]);
                    end
                    
                end
            end
            
            %% 5) launch the script
            evalin('base','script_mFD_Laplace')            
            
        end
        
        
    end


    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app, main_app, position)
            app.MainApp = main_app; %to get access to data and public functions of the main app
            app.UIFigure.Position(1) = position(1)+position(3)+50;
            get_options_app(main_app,app); %send options app to the main app 
                                           %by calling the public function of the main app
        end

        % Close request function: UIFigure
        function UIFigureCloseRequest(app, event)
            backup_on_close_request(app.MainApp,app); %calling public function of mFD_gui
            delete(app);

        end

        % Value changed function: LwtypeDropDown
        function LwtypeDropDownValueChanged(app, event)
            value = app.LwtypeDropDown.Value;

            wtypeChanged(app.MainApp,value,...
                                               app.LrbfepEditField,app.LrbfepLabel,...
                                               app.LrbfpowEditField,app.LrbfpowLabel,...
                                               app.LwenddSpinner,app.LwenddSpinnerLabel,...
                                               app.LwendkSpinner,app.LwendkSpinnerLabel,...
                                               app.LpolymuEditField,app.LpolymuLabel,...
                                               app.LporderSpinner,app.LporderSpinnerLabel);  
                                           
           %adjust polynomial order lower limit for Laplacian in the case of purely 
           %polynomial methods
           switch value
                case {'L1','L2'}    
                   app.LporderSpinner.Limits(1) = 3; 
           end
                                               
        end

        % Value changed function: LrbfpowEditField
        function LrbfpowEditFieldValueChanged(app, event)
            %value = app.LrbfpowEditField.Value;
            rbfpow = app.LrbfpowEditField.Value;
            
            %make sure that power/tps and MQ get porder at least their minimum cpd order
            if strcmp(app.LwtypeDropDown.Value,'Power/TPS') 
                app.LporderSpinner.Limits(1) = floor(rbfpow/2)+1;
            elseif strcmp(app.LwtypeDropDown.Value,'MQ')
                if rbfpow <= 1
                    app.LporderSpinner.Limits(1) = 0;
                else
                    app.LporderSpinner.Limits(1) = floor(rbfpow/2)+1;
                end               
            end
            
        end

        % Value changed function: dimSpinner
        function dimSpinnerValueChanged(app, event)

            %initiate update of problem list by problem(s) available in any dimensions
            app.problemDropDown.Items =  {'u = prod(sin(pi*x),2)','u = sin(10*sum(x,2))'};
            
            switch app.dimSpinner.Value
                case 1
                    %update domain list
                    app.domainDropDown.Items = {'unit cube'};
                    app.domainDropDown.Value = 'unit cube';
                    %Lwtype option 'Gauss-QR' is not available
                    app.LwtypeDropDown.Items = {'Power/TPS', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};
                    app.NwtypeDropDown.Items = {'Power/TPS', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};                    
                   
                case 2
                    %add specific 2D problems
                    app.problemDropDown.Items = [app.problemDropDown.Items,'adaptmesh'];
                    %update domain list (domains for the default problem 'u = prod(sin(pi*x),2)')
                    app.domainDropDown.Items = {'unit cube', 'unit ball','ball\ball','adaptmesh'};
                    %all Lwtype options are available including 'Gauss-QR'
                    app.LwtypeDropDown.Items = {'Power/TPS', 'Gauss-QR', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};
                    app.NwtypeDropDown.Items = {'Power/TPS', 'Gauss-QR', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};                    
                otherwise
                    %update domain list
                    app.domainDropDown.Items = {'unit cube', 'unit ball','ball\ball'};
                    %Lwtype option 'Gauss-QR' is not available
                    app.LwtypeDropDown.Items = {'Power/TPS', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};
                    app.NwtypeDropDown.Items = {'Power/TPS', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};                    
                   
            end
            
            %update the menus because Lwtype and Nwtype may have changed
            LwtypeDropDownValueChanged(app, []);
            NwtypeDropDownValueChanged(app, []);            
            
            %enable c
            app.cDropDown.Value = '0';
            app.cDropDown.Enable = 'on';
            
           
            set_nodestype_int_options(app);  
            %set_nodestype_bnd_options(app);  
            
                       
            %run further callbacks that depend on dim
            Lsel_orderSpinnerValueChanged(app, event);
            Nsel_orderSpinnerValueChanged(app, event);
            
        end

        % Value changed function: interiorDropDown
        function interiorDropDownValueChanged(app, event)
            value = app.interiorDropDown.Value;
            %switch on/off distmesh parameters panel and add special options for distmesh
            switch value
                case 'distmesh'
                    app.distmeshparametersPanel.Visible = 'on';
                    switch app.problemDropDown.Value
                        case 'adaptmesh'
                            app.typeDMDropDown.Items = {'uniform','boundary','feature'};
                        otherwise
                            app.typeDMDropDown.Items = {'uniform','boundary'};
                    end 
                    %callback executed because when we switch back to 'adaptmesh' after using it previously with 'feature',
                    %app.typeDMDropDown.Value changes into 'uniform'
                    typeDMDropDownValueChanged(app, event);
                otherwise
                    app.distmeshparametersPanel.Visible = 'off';
            end
            
            %make "fd star on grids" option active depending on whether we have a grid
            if strcmp(value,'grid')
                app.LfdstarongridsButton.Visible = 'on';                
            else
                app.LfdstarongridsButton.Visible = 'off'; 
            end
            
            %update boundary options
            set_nodestype_bnd_options(app);  
        end

        % Value changed function: domainDropDown
        function domainDropDownValueChanged(app, event)
           
            %update the menu for interior nodes
            set_nodestype_int_options(app);
            
            %update the menu for boundary nodes
            %set_nodestype_bnd_options(app);
            
            %set some default values in the menus
            switch app.domainDropDown.Value
                case 'unit cube'
                    app.spacing_nodesEditField.Value = 0.2;
                    app.BCDropDown.Items = {'Dirichlet','Neumann'};
                case 'unit ball'
                    app.spacing_nodesEditField.Value = 0.4;
                    app.BCDropDown.Items = {'Dirichlet','Neumann'};
                case 'ball\ball'
                    app.spacing_nodesEditField.Value = 0.4;
                    app.BCDropDown.Items = {'Dirichlet','Neumann','mixed'};
                    app.BCDropDown.Value = 'mixed';
                case 'adaptmesh' %here adaptmesh domian to be used with othe problems than 'adaptmesh'
                    app.spacing_nodesEditField.Value = 0.2;
                    app.BCDropDown.Items = {'Dirichlet','Neumann'};
            end
            %run this callback because we changed BCDropDownValue
            BCDropDownValueChanged(app, event);
            
            
        end

        % Value changed function: problemDropDown
        function problemDropDownValueChanged(app, event)
            value = app.problemDropDown.Value;

            switch value
                case 'adaptmesh'
                    %fix c value
                    app.cDropDown.Value = '0';
                    app.cDropDown.Enable = 'off';
                    %update domain list
                    app.domainDropDown.Items = {'adaptmesh'};
                    app.domainDropDown.Enable = 'off';
                    %update boundary conditions list
                    app.BCDropDown.Items = {'Dirichlet'};
                    app.BCDropDown.Enable = 'off';
                    %update nodes options because we have changed domain settings
                    set_nodestype_int_options(app);
                    %set_nodestype_bnd_options(app);  
                    
                otherwise %generic problems like 'u = prod(sin(pi*x),2)'
                    %enable c
                    app.cDropDown.Value = '0';
                    app.cDropDown.Enable = 'on';                    
                    %update domain list
                    app.domainDropDown.Items = {'unit cube', 'unit ball','ball\ball'};
                    if app.dimSpinner.Value == 2
                        app.domainDropDown.Items = [app.domainDropDown.Items,'adaptmesh'];
                    end
                    app.domainDropDown.Enable = 'on';
                    %update menus depending on domain (boundary conditions list, etc.)
                    domainDropDownValueChanged(app, event);
                    app.BCDropDown.Enable = 'on';
            end
 
                   
            
        end

        % Value changed function: typeDMDropDown
        function typeDMDropDownValueChanged(app, event)
            value = app.typeDMDropDown.Value;
            
            %visibility of the pow and mf fields 
            switch value
                case 'uniform'
                    app.powDMLabel.Visible = 'off';
                    app.powDMEditField.Visible = 'off';
                    app.mfDMLabel.Visible = 'off';
                    app.mfDMEditField.Visible = 'off';
                    
                otherwise
                    app.powDMLabel.Visible = 'on';
                    app.powDMEditField.Visible = 'on';
                    app.mfDMLabel.Visible = 'on';
                    app.mfDMEditField.Visible = 'on';
                    
            end
            
        end

        % Value changed function: LselectionmethodDropDown
        function LselectionmethodDropDownValueChanged(app, event)
            value = app.LselectionmethodDropDown.Value;
            
            %call a public function in main app
            selectionmethodChanged(app.MainApp,app,'L',value);
            
            %callback to turn on/off the weights panel and "try positive" button
            LuseweightsButtonValueChanged(app, event);
        end

        % Value changed function: NselectionmethodDropDown
        function NselectionmethodDropDownValueChanged(app, event)
            value = app.NselectionmethodDropDown.Value;
            
            %call a public function in main app
            selectionmethodChanged(app.MainApp,app,'N',value);

            %callback to turn on/off the weights panel 
            NuseweightsButtonValueChanged(app, event);
        end

        % Value changed function: NwtypeDropDown
        function NwtypeDropDownValueChanged(app, event)
            value = app.NwtypeDropDown.Value;
            
            wtypeChanged(app.MainApp,value,...
                                               app.NrbfepEditField,app.NrbfepLabel,...
                                               app.NrbfpowEditField,app.NrbfpowLabel,...
                                               app.NwenddSpinner,app.NwenddSpinnerLabel,...
                                               app.NwendkSpinner,app.NwendkSpinnerLabel,...
                                               app.NpolymuEditField,app.NpolymuLabel,...
                                               app.NporderSpinner,app.NporderSpinnerLabel);  
                                           
           %adjust polynomial order lower limit for Nuemann boundary condition in the case of purely 
           %polynomial methods
           switch value
                case {'L1','L2'}    
                   app.NporderSpinner.Limits(1) = 2; 
           end

        end

        % Value changed function: NrbfpowEditField
        function NrbfpowEditFieldValueChanged(app, event)
            rbfpow = app.NrbfpowEditField.Value;
            
            %make sure that power/tps and MQ get porder at least their minimum cpd order
            if strcmp(app.LwtypeDropDown.Value,'Power/TPS') 
                app.NporderSpinner.Limits(1) = floor(rbfpow/2)+1;
            elseif strcmp(app.LwtypeDropDown.Value,'MQ')
                if rbfpow <= 1
                    app.NporderSpinner.Limits(1) = 0;
                else
                    app.NporderSpinner.Limits(1) = floor(rbfpow/2)+1;
                end               
            end
 
        end

        % Value changed function: BCDropDown
        function BCDropDownValueChanged(app, event)
            value = app.BCDropDown.Value;
            switch value
                case 'Dirichlet'
                    app.NeumannTab.Title = '';
                    app.NsetsofInfluencePanel.Visible = 'off';
                    app.NweightsPanel.Visible = 'off';
                case {'Neumann','mixed'}
                    app.NeumannTab.Title = 'Neumann';
                    app.NsetsofInfluencePanel.Visible = 'on';
                    app.NweightsPanel.Visible = 'on';
                    
            end
        end

        % Value changed function: LuseweightsButton
        function LuseweightsButtonValueChanged(app, event)
            value = app.LuseweightsButton.Value;
            if value
                app.LweightsPanel.Visible = 'off';
                if strcmp(app.LselectionmethodDropDown.Value,'L1 selection') && app.Lsel_orderSpinner.Value < 5
                    app.LtrypositiveButton.Visible = 'on';
                else
                    app.LtrypositiveButton.Visible = 'off';
                    app.LtrypositiveButton.Value = false;
                end
            else
                app.LweightsPanel.Visible = 'on';
                app.LtrypositiveButton.Visible = 'off';
                app.LtrypositiveButton.Value = false;
            end
            
        end

        % Value changed function: Lsel_orderSpinner
        function Lsel_orderSpinnerValueChanged(app, event)
            value = app.Lsel_orderSpinner.Value;
            dim = app.dimSpinner.Value; %dimension of the space
            
            %set default value for mu
            app.Lsel_muEditField.Value = value;
            
            %set default value for init. cloud
            %(twice the dimension of polynomials of this order)
            app.LinitcloudSpinner.Value = 2*nchoosek(dim+value-1,dim);
            
            %refresh "use weights" button
            LuseweightsButtonValueChanged(app, event);
            
        end

        % Value changed function: Nsel_orderSpinner
        function Nsel_orderSpinnerValueChanged(app, event)
            value = app.Nsel_orderSpinner.Value;
            dim = app.dimSpinner.Value; %dimension of the space
            
            %set default value for mu
            app.Nsel_muEditField.Value = value;
            
            %set default value for init. cloud
            %(twice the dimension of polynomials of this order)
            app.NinitcloudSpinner.Value = 2*nchoosek(dim+value-1,dim);

        end

        % Value changed function: NuseweightsButton
        function NuseweightsButtonValueChanged(app, event)
            value = app.NuseweightsButton.Value;

            if value
                app.NweightsPanel.Visible = 'off';
            else
                app.NweightsPanel.Visible = 'on';
            end
        end

        % Value changed function: NporderSpinner
        function NporderSpinnerValueChanged(app, event)
            value = app.NporderSpinner.Value;

            %set default value for mu
            app.NpolymuEditField.Value = value;
        end

        % Value changed function: LporderSpinner
        function LporderSpinnerValueChanged(app, event)
            value = app.LporderSpinner.Value;
            
            %set default value for mu
            app.LpolymuEditField.Value = value;
        end
    end

    % App initialization and construction
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure
            app.UIFigure = uifigure;
            app.UIFigure.Position = [100 100 571 662];
            app.UIFigure.Name = 'Laplace Options';
            app.UIFigure.CloseRequestFcn = createCallbackFcn(app, @UIFigureCloseRequest, true);

            % Create ProblemPanel
            app.ProblemPanel = uipanel(app.UIFigure);
            app.ProblemPanel.Title = 'Problem    -Delta u + c u = f';
            app.ProblemPanel.FontWeight = 'bold';
            app.ProblemPanel.Position = [33 490 459 144];

            % Create dimSpinnerLabel
            app.dimSpinnerLabel = uilabel(app.ProblemPanel);
            app.dimSpinnerLabel.HorizontalAlignment = 'right';
            app.dimSpinnerLabel.Position = [18 99 26 15];
            app.dimSpinnerLabel.Text = 'dim';

            % Create dimSpinner
            app.dimSpinner = uispinner(app.ProblemPanel);
            app.dimSpinner.Limits = [1 5];
            app.dimSpinner.ValueChangedFcn = createCallbackFcn(app, @dimSpinnerValueChanged, true);
            app.dimSpinner.Position = [59 95 52 22];
            app.dimSpinner.Value = 2;

            % Create domainDropDownLabel
            app.domainDropDownLabel = uilabel(app.ProblemPanel);
            app.domainDropDownLabel.HorizontalAlignment = 'right';
            app.domainDropDownLabel.Position = [18 65 47 15];
            app.domainDropDownLabel.Text = 'domain';

            % Create domainDropDown
            app.domainDropDown = uidropdown(app.ProblemPanel);
            app.domainDropDown.Items = {'unit cube', 'unit ball', 'ball\ball', 'adaptmesh'};
            app.domainDropDown.ValueChangedFcn = createCallbackFcn(app, @domainDropDownValueChanged, true);
            app.domainDropDown.Position = [74 61 91 22];
            app.domainDropDown.Value = 'unit ball';

            % Create BCDropDownLabel
            app.BCDropDownLabel = uilabel(app.ProblemPanel);
            app.BCDropDownLabel.HorizontalAlignment = 'right';
            app.BCDropDownLabel.Position = [29 23 25 15];
            app.BCDropDownLabel.Text = 'BC';

            % Create BCDropDown
            app.BCDropDown = uidropdown(app.ProblemPanel);
            app.BCDropDown.Items = {'Dirichlet', 'Neumann'};
            app.BCDropDown.ValueChangedFcn = createCallbackFcn(app, @BCDropDownValueChanged, true);
            app.BCDropDown.Position = [63 19 91 22];
            app.BCDropDown.Value = 'Dirichlet';

            % Create problemDropDownLabel
            app.problemDropDownLabel = uilabel(app.ProblemPanel);
            app.problemDropDownLabel.HorizontalAlignment = 'right';
            app.problemDropDownLabel.Position = [142 99 51 15];
            app.problemDropDownLabel.Text = 'problem';

            % Create problemDropDown
            app.problemDropDown = uidropdown(app.ProblemPanel);
            app.problemDropDown.Items = {'u = prod(sin(pi*x),2)', 'u = sin(10*sum(x,2))', 'adaptmesh'};
            app.problemDropDown.ValueChangedFcn = createCallbackFcn(app, @problemDropDownValueChanged, true);
            app.problemDropDown.Position = [202 95 240 22];
            app.problemDropDown.Value = 'u = prod(sin(pi*x),2)';

            % Create cDropDownLabel
            app.cDropDownLabel = uilabel(app.ProblemPanel);
            app.cDropDownLabel.HorizontalAlignment = 'right';
            app.cDropDownLabel.Position = [196 65 25 15];
            app.cDropDownLabel.Text = 'c';

            % Create cDropDown
            app.cDropDown = uidropdown(app.ProblemPanel);
            app.cDropDown.Items = {'0', '1', '@(x) x(:,1)'};
            app.cDropDown.Editable = 'on';
            app.cDropDown.BackgroundColor = [1 1 1];
            app.cDropDown.Position = [230 61 206 22];
            app.cDropDown.Value = '0';

            % Create NodesPanel
            app.NodesPanel = uipanel(app.UIFigure);
            app.NodesPanel.Title = 'Nodes';
            app.NodesPanel.FontWeight = 'bold';
            app.NodesPanel.Position = [33 331 459 140];

            % Create distmeshparametersPanel
            app.distmeshparametersPanel = uipanel(app.NodesPanel);
            app.distmeshparametersPanel.TitlePosition = 'centertop';
            app.distmeshparametersPanel.Title = 'distmesh parameters';
            app.distmeshparametersPanel.Visible = 'off';
            app.distmeshparametersPanel.Position = [242 14 202 97];

            % Create powDMLabel
            app.powDMLabel = uilabel(app.distmeshparametersPanel);
            app.powDMLabel.HorizontalAlignment = 'right';
            app.powDMLabel.Visible = 'off';
            app.powDMLabel.Position = [12 12 28 15];
            app.powDMLabel.Text = 'pow';

            % Create powDMEditField
            app.powDMEditField = uieditfield(app.distmeshparametersPanel, 'numeric');
            app.powDMEditField.Limits = [0.0001 1000];
            app.powDMEditField.HorizontalAlignment = 'left';
            app.powDMEditField.Visible = 'off';
            app.powDMEditField.Position = [50 8 50 22];
            app.powDMEditField.Value = 0.8;

            % Create mfDMLabel
            app.mfDMLabel = uilabel(app.distmeshparametersPanel);
            app.mfDMLabel.HorizontalAlignment = 'right';
            app.mfDMLabel.Visible = 'off';
            app.mfDMLabel.Position = [119 12 25 15];
            app.mfDMLabel.Text = 'mf';

            % Create mfDMEditField
            app.mfDMEditField = uieditfield(app.distmeshparametersPanel, 'numeric');
            app.mfDMEditField.Limits = [1 100];
            app.mfDMEditField.ValueDisplayFormat = '%.0f';
            app.mfDMEditField.Visible = 'off';
            app.mfDMEditField.Position = [152 8 34 22];
            app.mfDMEditField.Value = 10;

            % Create maxiterDMEditFieldLabel
            app.maxiterDMEditFieldLabel = uilabel(app.distmeshparametersPanel);
            app.maxiterDMEditFieldLabel.HorizontalAlignment = 'right';
            app.maxiterDMEditFieldLabel.Position = [5 46 45 15];
            app.maxiterDMEditFieldLabel.Text = 'maxiter';

            % Create maxiterDMEditField
            app.maxiterDMEditField = uieditfield(app.distmeshparametersPanel, 'numeric');
            app.maxiterDMEditField.Limits = [1 Inf];
            app.maxiterDMEditField.ValueDisplayFormat = '%.0f';
            app.maxiterDMEditField.Position = [61 42 46 22];
            app.maxiterDMEditField.Value = 300;

            % Create typeDMDropDown
            app.typeDMDropDown = uidropdown(app.distmeshparametersPanel);
            app.typeDMDropDown.Items = {'uniform', 'boundary'};
            app.typeDMDropDown.ValueChangedFcn = createCallbackFcn(app, @typeDMDropDownValueChanged, true);
            app.typeDMDropDown.Position = [119 42 76 22];
            app.typeDMDropDown.Value = 'uniform';

            % Create spacingEditFieldLabel
            app.spacingEditFieldLabel = uilabel(app.NodesPanel);
            app.spacingEditFieldLabel.HorizontalAlignment = 'right';
            app.spacingEditFieldLabel.Position = [7 93 49 15];
            app.spacingEditFieldLabel.Text = 'spacing';

            % Create spacing_nodesEditField
            app.spacing_nodesEditField = uieditfield(app.NodesPanel, 'numeric');
            app.spacing_nodesEditField.Limits = [0.0001 1000];
            app.spacing_nodesEditField.HorizontalAlignment = 'left';
            app.spacing_nodesEditField.Position = [68 89 49 22];
            app.spacing_nodesEditField.Value = 0.4;

            % Create interiorDropDownLabel
            app.interiorDropDownLabel = uilabel(app.NodesPanel);
            app.interiorDropDownLabel.HorizontalAlignment = 'right';
            app.interiorDropDownLabel.Position = [15 61 43 15];
            app.interiorDropDownLabel.Text = 'interior';

            % Create interiorDropDown
            app.interiorDropDown = uidropdown(app.NodesPanel);
            app.interiorDropDown.Items = {'grid', 'hexgrid', 'Halton', 'Sobol', 'random', 'distmesh'};
            app.interiorDropDown.ValueChangedFcn = createCallbackFcn(app, @interiorDropDownValueChanged, true);
            app.interiorDropDown.Position = [75 57 100 22];
            app.interiorDropDown.Value = 'grid';

            % Create boundaryDropDownLabel
            app.boundaryDropDownLabel = uilabel(app.NodesPanel);
            app.boundaryDropDownLabel.HorizontalAlignment = 'right';
            app.boundaryDropDownLabel.Position = [8 34 56 15];
            app.boundaryDropDownLabel.Text = 'boundary';

            % Create boundaryDropDown
            app.boundaryDropDown = uidropdown(app.NodesPanel);
            app.boundaryDropDown.Items = {'projection', 'isosurface', 'uniform'};
            app.boundaryDropDown.Position = [74 30 101 22];
            app.boundaryDropDown.Value = 'projection';

            % Create refineSpinnerLabel
            app.refineSpinnerLabel = uilabel(app.NodesPanel);
            app.refineSpinnerLabel.HorizontalAlignment = 'right';
            app.refineSpinnerLabel.Position = [128 93 36 15];
            app.refineSpinnerLabel.Text = 'refine';

            % Create refineSpinner
            app.refineSpinner = uispinner(app.NodesPanel);
            app.refineSpinner.Limits = [0 99];
            app.refineSpinner.Position = [179 89 54 22];

            % Create DiffMatrixTabGroup
            app.DiffMatrixTabGroup = uitabgroup(app.UIFigure);
            app.DiffMatrixTabGroup.Position = [26 27 522 267];

            % Create LaplaceTab
            app.LaplaceTab = uitab(app.DiffMatrixTabGroup);
            app.LaplaceTab.Title = 'Laplace';

            % Create LsetsofInfluencePanel
            app.LsetsofInfluencePanel = uipanel(app.LaplaceTab);
            app.LsetsofInfluencePanel.Title = 'Sets of Influence';
            app.LsetsofInfluencePanel.FontAngle = 'italic';
            app.LsetsofInfluencePanel.Position = [10 130 498 102];

            % Create LsizeSISpinnerLabel
            app.LsizeSISpinnerLabel = uilabel(app.LsetsofInfluencePanel);
            app.LsizeSISpinnerLabel.HorizontalAlignment = 'right';
            app.LsizeSISpinnerLabel.Position = [393 54 27 15];
            app.LsizeSISpinnerLabel.Text = 'size';

            % Create LsizeSISpinner
            app.LsizeSISpinner = uispinner(app.LsetsofInfluencePanel);
            app.LsizeSISpinner.Limits = [1 999];
            app.LsizeSISpinner.Position = [435 50 52 22];
            app.LsizeSISpinner.Value = 7;

            % Create LselectionmethodDropDownLabel
            app.LselectionmethodDropDownLabel = uilabel(app.LsetsofInfluencePanel);
            app.LselectionmethodDropDownLabel.HorizontalAlignment = 'right';
            app.LselectionmethodDropDownLabel.Position = [14 54 100 15];
            app.LselectionmethodDropDownLabel.Text = 'selection method';

            % Create LselectionmethodDropDown
            app.LselectionmethodDropDown = uidropdown(app.LsetsofInfluencePanel);
            app.LselectionmethodDropDown.Items = {'nearest', 'L1 selection'};
            app.LselectionmethodDropDown.ValueChangedFcn = createCallbackFcn(app, @LselectionmethodDropDownValueChanged, true);
            app.LselectionmethodDropDown.Position = [123 50 120 22];
            app.LselectionmethodDropDown.Value = 'nearest';

            % Create Lsel_orderSpinnerLabel
            app.Lsel_orderSpinnerLabel = uilabel(app.LsetsofInfluencePanel);
            app.Lsel_orderSpinnerLabel.HorizontalAlignment = 'right';
            app.Lsel_orderSpinnerLabel.Visible = 'off';
            app.Lsel_orderSpinnerLabel.Position = [16 22 58 15];
            app.Lsel_orderSpinnerLabel.Text = 'sel_order';

            % Create Lsel_orderSpinner
            app.Lsel_orderSpinner = uispinner(app.LsetsofInfluencePanel);
            app.Lsel_orderSpinner.Limits = [3 Inf];
            app.Lsel_orderSpinner.ValueChangedFcn = createCallbackFcn(app, @Lsel_orderSpinnerValueChanged, true);
            app.Lsel_orderSpinner.Visible = 'off';
            app.Lsel_orderSpinner.Position = [83 19 52 22];
            app.Lsel_orderSpinner.Value = 3;

            % Create Lsel_muLabel
            app.Lsel_muLabel = uilabel(app.LsetsofInfluencePanel);
            app.Lsel_muLabel.HorizontalAlignment = 'right';
            app.Lsel_muLabel.Visible = 'off';
            app.Lsel_muLabel.Position = [152 23 25 15];
            app.Lsel_muLabel.Text = 'mu';

            % Create Lsel_muEditField
            app.Lsel_muEditField = uieditfield(app.LsetsofInfluencePanel, 'numeric');
            app.Lsel_muEditField.Limits = [0 1000];
            app.Lsel_muEditField.HorizontalAlignment = 'left';
            app.Lsel_muEditField.Visible = 'off';
            app.Lsel_muEditField.Position = [183 19 49 22];
            app.Lsel_muEditField.Value = 3;

            % Create LuseweightsButton
            app.LuseweightsButton = uibutton(app.LsetsofInfluencePanel, 'state');
            app.LuseweightsButton.ValueChangedFcn = createCallbackFcn(app, @LuseweightsButtonValueChanged, true);
            app.LuseweightsButton.Visible = 'off';
            app.LuseweightsButton.Text = 'use weights';
            app.LuseweightsButton.Position = [266 50 79 22];

            % Create LinitcloudSpinnerLabel
            app.LinitcloudSpinnerLabel = uilabel(app.LsetsofInfluencePanel);
            app.LinitcloudSpinnerLabel.HorizontalAlignment = 'right';
            app.LinitcloudSpinnerLabel.Visible = 'off';
            app.LinitcloudSpinnerLabel.Position = [352 22 57 15];
            app.LinitcloudSpinnerLabel.Text = 'init. cloud';

            % Create LinitcloudSpinner
            app.LinitcloudSpinner = uispinner(app.LsetsofInfluencePanel);
            app.LinitcloudSpinner.Limits = [1 999];
            app.LinitcloudSpinner.Visible = 'off';
            app.LinitcloudSpinner.Position = [427 19 60 22];
            app.LinitcloudSpinner.Value = 12;

            % Create LtrypositiveButton
            app.LtrypositiveButton = uibutton(app.LsetsofInfluencePanel, 'state');
            app.LtrypositiveButton.Visible = 'off';
            app.LtrypositiveButton.Text = 'try positive';
            app.LtrypositiveButton.Position = [266 19 79 22];

            % Create LweightsPanel
            app.LweightsPanel = uipanel(app.LaplaceTab);
            app.LweightsPanel.Title = 'Weights';
            app.LweightsPanel.FontAngle = 'italic';
            app.LweightsPanel.Position = [10 15 498 106];

            % Create LwtypeLabel
            app.LwtypeLabel = uilabel(app.LweightsPanel);
            app.LwtypeLabel.HorizontalAlignment = 'right';
            app.LwtypeLabel.Position = [15 50 27 15];
            app.LwtypeLabel.Text = 'type';

            % Create LwtypeDropDown
            app.LwtypeDropDown = uidropdown(app.LweightsPanel);
            app.LwtypeDropDown.Items = {'Power/TPS', 'Gauss-QR', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};
            app.LwtypeDropDown.ValueChangedFcn = createCallbackFcn(app, @LwtypeDropDownValueChanged, true);
            app.LwtypeDropDown.Position = [57 46 89 22];
            app.LwtypeDropDown.Value = 'Gauss';

            % Create LporderSpinnerLabel
            app.LporderSpinnerLabel = uilabel(app.LweightsPanel);
            app.LporderSpinnerLabel.HorizontalAlignment = 'right';
            app.LporderSpinnerLabel.Position = [171 50 41 15];
            app.LporderSpinnerLabel.Text = 'porder';

            % Create LporderSpinner
            app.LporderSpinner = uispinner(app.LweightsPanel);
            app.LporderSpinner.Limits = [0 99];
            app.LporderSpinner.ValueChangedFcn = createCallbackFcn(app, @LporderSpinnerValueChanged, true);
            app.LporderSpinner.Position = [218 46 53 22];

            % Create LrbfepLabel
            app.LrbfepLabel = uilabel(app.LweightsPanel);
            app.LrbfepLabel.HorizontalAlignment = 'right';
            app.LrbfepLabel.Position = [294 50 22 15];
            app.LrbfepLabel.Text = 'ep';

            % Create LrbfepEditField
            app.LrbfepEditField = uieditfield(app.LweightsPanel, 'numeric');
            app.LrbfepEditField.Limits = [0 1000];
            app.LrbfepEditField.HorizontalAlignment = 'left';
            app.LrbfepEditField.Position = [328 46 49 22];
            app.LrbfepEditField.Value = 1;

            % Create LrbfpowLabel
            app.LrbfpowLabel = uilabel(app.LweightsPanel);
            app.LrbfpowLabel.HorizontalAlignment = 'right';
            app.LrbfpowLabel.Visible = 'off';
            app.LrbfpowLabel.Position = [31 18 28 15];
            app.LrbfpowLabel.Text = 'pow';

            % Create LrbfpowEditField
            app.LrbfpowEditField = uieditfield(app.LweightsPanel, 'numeric');
            app.LrbfpowEditField.Limits = [0.0001 1000];
            app.LrbfpowEditField.ValueChangedFcn = createCallbackFcn(app, @LrbfpowEditFieldValueChanged, true);
            app.LrbfpowEditField.HorizontalAlignment = 'left';
            app.LrbfpowEditField.Visible = 'off';
            app.LrbfpowEditField.Position = [71 14 49 22];
            app.LrbfpowEditField.Value = 5;

            % Create LpolymuLabel
            app.LpolymuLabel = uilabel(app.LweightsPanel);
            app.LpolymuLabel.HorizontalAlignment = 'right';
            app.LpolymuLabel.Visible = 'off';
            app.LpolymuLabel.Position = [184 12 25 15];
            app.LpolymuLabel.Text = 'mu';

            % Create LpolymuEditField
            app.LpolymuEditField = uieditfield(app.LweightsPanel, 'numeric');
            app.LpolymuEditField.Limits = [0 1000];
            app.LpolymuEditField.HorizontalAlignment = 'left';
            app.LpolymuEditField.Visible = 'off';
            app.LpolymuEditField.Position = [222 11 49 22];
            app.LpolymuEditField.Value = 3;

            % Create LwenddSpinnerLabel
            app.LwenddSpinnerLabel = uilabel(app.LweightsPanel);
            app.LwenddSpinnerLabel.HorizontalAlignment = 'right';
            app.LwenddSpinnerLabel.Visible = 'off';
            app.LwenddSpinnerLabel.Position = [31 15 10 15];
            app.LwenddSpinnerLabel.Text = 'd';

            % Create LwenddSpinner
            app.LwenddSpinner = uispinner(app.LweightsPanel);
            app.LwenddSpinner.Step = 2;
            app.LwenddSpinner.Limits = [1 99];
            app.LwenddSpinner.Visible = 'off';
            app.LwenddSpinner.Position = [47 11 53 22];
            app.LwenddSpinner.Value = 3;

            % Create LwendkSpinnerLabel
            app.LwendkSpinnerLabel = uilabel(app.LweightsPanel);
            app.LwendkSpinnerLabel.HorizontalAlignment = 'right';
            app.LwendkSpinnerLabel.Visible = 'off';
            app.LwendkSpinnerLabel.Position = [97 14 25 15];
            app.LwendkSpinnerLabel.Text = 'k';

            % Create LwendkSpinner
            app.LwendkSpinner = uispinner(app.LweightsPanel);
            app.LwendkSpinner.Limits = [1 99];
            app.LwendkSpinner.Visible = 'off';
            app.LwendkSpinner.Position = [128 11 53 22];
            app.LwendkSpinner.Value = 3;

            % Create LfdstarongridsButton
            app.LfdstarongridsButton = uibutton(app.LaplaceTab, 'state');
            app.LfdstarongridsButton.Text = 'fd star on grids';
            app.LfdstarongridsButton.Position = [83 99 92 22];

            % Create NeumannTab
            app.NeumannTab = uitab(app.DiffMatrixTabGroup);

            % Create NweightsPanel
            app.NweightsPanel = uipanel(app.NeumannTab);
            app.NweightsPanel.Title = 'Weights';
            app.NweightsPanel.Visible = 'off';
            app.NweightsPanel.FontAngle = 'italic';
            app.NweightsPanel.Position = [10 14 498 106];

            % Create NwtypeLabel
            app.NwtypeLabel = uilabel(app.NweightsPanel);
            app.NwtypeLabel.HorizontalAlignment = 'right';
            app.NwtypeLabel.Position = [15 50 27 15];
            app.NwtypeLabel.Text = 'type';

            % Create NwtypeDropDown
            app.NwtypeDropDown = uidropdown(app.NweightsPanel);
            app.NwtypeDropDown.Items = {'Power/TPS', 'Gauss-QR', 'Gauss', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2'};
            app.NwtypeDropDown.ValueChangedFcn = createCallbackFcn(app, @NwtypeDropDownValueChanged, true);
            app.NwtypeDropDown.Position = [57 46 89 22];
            app.NwtypeDropDown.Value = 'Gauss';

            % Create NporderSpinnerLabel
            app.NporderSpinnerLabel = uilabel(app.NweightsPanel);
            app.NporderSpinnerLabel.HorizontalAlignment = 'right';
            app.NporderSpinnerLabel.Position = [171 50 41 15];
            app.NporderSpinnerLabel.Text = 'porder';

            % Create NporderSpinner
            app.NporderSpinner = uispinner(app.NweightsPanel);
            app.NporderSpinner.Limits = [0 99];
            app.NporderSpinner.ValueChangedFcn = createCallbackFcn(app, @NporderSpinnerValueChanged, true);
            app.NporderSpinner.Position = [218 46 53 22];

            % Create NrbfepLabel
            app.NrbfepLabel = uilabel(app.NweightsPanel);
            app.NrbfepLabel.HorizontalAlignment = 'right';
            app.NrbfepLabel.Position = [294 50 22 15];
            app.NrbfepLabel.Text = 'ep';

            % Create NrbfepEditField
            app.NrbfepEditField = uieditfield(app.NweightsPanel, 'numeric');
            app.NrbfepEditField.Limits = [0 1000];
            app.NrbfepEditField.HorizontalAlignment = 'left';
            app.NrbfepEditField.Position = [328 46 49 22];
            app.NrbfepEditField.Value = 1;

            % Create NrbfpowLabel
            app.NrbfpowLabel = uilabel(app.NweightsPanel);
            app.NrbfpowLabel.HorizontalAlignment = 'right';
            app.NrbfpowLabel.Visible = 'off';
            app.NrbfpowLabel.Position = [31 18 28 15];
            app.NrbfpowLabel.Text = 'pow';

            % Create NrbfpowEditField
            app.NrbfpowEditField = uieditfield(app.NweightsPanel, 'numeric');
            app.NrbfpowEditField.Limits = [0.0001 1000];
            app.NrbfpowEditField.ValueChangedFcn = createCallbackFcn(app, @NrbfpowEditFieldValueChanged, true);
            app.NrbfpowEditField.HorizontalAlignment = 'left';
            app.NrbfpowEditField.Visible = 'off';
            app.NrbfpowEditField.Position = [71 14 49 22];
            app.NrbfpowEditField.Value = 5;

            % Create NpolymuLabel
            app.NpolymuLabel = uilabel(app.NweightsPanel);
            app.NpolymuLabel.HorizontalAlignment = 'right';
            app.NpolymuLabel.Visible = 'off';
            app.NpolymuLabel.Position = [180 12 25 15];
            app.NpolymuLabel.Text = 'mu';

            % Create NpolymuEditField
            app.NpolymuEditField = uieditfield(app.NweightsPanel, 'numeric');
            app.NpolymuEditField.Limits = [0 1000];
            app.NpolymuEditField.HorizontalAlignment = 'left';
            app.NpolymuEditField.Visible = 'off';
            app.NpolymuEditField.Position = [218 11 49 22];
            app.NpolymuEditField.Value = 2;

            % Create NwenddSpinnerLabel
            app.NwenddSpinnerLabel = uilabel(app.NweightsPanel);
            app.NwenddSpinnerLabel.HorizontalAlignment = 'right';
            app.NwenddSpinnerLabel.Visible = 'off';
            app.NwenddSpinnerLabel.Position = [31 15 10 15];
            app.NwenddSpinnerLabel.Text = 'd';

            % Create NwenddSpinner
            app.NwenddSpinner = uispinner(app.NweightsPanel);
            app.NwenddSpinner.Step = 2;
            app.NwenddSpinner.Limits = [1 99];
            app.NwenddSpinner.Visible = 'off';
            app.NwenddSpinner.Position = [47 11 53 22];
            app.NwenddSpinner.Value = 1;

            % Create NwendkSpinnerLabel
            app.NwendkSpinnerLabel = uilabel(app.NweightsPanel);
            app.NwendkSpinnerLabel.HorizontalAlignment = 'right';
            app.NwendkSpinnerLabel.Visible = 'off';
            app.NwendkSpinnerLabel.Position = [97 14 25 15];
            app.NwendkSpinnerLabel.Text = 'k';

            % Create NwendkSpinner
            app.NwendkSpinner = uispinner(app.NweightsPanel);
            app.NwendkSpinner.Limits = [1 99];
            app.NwendkSpinner.Visible = 'off';
            app.NwendkSpinner.Position = [128 11 53 22];
            app.NwendkSpinner.Value = 1;

            % Create NsetsofInfluencePanel
            app.NsetsofInfluencePanel = uipanel(app.NeumannTab);
            app.NsetsofInfluencePanel.Title = 'Sets of Influence';
            app.NsetsofInfluencePanel.Visible = 'off';
            app.NsetsofInfluencePanel.FontAngle = 'italic';
            app.NsetsofInfluencePanel.Position = [10 132 498 102];

            % Create NsizeSISpinnerLabel
            app.NsizeSISpinnerLabel = uilabel(app.NsetsofInfluencePanel);
            app.NsizeSISpinnerLabel.HorizontalAlignment = 'right';
            app.NsizeSISpinnerLabel.Position = [393 54 27 15];
            app.NsizeSISpinnerLabel.Text = 'size';

            % Create NsizeSISpinner
            app.NsizeSISpinner = uispinner(app.NsetsofInfluencePanel);
            app.NsizeSISpinner.Limits = [1 999];
            app.NsizeSISpinner.Position = [435 50 52 22];
            app.NsizeSISpinner.Value = 7;

            % Create NselectionmethodDropDownLabel
            app.NselectionmethodDropDownLabel = uilabel(app.NsetsofInfluencePanel);
            app.NselectionmethodDropDownLabel.HorizontalAlignment = 'right';
            app.NselectionmethodDropDownLabel.Position = [14 54 100 15];
            app.NselectionmethodDropDownLabel.Text = 'selection method';

            % Create NselectionmethodDropDown
            app.NselectionmethodDropDown = uidropdown(app.NsetsofInfluencePanel);
            app.NselectionmethodDropDown.Items = {'nearest', 'L1 selection'};
            app.NselectionmethodDropDown.ValueChangedFcn = createCallbackFcn(app, @NselectionmethodDropDownValueChanged, true);
            app.NselectionmethodDropDown.Position = [123 50 120 22];
            app.NselectionmethodDropDown.Value = 'nearest';

            % Create Nsel_orderSpinnerLabel
            app.Nsel_orderSpinnerLabel = uilabel(app.NsetsofInfluencePanel);
            app.Nsel_orderSpinnerLabel.HorizontalAlignment = 'right';
            app.Nsel_orderSpinnerLabel.Visible = 'off';
            app.Nsel_orderSpinnerLabel.Position = [16 22 58 15];
            app.Nsel_orderSpinnerLabel.Text = 'sel_order';

            % Create Nsel_orderSpinner
            app.Nsel_orderSpinner = uispinner(app.NsetsofInfluencePanel);
            app.Nsel_orderSpinner.Limits = [2 Inf];
            app.Nsel_orderSpinner.ValueChangedFcn = createCallbackFcn(app, @Nsel_orderSpinnerValueChanged, true);
            app.Nsel_orderSpinner.Visible = 'off';
            app.Nsel_orderSpinner.Position = [83 19 52 22];
            app.Nsel_orderSpinner.Value = 2;

            % Create Nsel_muLabel
            app.Nsel_muLabel = uilabel(app.NsetsofInfluencePanel);
            app.Nsel_muLabel.HorizontalAlignment = 'right';
            app.Nsel_muLabel.Visible = 'off';
            app.Nsel_muLabel.Position = [152 23 25 15];
            app.Nsel_muLabel.Text = 'mu';

            % Create Nsel_muEditField
            app.Nsel_muEditField = uieditfield(app.NsetsofInfluencePanel, 'numeric');
            app.Nsel_muEditField.Limits = [0 1000];
            app.Nsel_muEditField.HorizontalAlignment = 'left';
            app.Nsel_muEditField.Visible = 'off';
            app.Nsel_muEditField.Position = [183 19 49 22];
            app.Nsel_muEditField.Value = 2;

            % Create NuseweightsButton
            app.NuseweightsButton = uibutton(app.NsetsofInfluencePanel, 'state');
            app.NuseweightsButton.ValueChangedFcn = createCallbackFcn(app, @NuseweightsButtonValueChanged, true);
            app.NuseweightsButton.Visible = 'off';
            app.NuseweightsButton.Text = 'use weights';
            app.NuseweightsButton.Position = [266 50 79 22];

            % Create NinitcloudSpinnerLabel
            app.NinitcloudSpinnerLabel = uilabel(app.NsetsofInfluencePanel);
            app.NinitcloudSpinnerLabel.HorizontalAlignment = 'right';
            app.NinitcloudSpinnerLabel.Visible = 'off';
            app.NinitcloudSpinnerLabel.Position = [352 22 57 15];
            app.NinitcloudSpinnerLabel.Text = 'init. cloud';

            % Create NinitcloudSpinner
            app.NinitcloudSpinner = uispinner(app.NsetsofInfluencePanel);
            app.NinitcloudSpinner.Limits = [1 999];
            app.NinitcloudSpinner.Visible = 'off';
            app.NinitcloudSpinner.Position = [427 19 60 22];
            app.NinitcloudSpinner.Value = 6;

            % Create DifferentiationMatrixLabel
            app.DifferentiationMatrixLabel = uilabel(app.UIFigure);
            app.DifferentiationMatrixLabel.FontWeight = 'bold';
            app.DifferentiationMatrixLabel.Position = [36 302 124 15];
            app.DifferentiationMatrixLabel.Text = 'Differentiation Matrix';
        end
    end

    methods (Access = public)

        % Construct app
        function app = Laplace_options_gui(varargin)

            % Create and configure components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            % Execute the startup function
            runStartupFcn(app, @(app)startupFcn(app, varargin{:}))

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end
