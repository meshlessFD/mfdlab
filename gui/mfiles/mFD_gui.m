
classdef mFD_gui < matlab.apps.AppBase

    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                   matlab.ui.Figure
        StartButton                matlab.ui.control.Button
        AtstartPanel               matlab.ui.container.Panel
        longEngButton              matlab.ui.control.StateButton
        closeallButton             matlab.ui.control.StateButton
        clearallButton             matlab.ui.control.StateButton
        clcButton                  matlab.ui.control.StateButton
        ProblemTypeLabel           matlab.ui.control.Label
        ProblemTypeDropDown        matlab.ui.control.DropDown
        PostprocessingPanel        matlab.ui.container.Panel
        computeeigenvaluesButton   matlab.ui.control.StateButton
        exploresystemmatrixButton  matlab.ui.control.StateButton
        plotsolutionButton         matlab.ui.control.StateButton
        ploterrorfunctionButton    matlab.ui.control.StateButton
        usereducedmatrixButton     matlab.ui.control.StateButton
        plotnodesButton            matlab.ui.control.StateButton
        ploteigenvaluesButton      matlab.ui.control.StateButton
        figs50Label                matlab.ui.control.Label
        figs1020Label              matlab.ui.control.Label
        figs3040Label              matlab.ui.control.Label
        figs60Label                matlab.ui.control.Label
        displaysummaryButton       matlab.ui.control.StateButton
        ploterrorsButton           matlab.ui.control.StateButton
        plotcondstabButton         matlab.ui.control.StateButton
        figs100102103Label         matlab.ui.control.Label
        fig101Label                matlab.ui.control.Label
        computestatisticsButton    matlab.ui.control.StateButton
    end

    % App Designer file mFD_gui.mlapp: the main GUI file for mFDlab.
    % Calls *_options_gui.mlapp and starts script_mFD_*.m
    %
    % Copyright (C) 2017-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
    %
    % This file is part of mFDlab released under GNU General Public License v2.0.
    % See the file README for further details.

    
    %user properties
    properties (Access = private)   
        options_app = []; %app with options for particular problem types
        
    end

    methods (Access = private)
    
        
        
        
        
    end

    methods (Access = public)
        
        
        %to be called from the options app
        function get_options_app(app,oapp)
            app.options_app = oapp;
        end
        
        %to be called by both main app and options app
        function backup_on_close_request(app,app_to_close)
            
            %(in addition to backup!) set the DropDown menu to 'choose problem type'
            app.ProblemTypeDropDown.Value = app.ProblemTypeDropDown.Items(1); 
            
            %update the m-file version of the app and make backups of .mlapp and .m-files in mlapp_backups
            %(if m-file is either not existent or older than .mlapp-file: since App Designer saves mlapp-file 
            %before running the gui)
            filename = class(app_to_close);
            [filepath,~,~] = fileparts(which([filename,'.mlapp']));
            mlappfilename = fullfile(filepath,[filename,'.mlapp']);
            %m-file to be stored in the subdirectory gui/mfiles
            if strcmp(filename,'mFD_gui')
                m_filename = fullfile(filepath,'gui','mfiles',[filename,'.m']);
            else
                m_filename = fullfile(filepath,'mfiles',[filename,'.m']);
            end
            if exist(mlappfilename,'file')
                
                %check that update is needed
                if exist(m_filename,'file') %otherwise update and backup are surely needed
                    D1 = dir(mlappfilename);
                    D2 = dir(m_filename);
                    if D1.datenum < D2.datenum + 2.314814814814815e-05
                        %mlapp-file not younger than m-file by at least 2 seconds: no update needed since the code is only changed
                        %from within App Designer that saves the latest version of mlapp-file before running the GUI
                        %(2 second tolerance to avoid annecessary beckup when the time stamps are mixed up by git)
                        disp(' ')
                        disp(['Exit ',filename])
                        return;
                    end
                end
                
                %create backup directory if not existent
                if ~isdir(['gui',filesep,'mlapp_backups'])
                    mkdir(['gui',filesep,'mlapp_backups']);
                    disp('created subdirectory gui/mlapp_backups')
                end
                
                         
                %create/update the m-file
                %using suggestion from https://github.com/StackOverflowMATLABchat/mlapp2classdef/issues/8
                fid = fopen(m_filename,'w');
                evalc type(mlappfilename);
                fprintf(fid,'%s',ans);
                fclose(fid);
                
                %create backup copies of both files
                current_datetime = char(datetime('now','Format','uuuu-MM-dd_HH:mm:ss'));
                copyfile(mlappfilename,['gui',filesep,'mlapp_backups',filesep,filename,current_datetime,'.mlapp'])               
                copyfile(m_filename,['gui',filesep,'mlapp_backups',filesep,filename,current_datetime,'.m'])  
                
                
                disp(' ')
                disp(['Exit ',filename,' and update ',filename,'.m'])
                disp('Save backups of .mlapp- and .m-files in mlapp_backups')
            else
                disp(' ')
                disp(['Exit ',filename])
                
            end
                        
        end

        
        %to be used by options app with different wtype drop-down menus that
        %influence ep, pow, porder and mu settings of the numerical differentiation 
        %(see Laplace_gui.mlapp for an example)
        function wtypeChanged(app,value,...
                              epEditField,epLabel,...
                              powEditField,powLabel,...
                              wenddSpinner,wenddSpinnerLabel,...
                              wendkSpinner,wendkSpinnerLabel,...
                              muEditField,muLabel,...
                              porderSpinner,porderLabel)
            
            switch value
                case 'Power/TPS'
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'off';
                    epLabel.Visible = 'off';
                    
                    powEditField.Visible = 'on';
                    powLabel.Visible = 'on';
                    
                    wenddSpinner.Visible = 'off';
                    wenddSpinnerLabel.Visible = 'off';
                    
                    wendkSpinner.Visible = 'off';
                    wendkSpinnerLabel.Visible = 'off';
                    
                    muEditField.Visible = 'off';
                    muLabel.Visible = 'off';
                    
                    porderSpinner.Visible = 'on';
                    porderLabel.Visible = 'on';
                    %make sure that rbf gets porder at least its minimum cpd order
                    rbfpow = powEditField.Value;
                    porderSpinner.Limits(1) = floor(rbfpow/2)+1;
                    porderSpinner.Enable = 'on';                   
                    
                case 'MQ'
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'on';
                    epLabel.Visible = 'on';
                    
                    powEditField.Visible = 'on';
                    powLabel.Visible = 'on';
                    
                    wenddSpinner.Visible = 'off';
                    wenddSpinnerLabel.Visible = 'off';
                    
                    wendkSpinner.Visible = 'off';
                    wendkSpinnerLabel.Visible = 'off';
                   
                    muEditField.Visible = 'off';
                    muLabel.Visible = 'off';
                    
                    porderSpinner.Visible = 'on';
                    porderLabel.Visible = 'on';
                    %make sure that rbf gets porder at least its minimum cpd order
                    rbfpow = powEditField.Value;
                    if rbfpow <= 1
                        porderSpinner.Limits(1) = 0;
                    else
                        porderSpinner.Limits(1) = floor(rbfpow/2)+1;
                    end
                    porderSpinner.Enable = 'on';                   
                 
                    
               case {'Gauss-QR','Gauss'}
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'on';
                    epLabel.Visible = 'on';
                    
                    powEditField.Visible = 'off';
                    powLabel.Visible = 'off';  
                    
                    wenddSpinner.Visible = 'off';
                    wenddSpinnerLabel.Visible = 'off';
                    
                    wendkSpinner.Visible = 'off';
                    wendkSpinnerLabel.Visible = 'off';
                    
                    muEditField.Visible = 'off';
                    muLabel.Visible = 'off';
                    
                    porderSpinner.Visible = 'on';
                    porderLabel.Visible = 'on';
                    %adjust limits for porder
                    porderSpinner.Limits(1) = 0;  
                    porderSpinner.Value = 0; %default for Gauss, and currently the only option for Gauss-QR
                    if strcmp(value,'Gauss-QR')
                        %make sure porder=0 only
                        porderSpinner.Enable = 'off';
                    else
                        porderSpinner.Enable = 'on';
                    end
                    
                case {'Matern','IMQ'}
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'on';
                    epLabel.Visible = 'on';
                    
                    powEditField.Visible = 'on';
                    powLabel.Visible = 'on';  
                    
                    wenddSpinner.Visible = 'off';
                    wenddSpinnerLabel.Visible = 'off';
                    
                    wendkSpinner.Visible = 'off';
                    wendkSpinnerLabel.Visible = 'off';
                    
                    muEditField.Visible = 'off';
                    muLabel.Visible = 'off';
                    
                    porderSpinner.Visible = 'on';
                    porderLabel.Visible = 'on';
                    %remove possible limits on porder
                    porderSpinner.Limits(1) = 0;  
                    porderSpinner.Enable = 'on';

                case {'Wendland'}
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'on';
                    epLabel.Visible = 'on';
                    
                    powEditField.Visible = 'off';
                    powLabel.Visible = 'off';  
                    
                    wenddSpinner.Visible = 'on';
                    wenddSpinnerLabel.Visible = 'on';
                    
                    wendkSpinner.Visible = 'on';
                    wendkSpinnerLabel.Visible = 'on';
                    
                    muEditField.Visible = 'off';
                    muLabel.Visible = 'off';
                    
                    porderSpinner.Visible = 'on';
                    porderLabel.Visible = 'on';       
                    
                    %adjust limits for porder
                    porderSpinner.Limits(1) = 0;
                    porderSpinner.Enable = 'on';
                    
                    %adjust limits for parameter d depending on the dimension dim
                    dmin = app.options_app.dimSpinner.Value;
                    if ~rem(dmin,2) %replace by next odd number if dmin is even
                        dmin=dmin+1;
                    end
                    wenddSpinner.Limits(1) = dmin; 
                    
                case {'L1','L2'}
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'off';
                    epLabel.Visible = 'off';
                    
                    powEditField.Visible = 'off';
                    powLabel.Visible = 'off';  
                    
                    wenddSpinner.Visible = 'off';
                    wenddSpinnerLabel.Visible = 'off';
                    
                    wendkSpinner.Visible = 'off';
                    wendkSpinnerLabel.Visible = 'off';
                    
                    muEditField.Visible = 'on';
                    muLabel.Visible = 'on';
                    
                    porderSpinner.Visible = 'on';
                    porderLabel.Visible = 'on';                    
                    %remove possible limits on porder
                    porderSpinner.Limits(1) = 0;  
                    porderSpinner.Enable = 'on';

                    
                case 'Least Interp'
                    
                    %switch on/off (ir)relevant parmeters
                    epEditField.Visible = 'off';
                    epLabel.Visible = 'off';
                    
                    powEditField.Visible = 'off';
                    powLabel.Visible = 'off';  
                    
                    wenddSpinner.Visible = 'off';
                    wenddSpinnerLabel.Visible = 'off';
                    
                    wendkSpinner.Visible = 'off';
                    wendkSpinnerLabel.Visible = 'off';
                    
                    muEditField.Visible = 'off';
                    muLabel.Visible = 'off';
                    
                    porderSpinner.Visible = 'off';
                    porderLabel.Visible = 'off';
                    
            end
            
            
        end
        
        
  
        
        %to be used by a child (options) app with different selectionmethod drop-down menus that
        %influence the settings of the selection of set of influence
        %(see Laplace_gui.mlapp for an example)       
        %operatorname = 'L' for Laplace, 'N' for Neumann condition
        function selectionmethodChanged(app,child_app,operatorname,value)
            
            sizeSISpinnerLabel = [operatorname,'sizeSISpinnerLabel'];
            sizeSISpinner = [operatorname,'sizeSISpinner'];
            initcloudSpinnerLabel = [operatorname,'initcloudSpinnerLabel'];
            initcloudSpinner = [operatorname,'initcloudSpinner'];
            sel_orderSpinnerLabel = [operatorname,'sel_orderSpinnerLabel'];
            sel_orderSpinner = [operatorname,'sel_orderSpinner'];
            sel_muLabel = [operatorname,'sel_muLabel'];
            sel_muEditField = [operatorname,'sel_muEditField'];
            useweightsButton = [operatorname,'useweightsButton'];
                       
            switch value
                case 'nearest'
                    child_app.(sizeSISpinnerLabel).Visible = 'on';
                    child_app.(sizeSISpinner).Visible = 'on';
                    child_app.(initcloudSpinnerLabel).Visible = 'off';
                    child_app.(initcloudSpinner).Visible = 'off';
                    child_app.(sel_orderSpinnerLabel).Visible = 'off';
                    child_app.(sel_orderSpinner).Visible = 'off';
                    child_app.(sel_muLabel).Visible = 'off';
                    child_app.(sel_muEditField).Visible = 'off';
                    child_app.(useweightsButton).Visible = 'off';
                    child_app.(useweightsButton).Value = false;
                    
                      
                case {'L1 selection'}
                    child_app.(sizeSISpinnerLabel).Visible = 'off';
                    child_app.(sizeSISpinner).Visible = 'off';
                    child_app.(initcloudSpinnerLabel).Visible = 'on';
                    child_app.(initcloudSpinner).Visible = 'on';
                    child_app.(sel_orderSpinnerLabel).Visible = 'on';
                    child_app.(sel_orderSpinner).Visible = 'on';
                    %child_app.(sel_orderSpinner).Limits(1) = 3;
                    child_app.(sel_muLabel).Visible = 'on';
                    child_app.(sel_muEditField).Visible = 'on';
                    child_app.(useweightsButton).Visible = 'on';
                    
                otherwise
                    error('unknown selection method')
                    
            end

            
        end
        
    end


    methods (Access = private)

        % Code that executes after component creation
        function startupFcn(app)
            disp(' ')
            disp(['Start GUI ',class(app)])         
            
            %search *_options_gui.mlapp-files in subfolder gui
            addpath('gui') %needed to launch options apps later
            list = dir('gui/*_options_gui.mlapp');
            options = {list.name}; %get the names of the files
            for i=1:length(options) %remove _options_gui.mlapp in the name
                options{i}(end-17:end)=[];
            end
            
            %set problem type options
            app.ProblemTypeDropDown.Items = ['choose problem type',options];
            app.options_app = [];

        end

        % Button pushed function: StartButton
        function StartButtonPushed(app, event)
            
            %matlab cleanup and format in base workspace
            if app.clearallButton.Value
                evalin('base','clear all')
            end
            if app.closeallButton.Value 
                evalin('base','close all')
            end
            if app.longEngButton.Value
                evalin('base','format longEng')
            else
                evalin('base','format')
            end
            if app.clcButton.Value
                evalin('base','clc')
            end   

            %postprocessing options
            assignin('base','compute_statistics',app.computestatisticsButton.Value);
            assignin('base','plot_nodes',app.plotnodesButton.Value);
            assignin('base','plot_solution',app.plotsolutionButton.Value);
            assignin('base','plot_error_function',app.ploterrorfunctionButton.Value);
            assignin('base','compute_SysMatrStats',app.exploresystemmatrixButton.Value);
            assignin('base','use_reduced_matrix',app.usereducedmatrixButton.Value);
            assignin('base','compute_eigenvalues',app.computeeigenvaluesButton.Value);
            assignin('base','plot_eigenvalues',app.ploteigenvaluesButton.Value);
            assignin('base','display_summary',app.displaysummaryButton.Value);
            assignin('base','plot_errors',app.ploterrorsButton.Value);
            assignin('base','plot_cond_stab',app.plotcondstabButton.Value);
          
            %launch the start function of the options app if it is defined
            if ~isempty(app.options_app)
                start(app.options_app);
            end
 
        end

        % Close request function: UIFigure
        function UIFigureCloseRequest(app, event)
            %close options app
            if ~isempty(app.options_app)
                backup_on_close_request(app,app.options_app);
                delete(app.options_app);
            end
            rmpath('gui')
            %close main app
            backup_on_close_request(app,app);
            delete(app);
        end

        % Value changed function: ProblemTypeDropDown
        function ProblemTypeDropDownValueChanged(app, event)
            value = app.ProblemTypeDropDown.Value;
            %%update app.options_app
            if ~isempty(app.options_app)
                backup_on_close_request(app,app.options_app);
                app.ProblemTypeDropDown.Value = value; %restore the current menu value destroyed by previous line
                delete(app.options_app) %close current options app                
            end
            if strcmp(value,'choose problem type')
                app.options_app = [];                
            else                
                options_app_name = [value,'_options_gui'];
                position = app.UIFigure.Position; %current position of the main gui window
                eval([options_app_name,'(app,position)']); %launch the options app
                                                           %this also sets app.options_app
                                                           % because the option apps calls get_options_app
                disp(' ')
                disp(['Start ',class(app.options_app)])                                                          
            end
            
        end

        % Value changed function: exploresystemmatrixButton
        function exploresystemmatrixButtonValueChanged(app, event)
            value = app.exploresystemmatrixButton.Value;
            if value && strcmp(app.exploresystemmatrixButton.Enable,'on')
                app.usereducedmatrixButton.Enable = 'on';
                app.computeeigenvaluesButton.Enable = 'on';
            else
                app.usereducedmatrixButton.Enable = 'off';
                app.computeeigenvaluesButton.Enable = 'off';
            end
            
            computeeigenvaluesButtonValueChanged(app, event)
            
        end

        % Value changed function: computestatisticsButton
        function computestatisticsButtonValueChanged(app, event)
            value = app.computestatisticsButton.Value;
            if value
                app.exploresystemmatrixButton.Enable = 'on';   
                app.plotcondstabButton.Enable = 'on';  
            else
                app.exploresystemmatrixButton.Enable = 'off';
                app.plotcondstabButton.Enable = 'off';
            end
            
            exploresystemmatrixButtonValueChanged(app, event);
            
        end

        % Value changed function: computeeigenvaluesButton
        function computeeigenvaluesButtonValueChanged(app, event)
            value = app.computeeigenvaluesButton.Value;
            if value && strcmp(app.computeeigenvaluesButton.Enable,'on')
                app.ploteigenvaluesButton.Enable = 'on'; 
            else
                app.ploteigenvaluesButton.Enable = 'off';
            end
        end

        % Value changed function: displaysummaryButton
        function displaysummaryButtonValueChanged(app, event)
            value = app.displaysummaryButton.Value;
            if value
                app.ploterrorsButton.Enable = 'on'; 
            else
                app.ploterrorsButton.Enable = 'off';
            end
            
            if value && app.computestatisticsButton.Value
                app.plotcondstabButton.Enable = 'on';
            else
                app.plotcondstabButton.Enable = 'off';
            end
        end
    end

    % App initialization and construction
    methods (Access = private)

        % Create UIFigure and components
        function createComponents(app)

            % Create UIFigure
            app.UIFigure = uifigure;
            app.UIFigure.Position = [100 100 400 500];
            app.UIFigure.Name = 'mFD GUI';
            app.UIFigure.CloseRequestFcn = createCallbackFcn(app, @UIFigureCloseRequest, true);

            % Create StartButton
            app.StartButton = uibutton(app.UIFigure, 'push');
            app.StartButton.ButtonPushedFcn = createCallbackFcn(app, @StartButtonPushed, true);
            app.StartButton.FontSize = 16;
            app.StartButton.FontWeight = 'bold';
            app.StartButton.Position = [119 365 68 34];
            app.StartButton.Text = 'Start';

            % Create AtstartPanel
            app.AtstartPanel = uipanel(app.UIFigure);
            app.AtstartPanel.TitlePosition = 'centertop';
            app.AtstartPanel.Title = 'At start';
            app.AtstartPanel.FontAngle = 'italic';
            app.AtstartPanel.Position = [320 356 61 113];

            % Create longEngButton
            app.longEngButton = uibutton(app.AtstartPanel, 'state');
            app.longEngButton.Text = 'longEng';
            app.longEngButton.Position = [-1 22 61 22];

            % Create closeallButton
            app.closeallButton = uibutton(app.AtstartPanel, 'state');
            app.closeallButton.Text = 'close all';
            app.closeallButton.Position = [-1 43 61 22];

            % Create clearallButton
            app.clearallButton = uibutton(app.AtstartPanel, 'state');
            app.clearallButton.Text = 'clear all';
            app.clearallButton.Position = [-1 66 61 21];
            app.clearallButton.Value = true;

            % Create clcButton
            app.clcButton = uibutton(app.AtstartPanel, 'state');
            app.clcButton.Text = 'clc';
            app.clcButton.Position = [-1 1 61 19];

            % Create ProblemTypeLabel
            app.ProblemTypeLabel = uilabel(app.UIFigure);
            app.ProblemTypeLabel.HorizontalAlignment = 'right';
            app.ProblemTypeLabel.Position = [25 451 84 15];
            app.ProblemTypeLabel.Text = 'Problem Type:';

            % Create ProblemTypeDropDown
            app.ProblemTypeDropDown = uidropdown(app.UIFigure);
            app.ProblemTypeDropDown.Items = {'choose problem type'};
            app.ProblemTypeDropDown.ValueChangedFcn = createCallbackFcn(app, @ProblemTypeDropDownValueChanged, true);
            app.ProblemTypeDropDown.Position = [119 447 138 22];
            app.ProblemTypeDropDown.Value = 'choose problem type';

            % Create PostprocessingPanel
            app.PostprocessingPanel = uipanel(app.UIFigure);
            app.PostprocessingPanel.Title = 'Postprocessing';
            app.PostprocessingPanel.FontAngle = 'italic';
            app.PostprocessingPanel.Position = [33 77 287 240];

            % Create computeeigenvaluesButton
            app.computeeigenvaluesButton = uibutton(app.PostprocessingPanel, 'state');
            app.computeeigenvaluesButton.ValueChangedFcn = createCallbackFcn(app, @computeeigenvaluesButtonValueChanged, true);
            app.computeeigenvaluesButton.Enable = 'off';
            app.computeeigenvaluesButton.Text = 'compute eigenvalues';
            app.computeeigenvaluesButton.Position = [30 87 133 22];

            % Create exploresystemmatrixButton
            app.exploresystemmatrixButton = uibutton(app.PostprocessingPanel, 'state');
            app.exploresystemmatrixButton.ValueChangedFcn = createCallbackFcn(app, @exploresystemmatrixButtonValueChanged, true);
            app.exploresystemmatrixButton.Text = 'explore system matrix';
            app.exploresystemmatrixButton.Position = [8 129 134 22];

            % Create plotsolutionButton
            app.plotsolutionButton = uibutton(app.PostprocessingPanel, 'state');
            app.plotsolutionButton.Text = 'plot solution';
            app.plotsolutionButton.Position = [8 171 134 22];
            app.plotsolutionButton.Value = true;

            % Create ploterrorfunctionButton
            app.ploterrorfunctionButton = uibutton(app.PostprocessingPanel, 'state');
            app.ploterrorfunctionButton.Text = 'plot error function';
            app.ploterrorfunctionButton.Position = [8 150 134 22];
            app.ploterrorfunctionButton.Value = true;

            % Create usereducedmatrixButton
            app.usereducedmatrixButton = uibutton(app.PostprocessingPanel, 'state');
            app.usereducedmatrixButton.Enable = 'off';
            app.usereducedmatrixButton.Text = 'use reduced matrix';
            app.usereducedmatrixButton.Position = [30 108 133 22];
            app.usereducedmatrixButton.Value = true;

            % Create plotnodesButton
            app.plotnodesButton = uibutton(app.PostprocessingPanel, 'state');
            app.plotnodesButton.Text = 'plot nodes';
            app.plotnodesButton.Position = [8 192 134 22];
            app.plotnodesButton.Value = true;

            % Create ploteigenvaluesButton
            app.ploteigenvaluesButton = uibutton(app.PostprocessingPanel, 'state');
            app.ploteigenvaluesButton.Enable = 'off';
            app.ploteigenvaluesButton.Text = 'plot eigenvalues';
            app.ploteigenvaluesButton.Position = [53 66 133 22];

            % Create figs50Label
            app.figs50Label = uilabel(app.PostprocessingPanel);
            app.figs50Label.FontAngle = 'italic';
            app.figs50Label.Position = [161 196 48 15];
            app.figs50Label.Text = 'figs 50+';

            % Create figs1020Label
            app.figs1020Label = uilabel(app.PostprocessingPanel);
            app.figs1020Label.FontAngle = 'italic';
            app.figs1020Label.Position = [161 175 72 15];
            app.figs1020Label.Text = 'figs 10+ 20+';

            % Create figs3040Label
            app.figs3040Label = uilabel(app.PostprocessingPanel);
            app.figs3040Label.FontAngle = 'italic';
            app.figs3040Label.Position = [160 154 72 15];
            app.figs3040Label.Text = 'figs 30+ 40+';

            % Create figs60Label
            app.figs60Label = uilabel(app.PostprocessingPanel);
            app.figs60Label.FontAngle = 'italic';
            app.figs60Label.Position = [202 70 48 15];
            app.figs60Label.Text = 'figs 60+';

            % Create displaysummaryButton
            app.displaysummaryButton = uibutton(app.PostprocessingPanel, 'state');
            app.displaysummaryButton.ValueChangedFcn = createCallbackFcn(app, @displaysummaryButtonValueChanged, true);
            app.displaysummaryButton.Text = 'display summary';
            app.displaysummaryButton.Position = [8 45 134 22];
            app.displaysummaryButton.Value = true;

            % Create ploterrorsButton
            app.ploterrorsButton = uibutton(app.PostprocessingPanel, 'state');
            app.ploterrorsButton.Text = 'plot errors';
            app.ploterrorsButton.Position = [30 24 134 22];
            app.ploterrorsButton.Value = true;

            % Create plotcondstabButton
            app.plotcondstabButton = uibutton(app.PostprocessingPanel, 'state');
            app.plotcondstabButton.Text = 'plot cond, stab';
            app.plotcondstabButton.Position = [30 3 134 22];
            app.plotcondstabButton.Value = true;

            % Create figs100102103Label
            app.figs100102103Label = uilabel(app.PostprocessingPanel);
            app.figs100102103Label.FontAngle = 'italic';
            app.figs100102103Label.Position = [185 28 101 15];
            app.figs100102103Label.Text = 'figs 100, 102, 103';

            % Create fig101Label
            app.fig101Label = uilabel(app.PostprocessingPanel);
            app.fig101Label.FontAngle = 'italic';
            app.fig101Label.Position = [185 7 42 15];
            app.fig101Label.Text = 'fig 101';

            % Create computestatisticsButton
            app.computestatisticsButton = uibutton(app.UIFigure, 'state');
            app.computestatisticsButton.ValueChangedFcn = createCallbackFcn(app, @computestatisticsButtonValueChanged, true);
            app.computestatisticsButton.VerticalAlignment = 'top';
            app.computestatisticsButton.Text = 'compute statistics';
            app.computestatisticsButton.Position = [143 296 133 21];
            app.computestatisticsButton.Value = true;
        end
    end

    methods (Access = public)

        % Construct app
        function app = mFD_gui

            % Create and configure components
            createComponents(app)

            % Register the app with App Designer
            registerApp(app, app.UIFigure)

            % Execute the startup function
            runStartupFcn(app, @startupFcn)

            if nargout == 0
                clear app
            end
        end

        % Code that executes before app deletion
        function delete(app)

            % Delete UIFigure when app is deleted
            delete(app.UIFigure)
        end
    end
end
