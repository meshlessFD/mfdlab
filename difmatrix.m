function [A] = difmatrix(prob,nodes,sten,II,use_selection_weights)
% Function difmatrix: differentiation matrix
%
% Compute the differentiation matrix for nodes in nodes.X(II.:) by the numerical differentiation method given in sten.difweights 
% (which implicitly sets the differential operator, too) using nodes selected by sten.select.
% Returnes the differentiation matrix A in sparse format of the size n x n, where n is the total number of nodes in nodes.X
%
% INPUT
%
% prob -- structure to define the problem to be solved, see respective setup_problem*.m file            
% nodes -- structure for discretization nodes
%        nodes.X  -- (n x d)-matrix of n points in R^d
%        nodes.ii -- indices of interior nodes in Omega into the rows of X
%        nodes.ib -- indices of boundary nodes on Gamma into the rows of X 
%        nodes.ns -- NeighborSearcher object created by ns = createns(X);
%        nodes.m -- number of nearest neighbors for initial search
%        nodes.neigh, nodes.dists -- sten.m nearest neighbors and distances found for all points in advance 
%                              (i-th row of neigh contains nodes.m nearest points in X to the i-th point in X)
% sten -- structure for the selection of stencil supports
%        sten.select -- function for the selection of the influence set
%              Syntax: either [si,weights,nodes] = function(prob,nodes,sten,i) or  si = function(prob,nodes,sten,i),
%                      depending on the flag use_selection_weights
%              INPUT:  i -- index into nodes.X of the for which we seek neighbors              
%              OUTPUT: si -- indices into nodes.X of the selected neighbors
%       sten.difweights -- function for the computation of numerical differentiation weights
%              Syntax: weights = function(nodes,prob,i,si) 
% II   -- (row of) the indices into the rows of nodes.X for those nodes where numerical differentiation matrix is to be computed
% use_selection_weights -- flag whether the weights produced by selection routine should be also used to in the 
%                          differentiation matrix (1: use; 0: no, compute the weights with sten.difweights)
%
% OUTPUT
%
% A --  the sparse differentiation matrix of size n x n, where n is the total number of nodes in nodes.X
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2017-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

%% initialization for efficient allocation of the sparse matrix

%preallocate the arrays for indices and entries of the sparse differentiation matrix
%number of nonzeros
numnz = length(II)*sten.num;
%their row and column indices
I = nan(1,numnz);
J = nan(1,numnz);
%values
V = nan(1,numnz);

%running index into I,J,V
ind = 1;


%% loop over nodes in ii, fill in the arrays I,J,V
        
%decide here to avoid checking this within the loop
if use_selection_weights %call a selection routine that also delivers weights (e.g pQR)
    
    for i = II
        
        %select stencil support and weights
        [si,weights,nodes] = sten.select(prob,nodes,sten,i); %si: indices into nodes.X of the selected neighbors for i-th node
        
        %%debugging
        %figure(301),plotp(nodes.X,'.'), hold on, plotp(nodes.X(si,:),'ro'), hold on, plotp(nodes.X(si(1),:),'*r'),hold off,title([int2str(length(si)),' nodes'])
        
        
        
        %%fill in arrays I,J,V
        si_num = length(si);
        
        %find the index into I,J,V for the current stencil
        ind_end = ind+si_num-1;
        
        %fill in the current portion of I
        I(ind:ind_end) = i*ones(1,si_num);
        
        %fill in the current portion of J
        J(ind:ind_end) = si;
        
        %fill in the current portion of V
        V(ind:ind_end) = weights;
        
        %update ind for the next iteration
        ind = ind_end + 1;
        
        
    end
    
else %call a selection routine and then a weights calculation routine
    
    for i = II
        
        %select stencil support
        si = sten.select(prob,nodes,sten,i); %indices into nodes.X of the selected neighbors for i-th node
        
        %compute stencil weights
        weights = sten.difweights(nodes,prob,i,si);
        
        %%fill in arrays I,J,V
        si_num = length(si);
        
        %find the index into I,J,V for the current stencil
        ind_end = ind+si_num-1;
        
        %fill in the current portion of I
        I(ind:ind_end) = i*ones(1,si_num);
        
        %fill in the current portion of J
        J(ind:ind_end) = si;
        
        %fill in the current portion of V
        V(ind:ind_end) = weights;
        
        %update ind for the next iteration
        ind = ind_end + 1;
        
        
    end
    
    
    
end

%% remove remaining positions in I,J,V
I(ind:end) = [];
J(ind:end) = [];
V(ind:end) = [];


%% create the sparse matrix
n = size(nodes.X,1);
A = sparse(I,J,V,n,n);





