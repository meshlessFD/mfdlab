function [nodes] = generate_inodes_qprand (a,qrand_type,spacing,domain)
% Function generate_inodes_qprand: interior nodes via quasi- or pseudo-random number generators
%
% Generate interior nodes as a quasi- or pseudo-random set (Halton or Sobol sequence 
% or pseude-random numbers) with given target 'spacing'.
%
% INPUT
%
% a - minimum distance from the boundary for the interior nodes to be created
% qrand_type - type of quasi- or pseudo-random points
% spacing - target grid spacing
% domain - structure describing the domain we generate the nodes for
%
% OUTPUT
%
% nodes -- the nodes structure
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%read domain dimensionality
dim = domain.dim; 

%create the qrandset object
switch qrand_type
    case {'halton','sobol'}
        P = qrandstream(qrand_type,dim);
    case 'rand'
        P = RandStream('mt19937ar','Seed',0); %default setting for 'rand' in MATLAB 2017b
end

% determine the size of the halton sequence according to the desired spacing
% (first find domain's maximum coordinate width
if isfield(domain,'bb') %using tight bounding box
    domain_width = max(domain.bb(2,:) - domain.bb(1,:));
else
    error('need bounding box of this domain')
end
% size of the quasi-random set for the bounding box
N = round((domain_width/spacing)^dim);

% choose quasi-random points and scale and translate them to cover the bounding box
%X = domain_width*net(P,N) + domain.bb(1,:);
X = domain_width*rand(P,N,dim) + domain.bb(1,:);

%remove points outside the bounding box (in particular in order to reduce work for domain.sdf)
X(any(X<domain.bb(1,:) | X>domain.bb(2,:),2),:)=[];

disp(['interior nodes: ',qrand_type,' set of ',int2str(size(X,1)),' points in the bounding box'])


% retain as interior only nodes at the distance at least a*spacing from the boundary.         
%Some distance is needed at least for RBF methods that are sensitive to the separation distance of nodes.
%a = .25; %0.01;% 1/4;%
lind_nodes = domain.sdf(X) < -a*spacing - 100*eps; %logical indices of grid nodes to be retained (into rows of the full grid)
X = X(lind_nodes,:);

disp(['  remove nodes with distance from boundary < ',num2str(a),'*spacing'])

%indices of interior nodes (all retained grid points)
ii = 1:size(X,1);

% generate 'nodes' structure
% h - actual grid step
% spacing  - target spacing of the nodes, may be slightly different from h
nodes = struct('itype',qrand_type,'spacing',spacing,'X',X,'ii',ii);

