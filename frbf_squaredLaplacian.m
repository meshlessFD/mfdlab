function y = frbf_squaredLaplacian (t, dim, type, par)
% Function frbf_squaredLaplacian:  squared Laplacian of kernel in f-form
%
% Compute the squared Laplacian (Delta^x)^2 K(x,y) or mixed Laplacian Delta^x Delta^y K(x,y), with t=||x-y||^2/2, 
% of the dim-dimensional RBF kernel in f-form.
% To accomodate for the shape parameter ep, use with t=ep^2||x||^2/2 resp. t=ep^2||x-y||^2/2 and multiply y by ep^4.
%
% INPUT
%
% t -- array of nonnegative input real values e.g. \|x-x_j\|^2 /2
% dim -- number of space dimensions
% type -- a string defining the type of RBF, see frbf.m for type description
% par -- parameter of the RBF of particular type, see frbf.m (NO shape parameter)
%
% OUTPUT
%
% y -- array of output real values (the same size as t)
%
% 
% INFO
%
% See [1] R. Schaback, MATLAB Programming for Kernel-Based Methods, technical report, 2011,
% available from http://num.math.uni-goettingen.de/schaback/research/group.html
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2011 Robert Schaback
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
% Code derived from Section 3.5.2 of [1].
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

y = (dim+2)*(dim*frbf(t, 2, type, par) + 4*t.*frbf(t, 3, type, par)) + 4*t.^2.*frbf(t, 4, type, par);
