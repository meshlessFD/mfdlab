% Script script_summary_Laplace: summary of the experiment 
%
% Display a summary of the experiment run by script_mFD_Laplace.m. Called at the end of script_mFD_Laplace.m, but
% can be run independently after loading results_mFD_Laplace.mat or results_mFD_Laplace_part.mat
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2019-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.



format shortG


%% display summary  
disp(' ')
disp('-----Summary-------------')

disp(' ')
disp('spacing')
disp(spacing_array')

disp('number of degrees of freedom: interior and Neumann (if any) nodes')
disp(ndf_array')


if compute_statistics
    
     
    disp('average Laplace stencil size')
    disp(av_Laplace_stencil_size_array')
    if strcmp(prob.bctype,'Neumann') || strcmp(prob.bctype,'mixed')
        disp('average Neumann stencil size')
        disp(av_Neumann_stencil_size_array')
    end
   
    
    fprintf('max Laplace weights computation condition numbers\n')
    fprintf('   %7.1e',maxcondLaplace_array)
    disp(' ')
    
    if strcmp(prob.bctype,'Neumann') || strcmp(prob.bctype,'mixed')
        fprintf('\nmax Neumann weights computation condition numbers\n')
        fprintf('   %7.1e',maxcondNeumann_array')
        disp(' ')
    end
    
    disp(' ')
    disp('system matrix condition numbers and increase factors')
    if use_reduced_matrix
        fprintf('(for reduced matrix)')
    end
    %disp([condSys_array condSys_array./[nan; condSys_array(1:end-1,:)]]')
    fprintfm('   %7.1e',[condSys_array condSys_array./[nan; condSys_array(1:end-1,:)]])
    
    
    disp(' ')
    disp('inf stability constant')
    if use_reduced_matrix
        disp('(for reduced matrix)')
    end
    disp(invSMnorm_array')
%     fprintf('   %7.1e',invSMnorm_array')
%     disp(' ')
 
    %table(spacing_array,rms_array)
    
    if compute_SysMatrStats %isfield(mFDstats,'System_Matrix') && isfield(mFDstats,'rhs')
%        disp(' ')
        disp('Cuthill-McKee bandwidth')
        if use_reduced_matrix
            disp('(for reduced matrix)')
        end
        disp(bandwidth_rcm_array')
%         fprintf('   %7.1e',bandwidth_rcm_array)
%         disp(' ')
        
        if compute_eigenvalues    

            %format compact
            %disp(' ')
            disp('max real part of eigenvalues')
            disp(EG_max_real_array')
%             fprintf('   %7.1e',EG_max_real_array')
%             disp(' ')
            %         disp('min real part of eigenvalues')
            %         disp(EG_min_real_array')
            disp('max abs of imaginary part of eigenvalues')
            disp(EG_max_abs_imag_array')
            
%             fprintf('   %7.1e',EG_max_abs_imag_array')
%             disp(' ')
            %         disp('eigenvalues computed by')
            %         disp(EG_method')
            %format loose
        end
        
        
        
%         disp('max residual and decrease factors')
%         disp([max_resid_array [nan; max_resid_array(1:end-1,:)]./max_resid_array])
%         disp('rms residual and decrease factors')
%         disp([rms_resid_array [nan; rms_resid_array(1:end-1,:)]./rms_resid_array])
        
        %disp(' ')
        disp('residual and decrease factors')
        %disp([maxer_array [nan; maxer_array(1:end-1,:)]./maxer_array])
        fprintf('      rms       decr.          max      decr.  \n')
        fprintf('    %7.1e    %5.1f        %7.1e    %5.1f\n',[rms_resid_array [nan; rms_resid_array(1:end-1,:)]./rms_resid_array...
            max_resid_array [nan; max_resid_array(1:end-1,:)]./max_resid_array]')
        disp(' ')
    end
    
end %end of if compute_statistics

if isfield(prob,'rs') %check that a reference solution is defined, otherwise the errors could not be computed
    
    disp('error and decrease factors')
    %disp([maxer_array [nan; maxer_array(1:end-1,:)]./maxer_array])
    fprintf('      rms       decr.          max      decr.  \n')
    fprintf('    %7.1e    %5.1f        %7.1e    %5.1f\n',[rms_array [nan; rms_array(1:end-1,:)]./rms_array...
        maxer_array [nan; maxer_array(1:end-1,:)]./maxer_array]')
    % disp('rms errors and decrease factors')
    % disp([rms_array [nan; rms_array(1:end-1,:)]./rms_array])
    disp(' ')
    disp('Info: orders from decrease factors')
    disp(['h: ',num2str(ref_factor),';  h^2: ',num2str(ref_factor^2),';  h^3: ',num2str(ref_factor^3),...
        ';  h^4: ',num2str(ref_factor^4),';  h^5: ',num2str(ref_factor^5)])
end



%% Add plots

%skip if there are no refinements
if ~nref 
    return;
end

format

%determine expected approximation order of the method based on selection and weights methods
if strcmp(fd_on_grid,'fd_star') && strcmp(nodestype_int,'grid')
    approx_order = 4; %'fd_star' is the standard (2*dim+1)-star stencil exact for polynomilas of order 4
elseif Luse_selection_weights %&& exist('Lsel_order','var')
    approx_order = Lsel_order;
elseif ismember(Lwtype,{'Power/TPS','L1', 'L2'})
    approx_order = Lporder; %approximation driven by the polynomial part of RBF+poly sum
elseif strcmp(Lselection_method,'L1')
    approx_order = Lsel_order;    
else %Lselection_method=nearest and remaining RBFs    
    %maximum order of polynomials of dim variables with dimension less or equal LsizeSI
    %WARNING: we only look at polynomial order up to 9
    approx_order = find([1, nchoosek(dim+1,dim), nchoosek(dim+2,dim), nchoosek(dim+3,dim), nchoosek(dim+4,dim),...
        nchoosek(dim+5,dim), nchoosek(dim+6,dim), nchoosek(dim+7,dim), nchoosek(dim+8,dim)] - LsizeSI < .5,1,'last');
    
    %approximation order may be reduced due to low smoothness of rbf (for 'Matern' and 'Wendland')
    if strcmp(Lwtype,'Matern')
        approx_order = min(approx_order,Lrbfpar);
    elseif strcmp(Lwtype,'Wendland')
        approx_order = min(approx_order,Lrbfpar(2));
    end
    
    %approximation order may be driven by the polynomial part of RBF+poly sum
    approx_order = max(approx_order,Lporder);    
end

%expected convergence order of error 
expected_order = approx_order-2;
%superconvergence order
superconv_order = expected_order+1;

if compute_statistics
    
    %plot condition number and inf-stability constant
    if plot_cond_stab
        figure(101)
        subplot(2,1,1)
        loglog(ndf_array,condSys_array,'k')
        hold on
        %expected_order = -(approx_order-2);
        %loglog(spacing_array,spacing_array.^expected_order/spacing_array(1)^expected_order,'--')
        loglog(ndf_array,ndf_array.^(expected_order/dim)/ndf_array(1)^(expected_order/dim)*condSys_array(1),'r--')%,'r--')%
        loglog(ndf_array,ndf_array.^(superconv_order/dim)/ndf_array(1)^(superconv_order/dim)*condSys_array(1),'r-.')%,'r-.')%
        hold off
        %title(['inf-cond number: d=',int2str(dim),', q=',int2str(approx_order)])
        %     title('inf-cond number of system matrix')
        %     legend('cond',['(N^{1/d})^',int2str(expected_order)]','Location','northwest')
        %xlabel('N: number of degrees of freedom')
        %modify figure(101)
        title('inf cond number of system matrix')
        legend('cond',['(N^{1/d})^',int2str(expected_order)]',['(N^{1/d})^',int2str(superconv_order)]','Location','northwest')
        
        subplot(2,1,2)
        %hold on
        semilogx(ndf_array,invSMnorm_array,'-ob')
        %hold off
        %title(['inf-cond number and stability constant: d=',int2str(dim),', q=',int2str(approx_order)])
        xlabel('N: number of degrees of freedom')
        title('inf stability constant of system matrix')
        legend('stab','Location','northwest')
    end
    
    %plot stability constant and residual
    if compute_SysMatrStats && plot_errors %isfield(mFDstats,'System_Matrix') && isfield(mFDstats,'rhs')
        
          
        
        %plot residual
        figure(102)
        %semilogy(spacing_array,rms_array)
        %loglog(spacing_array,rms_array)
        loglog(ndf_array,rms_resid_array,'b')
        hold on
        loglog(ndf_array,max_resid_array,'k')
        %loglog(spacing_array,spacing_array.^expected_order/spacing_array(1)^expected_order,'--')
        loglog(ndf_array,ndf_array.^(-expected_order/dim)/ndf_array(1)^(-expected_order/dim)*rms_resid_array(1),'r--')%,'r--')%
        loglog(ndf_array,ndf_array.^(-superconv_order/dim)/ndf_array(1)^(-superconv_order/dim)*rms_resid_array(1),'r-.')%,'r-.')%
        hold off
        %xlabel('h')
        xlabel('N: number of degrees of freedom')
        %title(['rms and max residual: d=',int2str(dim),', q=',int2str(approx_order)])
        title('rms and max residual')
        %legend('method',['h^',int2strexpected_order]','Location','northeast')
        legend('rms','max',['(N^{-1/d})^',int2str(expected_order)]',['(N^{-1/d})^',int2str(superconv_order)]','Location','northeast')
        
        
        
        %plot estimate
        maxest = invSMnorm_array.*max_resid_array;
        figure(103)
        loglog(ndf_array,maxest,'k')
        hold on
        %loglog(spacing_array,spacing_array.^expected_order/spacing_array(1)^expected_order,'--')
        loglog(ndf_array,ndf_array.^(-expected_order/dim)/ndf_array(1)^(-expected_order/dim)*maxest(1),'r--')%,'r--')%
        loglog(ndf_array,ndf_array.^(-superconv_order/dim)/ndf_array(1)^(-superconv_order/dim)*maxest(1),'r-.')%,'r-.')%
        hold off
        xlabel('N: number of degrees of freedom')
        %title(['max error estimate: d=',int2str(dim),', q=',int2str(approx_order)])
        title('max error estimate (stab.const*residual)')
        legend('estimate',['(N^{-1/d})^',int2str(expected_order)]',['(N^{-1/d})^',int2str(superconv_order)]','Location','northeast')
        
        %plot max reral eigenvalues into figure 101
        if plot_cond_stab && compute_eigenvalues
            
            
   
            
            figure(101)
            subplot(2,1,2)
            hold on
            lstrings = {'stab'};
            %plot good (negative) EG_max_real
            lind = EG_max_real_array < -10000*eps;
            %semilogx(ndf_array,EG_max_real_array,'m')
            semilogx(ndf_array(lind),EG_max_real_array(lind),'o-m')
            %             semilogx(ndf_array,repmat(-1,1,length(ndf_array)),'og')
            %             text(ndf_array,repmat(-1,1,length(ndf_array)),'-1')
            %             ax = axis;
            %             %            axis(ax-[0 0 1 0])
            %             ax(3)=-2;
            %             axis(ax+[0 0 0 1])
            if any(lind)
                lstrings = [lstrings,'max \Re(eig)'];
                %legend('stab','max \Re(eig)')
            end
            
            %display the cases of bad max real eigenvalues
            if any(~lind)
                ind = find(~lind)';
                for i = ind
                    plot(ndf_array(i),-.5,'.m')
                    text(ndf_array(i),-.5,num2str(EG_max_real_array(i),'%2.1e'),'HorizontalAlignment','center','Color','magenta')
                end
                lstrings = [lstrings,'bad max \Re(eig)'];
                %legend('stab','max \Re(eig)','bad max \Re(eig)') 
            end
            hold off
            ax = axis;
            ax(3) = ax(3)-max(.5,max(ax(4))/10);
            axis(ax)
            title('inf stability constant and max real part of eigenvalues of system matrix')
            legend(lstrings)
        end
    end
    
end

%show the figure with error plots at the end
if plot_errors && isfield(prob,'rs')
    
    %plot rms and max error on interior nodes
    figure(100)
    %semilogy(spacing_array,rms_array)
    %loglog(spacing_array,rms_array)
    loglog(ndf_array,rms_array,'b')
    hold on
    loglog(ndf_array,maxer_array,'k')
    %loglog(spacing_array,spacing_array.^expected_order/spacing_array(1)^expected_order,'--')
    loglog(ndf_array,ndf_array.^(-expected_order/dim)/ndf_array(1)^(-expected_order/dim)*rms_array(1),'r--')%,'r--')%
    loglog(ndf_array,ndf_array.^(-superconv_order/dim)/ndf_array(1)^(-superconv_order/dim)*rms_array(1),'r-.')%,'r-.')%
    hold off
    %xlabel('h')
    xlabel('N: number of degrees of freedom')
    %title(['rms and max error (interior nodes): d=',int2str(dim),', q=',int2str(approx_order)])
    title('rms and max error (interior nodes)')
    %legend('method',['h^',int2strexpected_order]','Location','northeast')
    legend('rms','max',['(N^{-1/d})^',int2str(expected_order)]',['(N^{-1/d})^',int2str(superconv_order)]','Location','northeast')
    
end

