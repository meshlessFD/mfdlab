function [si, weights,nodes]  = Select_LaplaceL1(i,prob,nodes,sten,porder,mu,initcloud_size,try_positive)
% Function Select_LaplaceL1: set of influence for Laplace operator at the i-th node using L1 stencils
%
% Select a set of influence for the numerical differentiation of Laplace operator at the i-th node by computing 
% the sparse L1-weights using L1LaplaceStencil. In addition, the L1 weights are returned that can be used as if
% produced by L1LaplaceWeights
% 
% INPUT 
%
% i - index of the current node into the rows of nodes.X
% prob - the prob structure
% nodes -- the nodes structure
% sten -- the sten structure
% porder -- polynomial order (degree+1) (integer >= 1)
% mu -- power of the L1 weights (real number >= 0) 
% initcloud_size -- the size of the initial set of neighbors to apply L1LaplaceStencil
% try_positive -- flag to attempt computing a positive stencil, see the info in L1Stencil.m.
%             0: no attempt to find positive stencils
%             1: run L1LaplaceStencil again with positive=0 if it fails, i.e., then an ordinary stencil is computed. 
%             NOTE: since positive stencils for Laplacian do not exist when porder > 4, this flag should be set to 0 in this case
%
% OUTPUT
%
% si - vector of indices of the set of influence into the rows of nodes.X
% weights -- a column vector of stencil weights for the Laplacian
% nodes -- the nodes structure
%
%
% INFO
%
% See L1LaplaceStencil.m, L1Stencil.m and L1LaplaceWeights.m for further details
%
% Selection of supports of positive stencils with subsequent computation of numerical diferentiation weights by multiquadric RBF
% has been sugggested by V. Bayona, M. Moscoso, and M. Kindelan. Optimal constant shape parameter for multiquadric based 
% RBF-FD method. J. Comput. Phys., 230(19):7384–7399, 2011.
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2019-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


% remove invisible points if the domain is non-convex and domain.dpf is available
[neigh,~] = remove_invisible_neighbors(i, prob,nodes); %returns updated neigh and dists if it updates them
%neigh = nodes.neigh(i,:);

% reduce initcloud_size and update neigh if there are too few neighbors
initcloud_size = min(initcloud_size, length(neigh));
neigh = neigh(1:initcloud_size);


%evaluation point
z = nodes.X(i,:);

%%L1 min stencils on the full set of neighbors in neigh
%try to find a positive stencil if try_positive=true
[w, grf, condn, supp] = L1LaplaceStencil(z, nodes.X(neigh,:), porder, mu, try_positive);
if any(isnan(w)) %if positive stencil cannot be found, then drop this condition and recompute;
    [w, grf, condn, supp] = L1LaplaceStencil(z, nodes.X(neigh,:), porder, mu,0);
    if any(w(2:end)<-100*eps) %~any(isnan(w))
        %update the number of non-positive stencils
        updateSTATS('sum','np_stencils_sel',1); 
        %disp(['positive stencil not found for i=',int2str(i)])
    end
end

%collect info about condition numbers
updateSTATS('max','maxcondLaplace_sel',condn);
        
%return support and weights        
si = neigh(supp);
weights = w(supp);

