function mat = kermat (X, Y, type, par, ep)
% Function kermat: kernel matrix for two point sets X and Y
%
% Creates RBF kernel matrix K(x_i,y_j) for two point sets X and Y in R^d.
%
% INPUT
%
% X -- an (m x d)-matrix of m points in R^d
% Y -- an (n x d)-matrix of n points in R^d
% type -- a string defining the type of RBF, see frbf.m for type description
% par -- parameter of the RBF of particular type
% ep -- shape parameter
%
% OUTPUT
%
% mat -- an (nx x ny)-matrix of kernel evaluations on point pairs (x_i,y_j) from X and Y
%
% WARNING: points in X and Y must have the same spacial dimension d
%
%
% INFO
%
% See [1] R. Schaback, MATLAB Programming for Kernel-Based Methods, technical report, 2011,
% available from http://num.math.uni-goettingen.de/schaback/research/group.html
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2011 Robert Schaback
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
% Adapted from kermat of [1].
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


mat = frbf (distsqh (ep*X, ep*Y), 0, type, par);
