function [weights,condn] = RbfDer1Stencil(z,X,type,par,ep,porder,direction)
% Function RbfDer1Stencil: weights of RBF numerical differentiation formula for a first order directional derivative 
%
% Compute the weights of RBF numerical differentiation formula for a first order 
% directional derivative (defined by vector direction, not normalized) at a point z in R^d, 
% given function values at X\subset R^d, using RBF interpolation, optionally with a polynomial term.
%
% INPUT
%
% z -- a row d-vector representing a point in R^d where the derivative is evaluated
% X -- an (nx x d)-matrix of nx points in R^d
% type -- a string defining the type of RBF, see frbf.m for type description.
%         In addition to the types of frbf.m accepts 'gQR' (Gauss-QR) for d=2 and porder=0 
% par -- parameter of the RBF of particular type
% ep -- shape parameter
% porder -- order of the polynomial part  
% direction -- row (1 x d)-vector that defines the direction of differentiation 
%              (not necessarily a unit vector)
%
% OUTPUT
%
% weights -- an (nx x 1)-vector (column)
% condn -- condition number of the kernel matrix, see  RbfStencil.m
%
%
% INFO
%
% See comments in RbfStencil.m and Gauss_QR_diffmat_2D.m
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


[nx, dim] = size(X);

%% Gauss-QR weights
if strcmp(type,'gQR')
    
    if porder > 0
        error('porder not implemented')
    end
      
    if dim == 2   
        
        %compute weights for the gradient
        [wcell,Psi] = Gauss_QR_diffmat_2D({'x','y'},z,X,ep);    
        weights = cell2mat(wcell)';
        weights = reshape(weights,nx,[]);
        
        %compute weights for the directional derivative
        weights = weights*direction';
        
        condn = cond(Psi.A0);
        
    else        
        error('dim not implemented')        
    end
    
    return;
end

%% set up the function for the evaluation of the RBF part of the right hand side
rhsf = @(z,X,type,par,ep) gradkermatX(z,X,type,par,ep)*direction';

%% compute the polynomial part cz of the rhs (assuming evaluation of the weights at the origin)

%dimension of polynomials of given order in d variables
if porder > 0
    polydim = nchoosek(porder+dim-1,dim);
else
    polydim = 0;
end

cz = zeros(polydim,1);
if porder > 1
    cz(2:dim+1,:) = direction';
end

%% call RbfStencil.m to compute the weights
[weights,~,condn] = RbfStencil(z,X,type,par,ep,porder,rhsf,cz,1);

% %%alternative computation via gradient weights:sometimes seems better than above, sometimes fails 
% %%since points selected can be bad for gradient (e.g. grid for a box, where good points for a
% %%directional derivative may be on a straight line)
%
% %compute gradient weights matrix
% [weights,condn] = RbfGradientStencil(z,X,type,par,ep,porder);
% 
% %compute weights for the directional derivative
% weights = weights*direction';

