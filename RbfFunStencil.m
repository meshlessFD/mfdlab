function [weights,condn] = RbfFunStencil(z,X,type,par,ep,porder)
% Function RbfFunStencil: weights for RBF interpolation 
%
% Compute the weights of the a formular for the evaluation of the RBF interpolant 
% (with an optional polynomial term) at a point z\in R^d, given function values at X\subset R^d.
%
% INPUT
%
% z -- a row d-vector representing a point in R^d where RBF interpolant is evaluated
% X -- an (nx x d)-matrix of np points in R^d
% type -- a string defining the type of RBF, see frbf.m for type description;
%         in addition to the types of frbf.m accepts 'gQR' (Gauss-QR) for d=2 and porder=0
% par -- parameter of the RBF of particular type
% ep -- shape parameter
% porder -- order of the polynomial part 
%
% OUTPUT
%
% weights -- an (nx x 1)-vector (column)
% condn -- condition number of the kernel matrix used to compute the stencil, see  RbfStencil.m 
%
%
% INFO
%
% See comments in RbfStencil.m and Gauss_QR_diffmat_2D.m
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2014-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

dim = size(X,2);
% if length(z) ~= dim
%     error('points of unequal dimensions')
% end

% Gauss-QR method
if strcmp(type,'gQR')
    
    if porder > 0
        error('porder not implemented')
    end
    
    if dim == 2
        [weights,Psi] = Gauss_QR_diffmat_2D('1',z,X,ep);
        weights = weights';
        condn = cond(Psi.A0);
    else
        error('dim not implemented')
    end
    
    return;
end

%% set up the function for the evaluation of the RBF part of the right hand side
rhsf = @(z,X,type,par,ep) frbf(distsqh (z, X)*ep^2, 0, type, par)'; 

%% compute the polynomial part cz of the rhs (assuming evaluation of the monomials at the origin)
% this works bacause all monomials of order > 1 are shifted and scaled in RbfStencil.m

%dimension of polynomials of given order in d variables
if porder > 0
    polydim = nchoosek(porder+dim-1,dim);
else
    polydim = 0;
end
 
cz = [1; zeros(polydim-1,1)];

%% call RbfStencil.m to compute the weights
[weights,~,condn] = RbfStencil(z,X,type,par,ep,porder,rhsf,cz,0);


