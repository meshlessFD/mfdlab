function [nodes] = generate_inodes_grid (a,spacing,domain)
% Function generate_inodes_grid: interior nodes from a uniform Cartesian grid
%
% Generates interior nodes as a uniform Cartesian grid with grid step 'spacing'.
% In addition computes grid neighbors of the nodes.
% WARNING: for unit cube also returns boundary nodes (of the same grid)
%
% INPUT
%
% a - minimum distance from the boundary for the interior nodes to be created (except 'unit cube')
% spacing - target grid spacing
% domain - structure describing the domain we generate the nodes for
%
% OUTPUT
%
% nodes -- the nodes structure
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.



%read domain dimensionality
dim = domain.dim; 


%% determine the (one-dimensional) grid size 'gridpar' and grid step size 'h'
%we first find domain's maximum coordinate width
switch domain.id
    case 'unit cube'
        domain_width = 1;
        
    case 'unit ball'
        domain_width = 2;
        
    case 'box'        
        domain_width = max(domain.c2 - domain.c1); %maximum coordinate width
        
    case 'ball'
        domain_width = 2*domain.radius;    
        
    otherwise
        if isfield(domain,'bb') 
            %TODO: for this to work add a bounding box field 'bb' to domain structure 
            domain_width = max(domain.bb(2,:) - domain.bb(1,:)); 
        else
            error('need more info to determine grid size for this domain')
        end

end
gridpar = round(domain_width/spacing);
h = domain_width/gridpar; %grid step size

disp(['interior nodes: grid with step size h=',num2str(h)])

%% generate an integer grid ordered lexicographically
if dim==1
    X=(0:gridpar)';
elseif dim==2
    [x,y] = ndgrid(0:gridpar);
    X = [x(:), y(:)];
    clear x y
elseif dim==3
    [x,y,z] = ndgrid(0:gridpar);
    X = [x(:), y(:), z(:)];
    clear x y z
elseif dim==4
    [x,y,z,w] = ndgrid(0:gridpar);
    X = [x(:), y(:), z(:), w(:)];
    clear x y z w
elseif dim==5
    [x1,x2,x3,x4,x5] = ndgrid(0:gridpar);
    X = [x1(:), x2(:), x3(:), x4(:), x5(:)];
    clear x1 x2 x3 x4 x5
else
    error('dim>5 not yet implemented') %use ndgrid
end


%%  Move and scale the nodes to cover the domain, and filter out nodes outside the domain 
%(except for 'unit box' we also remove nodes that are too close to the boundary).
% Also determine logical indices 'lind_nodes' of grid nodes to be retained into rows of the full grid 
% (needed for grid neighbors below)
switch domain.id
    
    case 'unit cube'
        %nothing to do for 'unit cube': simply retain all nodes 
        %(in particular no scaling yet because we want to use integer comparisons to find boundary nodes)
        disp('boundary nodes: grid')
        
        %find boundary nodes
        ib = find(min(X,[],2)==0 | max(X,[],2)==gridpar)';
        
        %find interior nodes
        ii = setdiff(1:size(X,1),ib);
        
        %scale the points into [0,1]^d
        X = X/gridpar;
        
        %logical indices of all nodes
        lind_nodes = true(size(X,1),1);
        
    otherwise
        
        %scale the nodes into the bounding box [-1,1]^d of the unit ball
        %X = 2*X/gridpar - 1;
        
        %scale the nodes into the bounding box of the ball    
        if ~isfield(domain,'bb')
            error('need bounding box for this domain')
        end
        %X = h*X + domain.bb(1,:);
        X = domain_width*X/gridpar + domain.bb(1,:); %this is very slightly more accurate
        
        %remove points outside the bounding box (in particular in order to reduce work for domain.sdf)
        lind_nodes = ~any(X<domain.bb(1,:) | X>domain.bb(2,:),2); %logical indices of grid nodes within BB (into rows of the full grid X)
        X = X(lind_nodes,:); %X now only contains nodes in BB
        
        if isfield(domain,'sdf')
            
            %retain as interior only nodes at the distance at least a*h from the boundary (h is the grid stepsize).
            %Some distance is needed at least for RBF methods that are sensitive to the separation distance of nodes.
            %a = .25; %0.01;% 1/4;%
            lind_nodes_intobb = domain.sdf(X) < -a*h - 100*eps; %logical indices of grid nodes to be retained (into the array of nodes in bb)
            X = X(lind_nodes_intobb,:); %X now only contains nodes in the domain
            lind_nodes(lind_nodes) = lind_nodes_intobb; %logical indices of grid nodes to be retained (into rows of the full grid)
            
            
            disp(['  remove nodes with distance from boundary < ',num2str(a),'*h'])
            
        else
            
            error('domain.sdf required')
            
        end
        
        %indices of interior nodes (all retained grid points)
        ii = 1:size(X,1);    
    
end

%% find coordinate grid neighbors for all (grid) nodes: for the generation of FD stencils
%(for 'unit cube' also grid neighbors of the boundary nodes)

%indices of full grid nodes into rows of X
%(cumulative sums of lind_nodes with NaNs for non-included points)
ind_nodes = cumsum(lind_nodes);
ind_nodes(~lind_nodes) = NaN;

gridpar1 = gridpar+1;
if dim==1
    
    %indices of grid neighbors of the full grid in positive x1 direction into the rows of X
    lit_1p = [ind_nodes(2:gridpar1);nan];
    
    %indices of grid neighbors of the full grid in negative x1 direction
    lit_1n = [nan;ind_nodes(1:gridpar1-1)];
    
    %restrict them  to the neighbors of the nodes in X and collect into the required array of neighbors
    grid_neigh = [lit_1p(lind_nodes) lit_1n(lind_nodes)];
    
elseif dim==2
    
    %linear index table for the full grid into itself
    lit = reshape(1:gridpar1^2,gridpar1,gridpar1);
    
    %linear index table of grid neighbors of the full grid in positive x1 direction into the rows of X
    lit_1p = [ind_nodes(lit(2:end,:));nan(1,gridpar1)];
    
    %linear index table of grid neighbors of the full grid in negative x1 direction
    lit_1n = [nan(1,gridpar1);ind_nodes(lit(1:end-1,:))];
    
    %linear index table of grid neighbors of the full grid in positive x2 direction
    lit_2p = [ind_nodes(lit(:,2:end)),nan(gridpar1,1)];
    
    %linear index table of grid neighbors of the full grid in negative x2 direction
    lit_2n = [nan(gridpar1,1),ind_nodes(lit(:,1:end-1))];
    
    %restrict them  to the neighbors of the nodes in X and collect into the required array of neighbors
    grid_neigh = [lit_1p(lind_nodes) lit_1n(lind_nodes) lit_2p(lind_nodes) lit_2n(lind_nodes)];
    
    
elseif dim==3
    
    %linear index table for the full grid into itself
    lit = reshape(1:gridpar1^3,gridpar1,gridpar1,gridpar1);
    
    %linear index table of grid neighbors of the full grid in positive x1 direction into the rows of X
    lit_1p = cat(1,ind_nodes(lit(2:end,:,:)),nan(1,gridpar1,gridpar1));
    
    
    %linear index table of grid neighbors of the full grid in negative x1 direction
    lit_1n = cat(1,nan(1,gridpar1,gridpar1),ind_nodes(lit(1:end-1,:,:)));
    
    %linear index table of grid neighbors of the full grid in positive x2 direction
    lit_2p = cat(2,ind_nodes(lit(:,2:end,:)),nan(gridpar1,1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x2 direction
    lit_2n = cat(2,nan(gridpar1,1,gridpar1),ind_nodes(lit(:,1:end-1,:)));
    
    %linear index table of grid neighbors of the full grid in positive x3 direction
    lit_3p = cat(3,ind_nodes(lit(:,:,2:end)),nan(gridpar1,gridpar1,1));
    
    %linear index table of grid neighbors of the full grid in negative x3 direction
    lit_3n = cat(3,nan(gridpar1,gridpar1,1),ind_nodes(lit(:,:,1:end-1)));
    
    %restrict them  to the neighbors of the nodes in X and collect into the required array of neighbors
    grid_neigh = [lit_1p(lind_nodes) lit_1n(lind_nodes) lit_2p(lind_nodes) lit_2n(lind_nodes) lit_3p(lind_nodes) lit_3n(lind_nodes)];
    
    
elseif dim==4
    
    %linear index table for the full grid into itself
    lit = reshape(1:gridpar1^4,gridpar1,gridpar1,gridpar1,gridpar1);
    
    %linear index table of grid neighbors of the full grid in positive x1 direction into the rows of X
    lit_1p = cat(1,ind_nodes(lit(2:end,:,:,:)),nan(1,gridpar1,gridpar1,gridpar1));
    
    
    %linear index table of grid neighbors of the full grid in negative x1 direction
    lit_1n = cat(1,nan(1,gridpar1,gridpar1,gridpar1),ind_nodes(lit(1:end-1,:,:,:)));
    
    %linear index table of grid neighbors of the full grid in positive x2 direction
    lit_2p = cat(2,ind_nodes(lit(:,2:end,:,:)),nan(gridpar1,1,gridpar1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x2 direction
    lit_2n = cat(2,nan(gridpar1,1,gridpar1,gridpar1),ind_nodes(lit(:,1:end-1,:,:)));
    
    %linear index table of grid neighbors of the full grid in positive x3 direction
    lit_3p = cat(3,ind_nodes(lit(:,:,2:end,:)),nan(gridpar1,gridpar1,1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x3 direction
    lit_3n = cat(3,nan(gridpar1,gridpar1,1,gridpar1),ind_nodes(lit(:,:,1:end-1,:)));
    
    %linear index table of grid neighbors of the full grid in positive x4 direction
    lit_4p = cat(4,ind_nodes(lit(:,:,:,2:end)),nan(gridpar1,gridpar1,gridpar1,1));
    
    %linear index table of grid neighbors of the full grid in negative x4 direction
    lit_4n = cat(4,nan(gridpar1,gridpar1,gridpar1,1),ind_nodes(lit(:,:,:,1:end-1)));
    
    %restrict them  to the neighbors of the nodes in X and collect into the required array of neighbors
    grid_neigh = [lit_1p(lind_nodes) lit_1n(lind_nodes) lit_2p(lind_nodes) lit_2n(lind_nodes)...
        lit_3p(lind_nodes) lit_3n(lind_nodes) lit_4p(lind_nodes) lit_4n(lind_nodes)];
    
elseif dim==5
    
    
    %linear index table for the full grid into itself
    lit = reshape(1:gridpar1^5,gridpar1,gridpar1,gridpar1,gridpar1,gridpar1);
    
    %linear index table of grid neighbors of the full grid in positive x1 direction into the rows of X
    lit_1p = cat(1,ind_nodes(lit(2:end,:,:,:,:)),nan(1,gridpar1,gridpar1,gridpar1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x1 direction
    lit_1n = cat(1,nan(1,gridpar1,gridpar1,gridpar1,gridpar1),ind_nodes(lit(1:end-1,:,:,:,:)));
    
    %linear index table of grid neighbors of the full grid in positive x2 direction
    lit_2p = cat(2,ind_nodes(lit(:,2:end,:,:,:)),nan(gridpar1,1,gridpar1,gridpar1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x2 direction
    lit_2n = cat(2,nan(gridpar1,1,gridpar1,gridpar1,gridpar1),ind_nodes(lit(:,1:end-1,:,:,:)));
    
    %linear index table of grid neighbors of the full grid in positive x3 direction
    lit_3p = cat(3,ind_nodes(lit(:,:,2:end,:,:)),nan(gridpar1,gridpar1,1,gridpar1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x3 direction
    lit_3n = cat(3,nan(gridpar1,gridpar1,1,gridpar1,gridpar1),ind_nodes(lit(:,:,1:end-1,:,:)));
    
    %linear index table of grid neighbors of the full grid in positive x4 direction
    lit_4p = cat(4,ind_nodes(lit(:,:,:,2:end,:)),nan(gridpar1,gridpar1,gridpar1,1,gridpar1));
    
    %linear index table of grid neighbors of the full grid in negative x4 direction
    lit_4n = cat(4,nan(gridpar1,gridpar1,gridpar1,1,gridpar1),ind_nodes(lit(:,:,:,1:end-1,:)));
    
    %linear index table of grid neighbors of the full grid in positive x5 direction
    lit_5p = cat(5,ind_nodes(lit(:,:,:,:,2:end)),nan(gridpar1,gridpar1,gridpar1,gridpar1,1));
    
    %linear index table of grid neighbors of the full grid in negative x5 direction
    lit_5n = cat(5,nan(gridpar1,gridpar1,gridpar1,gridpar1,1),ind_nodes(lit(:,:,:,:,1:end-1)));
    
    %restrict them  to the neighbors of the nodes in X and collect into the required array of neighbors
    grid_neigh = [lit_1p(lind_nodes) lit_1n(lind_nodes) lit_2p(lind_nodes) lit_2n(lind_nodes)...
        lit_3p(lind_nodes) lit_3n(lind_nodes) lit_4p(lind_nodes) lit_4n(lind_nodes) lit_5p(lind_nodes) lit_5n(lind_nodes)];
else
    error('dim>5 not yet implemented')
end


%% generate 'nodes' structure
% h - actual grid step
% spacing  - target spacing of the nodes, may be slightly different from h
nodes = struct('itype','grid','spacing',spacing,'h',h,'X',X,'ii',ii,'grid_neigh',grid_neigh);
%add boundary nodes for 'unit cube'
if strcmp(domain.id,'unit cube')
    nodes.ib = ib;
end

