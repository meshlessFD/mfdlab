function [neigh,dists] = remove_invisible_neighbors(i,prob,nodes)
% Function remove_invisible_neighbors: remove all invisible neighbors of the i-th node for non-convex domains
%
% Remove all neighbors in nodes.neigh(i,:) such that the segment between it and the i-th node crosses the boundary of the domain
%
% INPUT 
%
% i - index of the current node into the rows of nodes.X
% prob - the prob structure
% nodes -- the nodes structure
%
% OUTPUT
%
% neigh - updated neighbors of the i-th node
% dists - their distances to the i-th node
%
% WARNING: rlies on prob.dom.sdf and prob.dom.dpf, see set_sdf.m
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2019-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

neigh = nodes.neigh(i,:);
dists = nodes.dists(i,:);

if ~prob.dom.convex && isfield(prob.dom,'sdf') && isfield(prob.dom,'dpf') %check that domain is non-convex and domain.dpf is available

    z = nodes.X(i,:); %current node

    if dists(end) > -prob.dom.sdf(z) %check that the furthest neighbor point is further from the node than the closest boundary point 
 
        Xn = nodes.X(neigh(2:end),:); %neighbors       
        zz = repmat(z,length(neigh)-1,1); %repeat z for each neighbor
        dir = Xn - zz; %directions from z to all neighbors
        [bdists,~] = prob.dom.dpf(zz,dir); %distances to the boundary in these directions
        
        %find the indices into neigh(2:end) of the visible nodes (with a tolerance to include points on the boundary)
        ind = find(dists(2:end) < bdists' + 100*eps);

        
%         %info for debugging/research
%         if length(ind) < length(p)
%             points_removed = length(p) - length(ind);
%             neigh_orig = neigh;
%         end
        
        %update neighbors and distances (use ind+1 and distances to get indices into neigh rather than  neigh(2:end) )
        neigh = neigh([1 ind+1]);
        dists = dists([1 ind+1]);
        
        %info for debugging
        %points_removed = length(nodes.neigh(i,:))-length(neigh); if points_removed, disp(['points_removed: ',int2str(points_removed)]); end
   end
end
