% Script script_mFD_Laplace: set up structures, run mFD_Elliptic, and assess the results
%
% Solve a second order elliptic equation of the type -Delta u + cu = f.
% The script reads a number of parameters from MATLAB workspace, see descriptions below.
% There parameters may be set via mFD_gui. 
% Parameters used in the last call are saved in parameters_mFD_Laplace.mat.
% The workspace variables after the execution of the script are saved in results_mFD_Laplace.mat
% and after each refinement iteration in results_mFD_Laplace_part.mat.
% If nref >0 && display_summary, then script_summary_Laplace.m is called at the end. 
% (It can also be called independently after loading results_mFD_Laplace.mat or results_mFD_Laplace_part.mat.)
% Both scripts plot several figures, unless switched off by setting appropriate parameters to false.
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.




%% Parameters (to be set either before the script is called or in the script (for the latter some suggestions can be uncommented)

%%Save parameters at the start of script (overwritten in each call); can be loaded to run this script without GUI
%WARNING: parameters_mFD_Laplace.mat will contain all data at the end of previous call if the workspace is not cleared between 
%         repeated calls of the script
save('parameters_mFD_Laplace.mat')

%%Problem setting
% dim -- the number of variables (1 <= dim <= 5)
% domain_id -- domain identifier (some of them define the domain used in only one problem, some allow choosing different problems with this domain type)
% problem_id  -- problem identifier (again some of them unique, some have additional parameters, e.g. different c)
% ccoef -- the c coefficient either as a number or a scalar function of the type @(x) for multiple points x in R^dim
% bctype -- type of boundary conditions ('Dirichlet','Neumann': applied to full boundary if not part of a problem type; 'mixed' for 'ball\ball' domain)

%%Nodes
% spacing -- target initial spacing (h) of nodes
% nref -- number of refinements of the initial set of nodes
% nodestype_int -- method for interior node generation ('grid', 'Halton', 'Sobol', 'random', 'distmesh')
% if nodestype_int == 'distmesh'
%    dm_maxiter -- maximum number of iterations of repulsion
%    dm_type -- defines the way node density is prescribed ('uniform','boundary'; for certain domains: 'feature')
%    if dm_type ~= uniform
%        dm_pow -- power parameter for the density near boundary
%        dm_mf -- magnification parameter for the density near boundary
% nodestype_bnd -- method for boundary node generation ('grid', 'projection', 'isosurface', 'distmesh' depending on domain types)

%%Sets of influence
% Lselection_method -- method of selection of sets of influence 
%      (currently: 'nearest', 'L1 selection')
% Linitcloud_size -- (target) number of nearest nodes to be found in advance and stored in nodes structure
% if  Lselection_method== 'L1 selection'
%   Lsel_order -- polynomial order used for selection
%   Lsel_mu -- mu for polynomial formulas
%   Luse_selection_weights -- if true, use the weights computed by the selection routine rather than recompute them by a method set by Lwtype
% if Lselection_method=='nearest'
% LsizeSI -- (target) number of points in the sets of influence
% if bctype == 'Neumann'
%   Nselection_method, Ninitcloud_size, NsizeSI, Nsel_order, Nsel_mu, Nuse_selection_weights (as the L-versions above)

%%Weights
% if ~Luse_selection_weights
%   Lwtype -- type of weights computation method: 'Power/TPS', 'Gauss', 'Gauss-QR', 'Matern', 'MQ', 'IMQ', 'Wendland', 'L1', 'L2' for Laplace stencil weights
%   Lporder -- order of the polynomial term for weights computation
%   Lmu -- exponent for minimal formulas (L1, L2)
%   Lrbfep -- shape parameter of rbf (insignificant for 'Power/TPS')
%   if Lwtype=='Power/TPS', 'Matern', 'MQ' or 'IMQ'
%     Lrbfpar -- power/smoothness parameter of rbf
%   if Lwtype=='Wendland'
%     Lrbfpar -- [d,k] pair of parameters of Wendland rbf
%   Lstencil_type -- types: only 'simple' in the moment
% if bctype == 'Neumann' && ~Nuse_selection_weights
%   Nwtype, Nporder, Nmu, Nrbfep, Nrbfpar, Nstencil_type (as the L-versions above)

%both sets of influence and weights
% fd_on_grid -- either 0 or the type of FD stencil to be used for gridded nodes whenever possible 
%                 (currently type can only be 'fd_star': 2d+1 star in d variables)
% If  Lselection_method=='L1 selection' or Lwtype=='L1' 
%   Ltry_positive_stencil -- if true, try first to compute a positive stencil, if this fails compute general L1 
%                            (NOTE: positive stencils may only exist for Laplacian if Lsel_order < 5, so otherwise useless)

%%Postprocessing options
% compute_statistics (true/false) -- switches on/off computation of statistics (condition numbers, etc.)
% plot_nodes (true/false) -- plot the nodes if dim=2
% plot_solution (true/false) -- 3D plot with plot3 of the computed solution (and reference solution if available) if dim=2
% plot_error_function (true/false) -- 3D plot with plot3 of the error function (if reference solution if available) 
%                                     and residual (if system matrix is returned) if dim=2
% compute_SysMatrStats (true/false) -- return system matrix to compute stability constant, differentiation error, etc.
% use_reduced_matrix (true/false) -- cond number, stability constant and bandwidth computed for the reduced matrix
% compute_eigenvalues (true/false) -- computation of eigenvalues of the system matrix 
% plot_eigenvalues (true/false) --plot computed eigenvalues of the system matrix (if computed with eig)
% display_summary (true/false) --run script_summary_Laplace.m at the end
%   plot_errors, plot_cond_stab (true/false) -- switch on/off options in script_summary_Laplace.m

%%Check some conditions on parameters

if (plot_solution || plot_error_function) && nref >9  
    nref = 9;
    disp('nref reduced to 9 to get correct plots')
end

%use only unit cube (i.e. unit interval) in 1D
if dim == 1 && ~strcmp(domain_id,'unit cube')
    %domain_type = 'unit cube';
    error('use only domain_type = ''unit cube'' in 1D')
end

%for problems with fixed or restricted domain types make sure an appropriate domain is used
switch problem_id
    case 'adaptmesh'
       if ~strcmp(domain_id,'adaptmesh')
           error('use only domain_type = ''adaptmesh'' for this problem')
       end
end

%make sure that selection weights are only used with selection methods that actually compute weights
if Luse_selection_weights 
    if ~ismember(Lselection_method,{'L1 selection'}) %only methods listed here compute weights
        error('Luse_selection_weights cannot be used with this Lselection_method')
    end
end


%% define the problem 


%set the prob structure
prob = setup_problem_Laplace(dim,domain_id,problem_id,ccoef,bctype);


  
disp(' ')
disp('        NEW EXPERIMENT')
disp(' ')
disp(['Solving Elliptic equation -Delta u + cu = f on the ',domain_id,' in ',int2str(dim),'D']);
disp(['  with ',bctype,' boundary conditions'])




%% setup differentiation matrix computation for Laplace operator 
% This will create a structure sten whose main field is 'difmatrix', a
% routine for the computation of a differentiation matrix for the Laplace operator.
% The number of nodes in any domain of influence must not exceed sten.num for some methods (e.g. Select_knearest).

disp(' ')

%%choose a set of influence ("stencil") selection function (sten.select)
switch Lselection_method
    
    case 'nearest' %LsizeSI nearest nodes
        
        Lselect = @(prob,nodes,sten,i) Select_knearest(i,prob,nodes,LsizeSI);
        disp(['Selection method for Laplace operator:'])
        disp(['  ',Lselection_method])
        disp(['  size of influence sets: ',int2str(LsizeSI)]) 
        searchcloud_size = LsizeSI; %size of the local clouds for knnsearch
        %overwrite Linitcloud_size since we do not need more than LsizeSI nodes
        Linitcloud_size = LsizeSI; %TODO: may need to be set higher for non-convex domains where invisible points are removed

         
    case 'L1 selection'
        
        Lselect = @(prob,nodes,sten,i) Select_LaplaceL1(i,prob,nodes,sten,Lsel_order,Lsel_mu,Linitcloud_size,Ltry_positive_stencil);
        disp(['Selection method for Laplace operator:'])
        disp(['  ',Lselection_method,' with order=',int2str(Lsel_order),' and mu=',int2str(Lsel_mu)])
        disp(['  initial cloud size: ',int2str(Linitcloud_size)])
        searchcloud_size = Linitcloud_size; %size of the local clouds for knnsearch
        


       
    otherwise
        error('unknown selection method')
end


%%choose a stencil weights computation function (sten.difweights) with its parameters

disp(' ')

if Luse_selection_weights %no weights routine if weights are computed already by the selection routine
    
    Ldifweights = [];
    disp('Weights are provided by the selection method')
    
else

    switch Lwtype
        
        case 'L1'
            
            disp('L1 weights for Laplace operator')
            disp(['  with order=',int2str(Lporder),' and mu=',int2str(Lmu)])
            
            if strcmp(Lselection_method,'L1 selection')
                %no need to try positive stencils twice, which would happen if using Ltry_positive_stencil set to true;
                %WARNING: in this case we try to find positive stencils when we do L1 selection of sets of influence,
                %but only use the set as the set of influence without using the weights, which are computed by L1 method again,
                %possibly with different parameters, but without trying to make them positive
                Ldifweights = @(nodes,prob,i,si) L1LaplaceWeights(nodes,i,si,prob, Lporder, Lmu, 0);
            else %trying to obtain positive weights after a different selection method
                Ldifweights = @(nodes,prob,i,si) L1LaplaceWeights(nodes,i,si,prob, Lporder, Lmu, Ltry_positive_stencil);
            end
            
        case 'L2'
            
            disp('Least squares weights for Laplace operator')
            disp(['  with order=',int2str(Lporder),' and mu=',int2str(Lmu)])

           

            
            Ldifweights = @(nodes,prob,i,si) L2LaplaceWeights(nodes,i,si,prob, Lporder, Lmu);
            
            
            
        otherwise %RBF weights
            
            %Lwtype = 'Gauss'; Lporder = 0; Lrbfep = 1; Lrbfpar = [];
            disp('RBF weights for Laplace operator')    %set the method of Laplace stencil computation
            rbf_Laplace = set_rbf(Lwtype,Lporder,dim,Lrbfep,Lrbfpar);
            
            %RBF stencil weights computation function 
            Ldifweights = @(nodes,prob,i,si) RbfLaplaceWeights(nodes,i,si,prob,rbf_Laplace);
            
    end
    
end

%%set up difmatrix routine
Ldifmatrix = @(prob,nodes,sten,II) difmatrix(prob,nodes,sten,II,Luse_selection_weights);

%%create stencil structure
Lsten = struct('type',Lstencil_type,'select',Lselect,'difweights',Ldifweights,'difmatrix',Ldifmatrix);
Lsten.num = LsizeSI; %size of sets of influence prescribed in some methods
if ~isempty(fd_on_grid) %fd stencils on grid
    Lsten.fd_on_grid = fd_on_grid;
    disp('Using FD-star weights of order 2 on all interior grid nodes')
end


%% setup differentiation matrix computation for Neumann boundary conditions 
% This will create a structure sten whose main field is 'difmatrix', a
% routine for the computation of a differentiation matrix for the normal derivatives.
% The number of nodes in any domain of influence must not exceed sten.num.
if strcmp(prob.bctype,'Neumann') || strcmp(prob.bctype,'mixed')
    
    disp(' ')
    
    %%choose a set of influence ("stencil") selection function (sten.select)
    switch Nselection_method
        
        case 'nearest' %NsizeSI nearest nodes
            Nselect = @(prob,nodes,sten,i) Select_knearest(i,prob,nodes,NsizeSI);
            disp(['Selection method for Neumann boundary condition:'])
            disp(['  ',Nselection_method])
            disp(['  size of influence sets: ',int2str(NsizeSI)])
            searchcloud_size = max(searchcloud_size,NsizeSI); %increase search cloud size to be at least the size of Neumann influence sets
            Ninitcloud_size = NsizeSI;
            

         
        case 'L1 selection'
            
            Nselect = @(prob,nodes,sten,i) Select_NeumannL1(i,prob,nodes,sten,Nsel_order,Nsel_mu,Ninitcloud_size);
            disp(['Selection method for Neumann boundary condition:'])
            disp(['  ',Nselection_method,' with order=',int2str(Nsel_order),' and mu=',int2str(Nsel_mu)])
            disp(['  initial cloud size: ',int2str(Ninitcloud_size)])
            searchcloud_size = max(searchcloud_size,Ninitcloud_size); %increase search cloud size to be at least the size of Neumann ininial clouds
            
            
        otherwise
            error('unknown selection method')
    end
    
    
    if Nuse_selection_weights %no weights routine if weights are computed already by the selection routine
        
        Ndifweights = [];
        disp('Weights are provided by the selection method')
        
    else
        
        
        %%choose a stencil weights computation function (sten.difweights) with its parameters
        switch Nwtype
            
            case 'L1'
                
                disp('L1 weights for Neumann boundary conditions')
                disp(['  with order=',int2str(Nporder),' and mu=',int2str(Nmu)])
                
                Ndifweights = @(nodes,prob,i,si) L1NeumannWeights(nodes,i,si,prob,Nporder, Nmu);
                
            case 'L2'
                
                disp('Least squares weights for Neumann boundary conditions')
                disp(['  with order=',int2str(Nporder),' and mu=',int2str(Nmu)])
                
                Ndifweights = @(nodes,prob,i,si) L2NeumannWeights(nodes,i,si,prob,Nporder, Nmu);

                
                
            otherwise %RBF weights
                
                %Nwtype = 'Gauss'; Nporder = 0; Nrbfep = 1; Nrbfpar = [];
                disp('RBF weights for Neumann boundary conditions')
                rbf_Neumann = set_rbf(Nwtype,Nporder,dim,Nrbfep,Nrbfpar);
                
                %RBF stencil weights computation function (rbf  passed here! -- for 'simple' sten)
                Ndifweights = @(nodes,prob,i,si) RbfNeumannWeights(nodes,i,si,prob,rbf_Neumann);
                
                
        end
        
    end
    
    %%set up difmatrix routine
    %Ndifmatrix = @difmatrix;
    Ndifmatrix = @(prob,nodes,sten,II) difmatrix(prob,nodes,sten,II,Nuse_selection_weights);
    
    %%create stencil structure
    Nsten = struct('type',Nstencil_type,'fd_on_grid',fd_on_grid,'num',NsizeSI,'select',Nselect,'difweights',Ndifweights,'difmatrix',Ndifmatrix);
    Nsten.num = LsizeSI; %size of sets of influence prescribed in some methods
else
    
    Nsten = [];
    
end

disp(' ')

%% Refinement loop (with node generation)

%define arrays to store the info for all refinement levels
spacing_array = nan(nref+1,1);
condSys_array = nan(nref+1,1);
maxcondLaplace_array = nan(nref+1,1);
maxcondNeumann_array = nan(nref+1,1);
ndf_array = nan(nref+1,1); %number of degrees of freedom (interior and Neumann nodes)
rms_array = nan(nref+1,1); %error
maxer_array = nan(nref+1,1);
av_Laplace_stencil_size_array = nan(nref+1,1);
av_Laplace_nnz_reduced_array = nan(nref+1,1);
av_Neumann_stencil_size_array = nan(nref+1,1);
EG_method = cell(nref+1,1);
EG_max_real_array = nan(nref+1,1);
EG_min_real_array = nan(nref+1,1);
EG_max_abs_imag_array = nan(nref+1,1);

max_resid_array = nan(nref+1,1); %error of numerical differentiation (requires System_Matrix and rhs returned in mFDstats)
rms_resid_array = nan(nref+1,1);
invSMnorm_array = nan(nref+1,1); %inf-norm of the inverse of the system matrix, estimated with the help of condest
bandwidth_rcm_array = nan(nref+1,1); %Cuthill-McKee bandwidth of the system matrix

ref_spacing = spacing; %initial spacing

%start refinement loop: we reduce the target spacing in each step by ref_factor
ref_factor = 4^(1/dim); %the number of degrees of freedom will increase with a factor close to 4
for iref = 0:nref
 
    if nref>0 && iref==0
        
        disp(['----Initial nodes -----'])
        disp(' ')
    end
    if iref>0
        disp(' ')
        disp(['----Refinement #',int2str(iref),' -----'])
        disp(' ')
        
        %refine by reducing spacing with factor 4^(1/dim), that is 2 in 2D, 1.5874 in 3D, etc.
        ref_spacing = ref_spacing/ref_factor;
    end
    disp(['Nodes with spacing ',num2str(ref_spacing)])
    spacing_array(iref+1) = ref_spacing;
    
    
    %% discretization nodes 

    
    %%generate interior nodes (boundary nodes are also created in some cases, like 'grid' for 'unit cube')
    switch nodestype_int %method for generation of interior nodes
        
        case 'grid' %uniform cartesian grid
            
            nodes = generate_inodes_grid (0.25,ref_spacing,prob.dom);
        
        case 'hexgrid' %uniform cartesian grid
            
            nodes = generate_inodes_hexgrid (0.25,ref_spacing,prob.dom);
           
        case 'Halton' %quasi-random Halton points
            nodes = generate_inodes_qprand (0.25,'halton',ref_spacing,prob.dom);
            
        case 'Sobol' %quasi-random Sobol points
            nodes = generate_inodes_qprand (0.25,'sobol',ref_spacing,prob.dom);
            
        case 'random' %pseudo-random numbers (as for 'rand' command in MATLAB)
            nodes = generate_inodes_qprand (0.25,'rand',ref_spacing,prob.dom);
            
        case 'distmesh'
            
            %only available in 2D in the moment
            if prob.dom.dim ~=2
                error('distmesh is only avalable in 2D')
            end
            
            
            %if boundary nodes are to be computed by distmesh, then we set a=0
            if strcmp(nodestype_bnd,'distmesh')
                a=0;
            else 
                a=0.25; %0.25 seems a good value if we create boundary nodes by isosurface.
                        %To use all interior nodes but avoid creation of boundary nodes set a=eps e.g.
            end
            
            %define mock values for dm_pow, dm_mf in the 'uniform' case
            if strcmp(dm_type,'uniform')
                
                dm_pow = nan;
                dm_mf = 0;
            end
                       
            nodes = generate_inodes_distmesh (a,ref_spacing,dm_maxiter,dm_type,dm_pow,dm_mf,prob);
            
            
        otherwise
            
            error('unknown method for generation of interior nodes')
            
    end
    
    if isempty(nodes.ii)
        warning('no interior nodes have been generated')
        disp('ABORT: adjust parameters')
        return
    end
    

    
    %%generate boundary nodes
    if ~isfield(nodes,'ib') %look for boundary nodes only if they have not been created already (together with interior nodes)
        
        
        %generate boundary nodes
        %nodestype_bnd = 'isosurface';
        switch nodestype_bnd %method for generation of boundary nodes
            
            case 'uniform' %currently only for  circles and hence domains whos boundary consists of circles ('unit ball','ball','ball\ball' in 2D)
                
                init_angle = 0;
                [nodes] = generate_bnodes_uniform(init_angle,prob.dom,nodes);
                
            case 'projection' %requires sdf and npf; available for any dim>1
                
                %project to the boundary all nodes at distance at most b*h from the boundary, and make them boundary nodes
                b = 1;
                nodes = generate_bnodes_projection(b,prob.dom,nodes);
                
                disp('boundary nodes: projection')
                disp(['  project interior nodes with distance from boundary < ',num2str(b),'*h'])
                
                
                
            case 'isosurface' %only for dim=2 (and 3 after implemented): boundary has to be defined by an implicit equation (e.g. sdf)
                
                
                %generate nodes by isosurface extraction from an implicit equation of the boundary with target spacing b*h
                bspacing_coef = 1;
                
                %set target boundary spacing (use known spacing of interior nodes)
                if strcmp(nodes.itype,'grid')
                    bspacing = bspacing_coef*nodes.h; %this uses step size of the uniform grid
                else
                    bspacing = bspacing_coef*nodes.spacing; %approximate spacing (assuming quasi-uniform nodes)
                end
              
                nodes = generate_bnodes_isosurface(bspacing,prob.dom,nodes);
                
                disp('boundary nodes: isosurface')
                
                
                
            otherwise
                
                error('unknown method for generation of boundary nodes')
                
        end
        
        %for gridded interior nodes extend grid_neigh to boundary nodes as [nan nan nan nan] (in 2D), etc. 
        %if boundary nodes have not been created together with interior (as for unit cube)
        %WARNING: we use here that in this case ib are at the end of the X array
        if strcmp(nodes.itype,'grid')
            nodes.grid_neigh = [nodes.grid_neigh; nan(length(nodes.ib),2*dim)]; %repmat(nan(1,2*dim),length(nodes.ib),1)];
        end
        
        
    end
    
    %setup initcloud size for selecting nodes
    nodes.Linitcloud_size = min(Linitcloud_size,size(nodes.X,1)); 
    if ismember(bctype,{'Neumann','mixed'})
        nodes.Ninitcloud_size = min(Ninitcloud_size,size(nodes.X,1)); 
    end
    
    %prepare arrays of indices for boundary conditions
    switch bctype
        
        case 'Dirichlet' %Dirichlet condition everywhere on the boundary
            
            nodes.ibD = nodes.ib;

            
        case 'Neumann' %Neumann condition everywhere on the boundary
            
            nodes.ibN = nodes.ib;

            
        case 'mixed' 
            
            %find indices of Neumann boundary points in nodes.ib
            neumann = prob.neumannbnd(nodes.X(nodes.ib,:));
            
            %generate ibD, ibN
            nodes.ibD = nodes.ib(~neumann);
            nodes.ibN = nodes.ib(neumann);

    end
    
    %%in case of Neumann boundary conditions precompute the normals for boundary nodes and remove those nodes where this is not possible
    if isfield(nodes,'ibN') %only do if there are any Neumann nodes
        
        normals = prob.dom.normalbnd(nodes.X(nodes.ibN,:)); %compute normals at all Neumann boundary points (some will be nan: corners, etc.)
        no_normals = any(isnan(normals),2); %logical indices into ibN of Neumann boundary nodes without well-defined normals
 
        if ~any(no_normals) %%all boundary normals are well defined
            
            nodes.bnormals = nan(size(nodes.X)); %the normal is set to nan at all (including interior) nodes
            nodes.bnormals(nodes.ibN,:) = normals; %assign normals to all Neumann boundary nodes
            
        else %%remove Neumann boundary nodes that do not have well-defined normals
            
            %compute logical indices of nodes to be retained
            lind = true(1,size(nodes.X,1));
            lind(nodes.ibN(no_normals))=false;
            
            %compute new indices of all nodes, with nan for those to be removed
            nind = cumsum(lind);
            nind(~lind) = nan;
            
            %update X, ii, ib, ibD, ibN
            nodes.X(~lind,:) = []; %nodes with nan removed from X
            nodes.ii = nind(nodes.ii); %no nans here because only boundary nodes are removed
            nodes.ib = nind(nodes.ib);
            nodes.ib(isnan(nodes.ib)) = []; 
            if isfield(nodes,'ibD')
                nodes.ibD = nind(nodes.ibD); %no nans here because only Neumann boundary nodes are removed
            end
            nodes.ibN = nind(nodes.ibN);
            nodes.ibN(isnan(nodes.ibN)) = []; 
           
            %assign normals to retained Neumann boundary nodes
            nodes.bnormals = nan(size(nodes.X)); %the normal is set to nan at all (including interior) nodes
            nodes.bnormals(nodes.ibN,:) = normals(~no_normals,:);
            
            if isfield(nodes,'grid_neigh') %update grid_neigh for grids, in order to use FD formulas
                nodes.grid_neigh(~lind,:) = []; 
                notnan = ~isnan(nodes.grid_neigh);
                nodes.grid_neigh(notnan) = nind(nodes.grid_neigh(notnan));
            end
            
        end
        

    end
    
    disp(['Nodes summary: ',int2str(size(nodes.X,1)),' (',int2str(length(nodes.ii)),' interior and ',int2str(length(nodes.ib)),' boundary)'])
    if isfield(nodes,'ibD')
        disp(['  including ',int2str(length(nodes.ibD)),' Dirichlet boundary nodes'])
    end
    if isfield(nodes,'ibN')
        disp(['  including ',int2str(length(nodes.ibN)),' Neumann boundary nodes'])
    end
    
    %store the number of degrees of freedom (interior and Neumann nodes)
    ndf_array(iref+1) = length(nodes.ii);
    if isfield(nodes,'ibN')
        ndf_array(iref+1) = ndf_array(iref+1) + length(nodes.ibN);
    end
    
    %plot the nodes in 2D case
    if plot_nodes && dim==2 
        figure(50+iref) 
        plot(nodes.X(nodes.ii,1),nodes.X(nodes.ii,2),'*b')
        legend_cell = {'interior'};
        hold on
        if isfield(nodes,'ibD')
            plot(nodes.X(nodes.ibD,1),nodes.X(nodes.ibD,2),'*r')
            legend_cell = [legend_cell, 'Dirichlet'];
        end
        if isfield(nodes,'ibN')
            plot(nodes.X(nodes.ibN,1),nodes.X(nodes.ibN,2),'*g')
            legend_cell = [legend_cell, 'Neumann'];
        end
        hold off
        axis equal
        axis(prob.dom.bbm(:)')
        legend(legend_cell)
        title('Discretization nodes')
    end
    
    %%precompute neighbors for each node
    
    % add a NeighborSearcher object for repeated calls of knnsearch
    nodes.ns = createns(nodes.X);         
         
    %set actual number of neighbors we find (smaller than the target searchcloud_size if X is too small)
    nodes.m = min(searchcloud_size,size(nodes.X,1)); 
    
    %flag to activate searching only for interior neighbors for ibN (however it seems to work significantly worse!)
    interior_neighbors_for_ibN = 0; 
    
    %find m nearest nodes in advance and store them in nodes structure
    if  ~interior_neighbors_for_ibN || ~isfield(nodes,'ibN')
        %nearest nodes in X for all nodes (even if we do not need them for boundary nodes)
        [nodes.neigh, nodes.dists] = knnsearch(nodes.ns, nodes.X, 'k', nodes.m);
    else %this option of choosing only interior neighbors seems significantly worse!
        nodes.nsi = createns(nodes.X(nodes.ii,:)); %create a NeighborSearcher object for interior nodes, too
        %init the arrays
        nodes.neigh = nan(size(nodes.X,1),nodes.m);
        nodes.dists = nan(size(nodes.X,1),nodes.m);
        %nearest nodes in X for interior nodes
        [nodes.neigh(nodes.ii,:), nodes.dists(nodes.ii,:)] = knnsearch(nodes.ns, nodes.X(nodes.ii,:), 'k', nodes.m);
        %nearest interior nodes for boundary nodes
        m = min(searchcloud_size,length(nodes.ii));
        [nodes.neigh(nodes.ibN,1:m), nodes.dists(nodes.ibN,1:m)] = knnsearch(nodes.nsi, nodes.X(nodes.ibN,:), 'k', nodes.m);       
    end
    
    
    %% setup statistics
    
    %initialize statistics structure   
    updateSTATS('init','STATS_RBFFD');
    
    %diagnostics for known causes of failure
    updateSTATS('initf',{'poly_notexact','linprog_failure'},NaN);

    %debugging example (see commented out at the end of RbfLaplaceWeights.m)
    %updateSTATS('debug','debug1')
    
    %various useful statistics
    if compute_statistics
        
        %compute the condition number of system matrix and the norm of its inverse
        updateSTATS('initf',{'condSys','invSMnorm'},NaN);
        
        %statistics for system matrix assembly
        updateSTATS('initf',{'maxcondLaplace','maxcondLaplace_sel','maxcondNeumann','maxcondNeumann_sel',...
            'av_Laplace_stencil_size','av_Neumann_stencil_size','max_Laplace_stencil_size'},NaN);
        
        %store the system matrix and the right hand side in STATS for additional info
        if compute_SysMatrStats
            updateSTATS('initf',{'System_Matrix','rhs'},[]);
        end
        
        %compute number of non-positive stencils for Laplacian
        if Ltry_positive_stencil
            updateSTATS('initf',{'np_stencils','np_stencils_sel'},0);
        end

        
        %turn off warnings for singular/near singular matrices (we compute condition numbers anyway)
        % A way to find out the message id: [a, MSGID] = lastwarn()
        warning('off','MATLAB:singularMatrix'); %turn off the warnings about singular matrices
        warning('off','MATLAB:nearlySingularMatrix'); %turn off the warnings about near singular matrices
    end
    
    
    %% run mFD
    disp(' ')
    disp('Run computation')
    tic
    [u] = mFD_Elliptic(prob, nodes, Lsten, Nsten);
    toc
    
    %% restore the warnings
    if compute_statistics
        warning('on','MATLAB:singularMatrix');
        warning('on','MATLAB:nearlySingularMatrix');
    end
    
    %% get statistics
    
    %get the data (mFDstats will be empty if statistics is disabled)
    mFDstats = updateSTATS('return');
    
    
    %clear the persistent variable STATS in the file updateSTATS 
    updateSTATS('clear');
    
   
    %% report and abort computation in case of failure ('poly_notexact','linprog_failure')
    if ~isnan(mFDstats.poly_notexact)
        disp(' ')
        warning(mFDstats.poly_notexact)
        disp('ABORT: either increase the size of influence sets or decrease porder')
        return;
    end
    
    if ~isnan(mFDstats.linprog_failure)
        disp(' ')
        warning(mFDstats.linprog_failure)
        disp('ABORT: L1 method is not suitable for this setting')
        return;
    end

    
    %% display statistics/results
    
    %plot solution (on nodes) with plot3 in 2D case
    %plot_solution = 1;
    if plot_solution && dim==2
        figure(20+iref)
        plot3(nodes.X(:,1),nodes.X(:,2),u,'.')
        title('approximate solution')
    end
    
    
    %%compute the error using a reference solution defines as a function in prob.rs
    if isfield(prob,'rs') %check that a reference solution is defined

        
        % values of the reference solution
        ue = prob.rs(nodes.X);
        
        % plot the reference solution on current nodes
        if plot_solution && dim==2
            figure(10+iref)
            plot3(nodes.X(:,1),nodes.X(:,2),ue,'.')
            title('exact/reference solution')
        end
       
        
        % error function
        er = u - ue;
        
        %error on interior nodes
        ue_ii = ue(nodes.ii);
        u_ii = u(nodes.ii);
        er_ii = er(nodes.ii);
        
        %subtract the average, that is best least squares approximation by constants in case of pure Neumann BC and absent c
        %(in what case the solution of the PDE is unique up to additive constant)
        if strcmp(bctype,'Neumann') && ~isfield(prob,'c')
            av_er = sum(er)/length(er);
            er = er - sum(er)/length(er);
        end
        
        %optional 3D plot of the error function in 2D case
        %plot_error_function = 1;
        if plot_error_function && dim==2
            figure(30+iref)
            plot3(nodes.X(:,1),nodes.X(:,2),er,'.')
            title('error function')
        end
        
        % maximum error on the interior nodes
        maxer = max(abs(er_ii),[],'includenan');
        % relative maximum error on interior nodes
        %maxer = max(abs(er(nodes.ii)),[],'includenan')/max(abs(ue(nodes.ii)));
        maxer_array(iref+1) = maxer;
        
        %rms error on the interior nodes
        rms = norm(er_ii)/sqrt(length(er_ii));
        %relative rms error on the interior nodes
        %rms = norm(er_ii)/norm(ue_ii);
        rms_array(iref+1) = rms;
        
        disp(' ')
        disp('Error on nodes')
        disp(['  maximum: ',num2str(maxer,'%2.2e\n')])
        disp(['  rms/L2:  ',num2str(rms,'%2.2e\n')])
        if strcmp(bctype,'Neumann') && ~isfield(prob,'c') %only for pure Neumann problem
            disp(['average error before correction: ',num2str(av_er,'%2.2e\n')])
        end
        
    else %end of if isfield(prob,'rs')
        
        disp(' ')
        disp('Cannot compute errors since a reference solution is not given')
        
    end
    
    %stop assessment for this refinement level if there are no statistics
    if ~compute_statistics
        %save partial results for the case of a crash
        save('results_mFD_Laplace_part.mat')
        continue;
    end
    
    %%the remaining code inside the iref-loop is executed only if statistics have been collected (mFDstats is not empty)
    disp(' ')
    
    %condition number of the system matrix
    condSys = mFDstats.condSys;
    %average stencil size
    fprintf('Average Laplace stencil size: %1.2f\n',mFDstats.av_Laplace_stencil_size) %set in mFD_elliptic.m
    if Ltry_positive_stencil
        if Luse_selection_weights
            np_stencils = mFDstats.np_stencils_sel;
        else
            np_stencils = mFDstats.np_stencils;
       end
        fprintf('  nonpositive stencils: %i (%1.2f %%)\n',np_stencils,100*np_stencils/length(nodes.ii))
    end
    av_Laplace_stencil_size_array(iref+1) = mFDstats.av_Laplace_stencil_size; %store it, too
    if strcmp(prob.bctype,'Neumann') || strcmp(prob.bctype,'mixed')
        fprintf('Average Neumann stencil size: %1.2f\n',mFDstats.av_Neumann_stencil_size) %set in mFD_elliptic.m
        av_Neumann_stencil_size_array(iref+1) = mFDstats.av_Neumann_stencil_size; %store it, too
    end
    
    
    disp(' ')
    disp(['Condition numbers'])
    if Luse_selection_weights
        maxcondLaplace = mFDstats.maxcondLaplace_sel;
        maxcondNeumann = mFDstats.maxcondNeumann_sel;
    else
        maxcondLaplace = mFDstats.maxcondLaplace;
        maxcondNeumann = mFDstats.maxcondNeumann;
    end
    fprintf('  System matrix: %7.1e\n  Laplace weights computation (max): %7.1e  \n',condSys,maxcondLaplace)
    if strcmp(prob.bctype,'Neumann') || strcmp(prob.bctype,'mixed')
        fprintf('  Neumann weights computation (max): %7.1e  \n',maxcondNeumann)
        maxcondNeumann_array(iref+1) = maxcondNeumann;
    end
    disp(' ')
    
    maxcondLaplace_array(iref+1) = maxcondLaplace;
    condSys_array(iref+1) = condSys;
    
    %norm of the inverse system matrix        
    invSMnorm = mFDstats.invSMnorm;
    disp(['Inf stability constant: ',num2str(invSMnorm,'%2.2e\n')])
    
    invSMnorm_array(iref+1) = invSMnorm;

    
    %additional statistics using System_Matrix and rhs
    if ~compute_SysMatrStats 
       
       %reduced matrix can be used only if System_Matrix and rhs are returned
       use_reduced_matrix = 0;
        
    else %isfield(mFDstats,'System_Matrix') && isfield(mFDstats,'rhs')
        
        %%We use reduced system matrix (Dirichlet nodes eliminated!) for cond numbers, stab. constants and bandwidth
        %(since they are sparser and could have been used in the computation)
        %use_reduced_matrix = 1;
        if use_reduced_matrix

            disp('For reduced matrix')
            
            M = mFDstats.System_Matrix;
%             M(nodes.ibD,:) = []; %remove Dirichlet rows and columns (since they may be eliminated)
%             M(:,nodes.ibD) = [];
            
            rcmperm = symrcm(M); %symmetric reverse Cuthill-McKee permutation
            rcmperm_inv(rcmperm) = 1:length(rcmperm); %inverse permutation
            M = M(rcmperm,rcmperm);
            M(rcmperm_inv(nodes.ibD),:) = []; %remove Dirichlet rows and columns (since they may be eliminated)
            M(:,rcmperm_inv(nodes.ibD)) = [];
            clear rcmperm_inv %otherwise it is reused for next loop iteration 
            

            
            %average number of nonzeros of reduced matrix per row
            av_Laplace_nnz_reduced = nnz(M)/size(M,1);
            av_Laplace_nnz_reduced_array(iref+1) = av_Laplace_nnz_reduced;
            
            %recompute condition number
            condSys_reduced = condest(M');
            fprintf('  System matrix condition number: %7.1e\n ',condSys_reduced);
            condSys_array(iref+1) = condSys_reduced; %overwrite condSys array! (for summary)
            
            %norm of the inverse system matrix
            %(inexpensive estimate of inf-norm of the inverse matrix as suggested by R. Schaback, Error analysis of
            %nodal meshless methods. In Meshfree methods for partial differential equations VIII, pp. 117-143. Springer, 2017.)
            invSMnorm = condSys_reduced/norm(M,Inf); 
            disp([' inf stability constant: ',num2str(invSMnorm,'%2.2e\n')])  

            invSMnorm_array(iref+1) = invSMnorm; %overwrite the non-reduced results
             
            %Cuthill-McKee bandwidth of the reduced system matrix
            %(bandwidth after symmetric reverse Cuthill-McKee permutation of A'+A)
%             rcmperm = symrcm(M); %symmetric reverse Cuthill-McKee permutation
%             M = M(rcmperm,rcmperm);
            [ub, lb] = bandwidth(M);
            bandwidth_rcm = max(ub, lb)+1; %bandwidth of the permuted matrix
            bandwidth_rcm_array(iref+1) = bandwidth_rcm;
            
            disp(' ')
            fprintf('Cuthill-McKee bandwidth (reduced matrix): %i\n',bandwidth_rcm);
            
           
            clear M
            
        else %%End of using reduced matrix M
             

            
            %Cuthill-McKee bandwidth of the system matrix
            %(bandwidth after symmetric reverse Cuthill-McKee permutation of A'+A)
            rcmperm = symrcm(mFDstats.System_Matrix); %symmetric reverse Cuthill-McKee permutation
            %         [I,J]=find(mFDstats.System_Matrix(rcmperm,rcmperm));
            %         bandwidth_rcm = max(abs(I-J)) + 1; %bandwidth of the permuted matrix            
            [ub, lb] = bandwidth(mFDstats.System_Matrix(rcmperm,rcmperm));
            bandwidth_rcm = max(ub, lb)+1; %bandwidth of the permuted matrix
            
            disp(' ')
            fprintf('Cuthill-McKee bandwidth: %i\n',bandwidth_rcm);
        end
      
        bandwidth_rcm_array(iref+1) = bandwidth_rcm; %we store it for reduced matrix only if use_reduced_matrix = 1
  
        %%compute residual 
        residual = mFDstats.System_Matrix*ue - mFDstats.rhs;
        max_resid = max(abs(residual));
        max_resid_array(iref+1) = max_resid;
        rms_resid = norm(residual)/(sqrt(length(residual)));
        rms_resid_array(iref+1) = rms_resid;
        
        disp(' ')
        disp('Residual')
        disp(['  maximum: ',num2str(max_resid,'%2.2e\n')])
        disp(['  rms/L2:  ',num2str(rms_resid,'%2.2e\n')])
        
        
        %optional 3D plot of the residual at all points in 2D case
        if plot_error_function && dim==2
            figure(40+iref)
            plot3(nodes.X(:,1),nodes.X(:,2),residual,'.')
            title('residual')
        end
                
        
        %compute and plot the egenvalues of the (negative of) system matrix
        %NOTE: positions of eigenvalues are particularly important for the stability of non-stationary problems, see e.g. 
        %B. Fornberg and N. Flyer, A Primer on Radial Basis Functions with Applications to the Geosciences, SIAM 2015.    
        %The paper [V. Bayona, N. Flyer, B. Fornberg, and G. A. Barnett. On the role of polynomials in RBF-FD approximations: 
        %II. Numerical solution of elliptic PDEs Journal of Computational Physics, 332:257 – 273, 2017] demonstrates that
        %Power/TPS (also called polyharmonic) RBF produce system matrices with more stable eigenvalues than e.g. Gauss RBF

        %compute_eigenvalues = 1;  %flag to switch it on/off   
        if compute_eigenvalues  %expensive 
            
            disp(' ')           
            disp('Eigenvalues of system matrix')    
            
            %save partial results for the case of a crash
            save('results_mFD_Laplace_part.mat')

            %compute and plot either all egenvalues  (only for smaller matrices) or only compute max and min real and max imaginary
            if  size(mFDstats.System_Matrix,1) < 1000 %30000
                

                disp('(computed by converting to full matrix)')       
                EG=eig(full(-mFDstats.System_Matrix));
                
                EG_max_real = max(real(EG));
                EG_min_real = min(real(EG));
                EG_max_abs_imag = max((abs(imag(EG))));
                
                %plot eigenvalues
                if plot_eigenvalues
                    figure(60+iref)
                    plot(EG + 1e-16i,'.') %add a numerically zero imaginary part, in order to have a correct plot
                    axis tight
                    xlabel('Re(\lambda)')
                    ylabel('Im(\lambda)')
                end
                
                %store the method's id
                EG_method(iref+1) = {'eig'};
                
            else %otherwise use eigs routine that sometimes fails (increasing 'SubspaceDimension' ssd may help)

                disp('(computed by eigs)')
                
                %choose the size of SubspaceDimension
                ssd = min(size(mFDstats.System_Matrix,1),50); 
%                 EG_real_bothends = real(eigs(-mFDstats.System_Matrix,2,'bothendsreal','SubspaceDimension',ssd));
%                 EG_max_real = max(EG_real_bothends);
%                 EG_min_real = min(EG_real_bothends);
                %switched to separate computation of max and min real since 
                %it is not clear where bothends (which saves half of the time) places nan when one of them fails
                [egvector_rmax,eg,failed] = eigs(-mFDstats.System_Matrix,1,'largestreal','SubspaceDimension',ssd);
                if failed
                    %try to double ssd
                    ssd = min(size(mFDstats.System_Matrix,1),2*ssd);
                    [egvector_rmax,eg,failed] = eigs(-mFDstats.System_Matrix,1,'largestreal','SubspaceDimension',ssd);
                    if failed
                        EG_max_real = NaN; %return NaN in this case
                        warning('max real part computation failed')
                    else
                        EG_max_real = real(eg);
                    end
                else
                    EG_max_real = real(eg);
                end
                [egvector_rmin,eg,failed] = eigs(-mFDstats.System_Matrix,1,'smallestreal','SubspaceDimension',ssd);
                if failed
                    EG_min_real = NaN;
                else
                    EG_min_real = real(eg);
                end
                
                [egvector_imax,eg,failed] = eigs(-mFDstats.System_Matrix,1,'largestimag','SubspaceDimension',ssd);
                if failed
                    EG_max_abs_imag = NaN;
                else
                    EG_max_abs_imag = imag(eg);
                end
               

                EG_method(iref+1) = {['eigs',int2str(ssd)]};
            end
            
       
            disp(['   max real part: ',num2str(EG_max_real)])
            disp(['   min real part: ',num2str(EG_min_real)])
            disp(['   max absolute imaginary part: ',num2str(EG_max_abs_imag)])
            
            EG_max_real_array(iref+1) = EG_max_real;
            EG_min_real_array(iref+1) = EG_min_real;
            EG_max_abs_imag_array(iref+1) = EG_max_abs_imag;
          

        end
    end
    
    %save partial results for the case of a crash
    save('results_mFD_Laplace_part.mat')
    
end %end of refinement loop

%save the results 
save('results_mFD_Laplace.mat')

%% display summary and plot errors 
if nref >0 && display_summary
    script_summary_Laplace
else
    disp('No summary or error plots since no refinements')
end

