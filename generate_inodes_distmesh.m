function [nodes] = generate_inodes_distmesh (a,spacing,maxiter,dm_type,pow,mf,prob)
% Function generate_inodes_distmesh: interior nodes by DistMesh
%
% Generates interior nodes using DistMesh
%
% INPUT
%
% a - parameter: interior nodes to be created at distance a*spacing from the boundary
% spacing - "initial edge length" for distmesh
%           (seems to correspond to the final spacing near/along the feature defined by fh, at least with fh = @(p) 1 + ...)
%           hence used as target grid spacing
% maxiter - maximum number of iterations of repulsion
%           (increasing makes a smoother set of nodes, but takes longer and risk instability)
% dm_type - parameter to setup the density distribution of nodes 
% pow - power of the level set function
%       (increasing pow makes the "band" of dense points near the features thicker)
% mf - magnification factor of the level set function
%      (increasing mf reduces the density of points away from the features)
% prob - structure describing the problem we generate the nodes for
%
% OUTPUT
%
% nodes -- the nodes structure
%
%
% INFO
% 
% Uses distmesh2d_npf.m derived from distmesh2d.m from DistMesh v1.1
% which is developed by Per-Olof Persson and Gilbert Strang, see
% http://persson.berkeley.edu/distmesh/ and Per-Olof Persson and Gilbert
% Strang, "A Simple Mesh Generator in MATLAB," SIAM Review Vol. 46 (2) 2004.
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%number of the figure that shows how the mesh evolves at DistMesh iterations
fign = 1000; %0 is supposed to supress drawing this figure, but then it seems to plot into some other figure window 
             %and distorting the plots there, perhaps because of "drawnow" commands in distmesh2d_npf?

%% sdf function fd for the domain
if isfield(prob.dom,'sdf')
    fd = prob.dom.sdf;
else
    error('prob.dom.sdf is required')
end



%%setup for the scaled edge length function fh to define the features near which more density of nodes is sought.
switch dm_type
    
    case 'uniform'      
        
        fh = []; %uniform density
        
    case 'boundary'
        
        %Here the signed distance function is used as the level set function, which means we place
        %the nodes more densely near the boundary
        ls = prob.dom.sdf;
        fh = @(p) 1 + mf*abs(ls(p)).^pow;
        
    case 'feature' %special density for particular problems
        
        switch prob.id
            
            case 'adaptmesh' %high density near the origin
                
                
%                 %spacing and pow parameters and fh used in Dang Thi Oanh, Oleg Davydov and Hoang Xuan Phu, 
%                 %Adaptive RBF-FD method for elliptic problems with point singularities in 2D, 
%                 %Applied Mathematics and Computation, 313 (2017), 474-497. 
%                 spacing = [0.01 0.0007 0.0007 0.001  0.0012 0.00065 0.0012  0.00070  0.0013  ] ; %h0 = 0.0002               
%                 pow = [ 0.65 0.7 0.675  0.65 0.6 0.575 0.525 0.5 0.425 ];
%                 fh = @(p) sqrt(sum(p.^2,2)).^pow;
                
                %experiments with pow=0.65, mf=10
                ls = @(p) sqrt(sum(p.^2,2));               
                fh = @(p) 1 + mf*abs(ls(p)).^pow;
                
            otherwise
                
                error('features are not defined for this problem')
        end
        
        
end



%% choose fixed nodes depending on the domain
switch prob.dom.id
    
    case 'unit cube'
        
        %Fixed node positions
        pfix = [0,0; 0,1; 1,0; 1,1];
        
    case 'adaptmesh'
        
        pfix = [0 0; -1/sqrt(2) 1/sqrt(2); -1/sqrt(2) -1/sqrt(2)];
        
    otherwise %no fixed nodes
         
        pfix = [];

        
end 
        



%% set bounding box for initial distribution
if isfield(prob.dom,'bbm')
    bbox = prob.dom.bbm; %[-2,-2; 2,2];
else
    error('extended bounding box prob.dom.bbm is required')
end


%% decide whether we use npf version of distmesh
if a==0
    use_npf = 1; %for a=0 we use npf because distmesh otherwise produces nodes not stricly on the boundary
    if ~isfield(prob.dom,'npf')
        error('prob.dom.npf is required if we use distmesh for boundary nodes, too')
    end
    
else
    use_npf = 0; %not clear whether we should use npf or not in this case
end

%% run distmesh (it plots into the current figure)
if fign
    figure(fign) %opens the figure
else
    h=figure('visible','off');  %make sure 'distmesh2d_npf' does not produce a plot 
end
title('DistMesh iterations')
if use_npf
    [X,~,~]=distmesh2d_npf(fd,fh,spacing,bbox,pfix,prob.dom.npf,maxiter,use_npf,1);
else
    [X,~,~]=distmesh2d_npf(fd,fh,spacing,bbox,pfix,[],maxiter,use_npf,1);
end
if ~fign
    close(h)
end

if isempty(X)
%     disp('no interior nodes have been generated yb distmesh')
%     disp('ABORT: adjust parameters')
    nodes = struct('itype','distmesh','spacing',spacing,'X',X,'ii',[]);
    return
end


%% find interior and (possibly) boundary nodes and mark the latter red in the figure

%make interior the nodes at distance at least a*spacing from the boundary
[ii,~] = find(fd(X)< -a*spacing-100*eps);


% either create ib or throw away the boundary vertices
if a==0 %only in this case we create boundary vertices!
    
    ii = ii'; %make ii a row vector!
    ib = setdiff(1:size(X,1),ii);
    
    
    %plot boundary nodes in red
    if fign
        figure(fign)
        hold on
        plot(X(ib,1),X(ib,2),'*r')
        hold off
        drawnow
    end
else
    
    X=X(ii,:);
    ii=1:size(X,1);
end

%pause for 0.1 sec to see the result of DistMesh
pause(.1)

%% generate 'nodes' structure
% h - actual grid step
% spacing  - target spacing of the nodes, may be slightly different from h
nodes = struct('itype','distmesh','spacing',spacing,'X',X,'ii',ii);

if a==0
    nodes.ib = ib;
end


