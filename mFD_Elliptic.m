function [u] = mFD_Elliptic(prob, nodes, Lsten, Nsten)
% Function mFD_Elliptic: solve an elliptic equation -Du + cu = f
%
% Solve a scalar second order elliptic equation (without first order terms) in a domain in R^d, with Dirichlet and/or 
% Neumann boundary conditions, by meshless finite difference method
%
% INPUT
%
% prob -- structure to define the problem to be solved, see setup_problem_*.m
% nodes -- structure for discretization nodes, see difmatrix.m
% Lsten -- structure for the selection of stencil supports for the second order operator, see script_mFD_Laplace.m
% Nsten -- structure for the selection of stencil supports for Neumann boundary condition, see script_mFD_Laplace.m
%
% OUTPUT
%
% u --  the values of the solution at all nodes in nodes.X
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%% get nodes from corresponding structures
X = nodes.X;
[n, dim]= size(X);
ii = nodes.ii; % interior nodes
%ib = nodes.ib; % boundary nodes

%% Initialize system matrix as an empty n x n sparse matrix
System_Matrix  = sparse(n,n);
ii_nongrid = ii;

%% Use FD stencils for gridded nodes (currently only (2*dim+1)-star stencil for Laplacian)
if isfield(Lsten,'fd_on_grid') && isfield(nodes,'grid_neigh') %check that fd stencils are desired and gridded nodes exist
    if strcmp(Lsten.fd_on_grid,'fd_star') && strcmp(prob.type,'Laplace') %check that fd_star stencils are sought for the Laplacian
        
        %find indices into rows of X of the interior nodes that have four grid neighbors (ii_grid) and its complement (ii_nongrid)
        nongridsubind = any(isnan(nodes.grid_neigh(ii,:)),2);
        ii_grid = ii(~nongridsubind);
        ii_nongrid = ii(nongridsubind);
        
        %assembly of the stiffness matrix for -Delta on gridded nodes
        I = reshape(repmat(ii_grid,2*dim+1,1),[],1);
        J = reshape([ii_grid; nodes.grid_neigh(ii_grid,:)'],[],1);
        V = repmat([2*dim; -ones(2*dim,1)]/nodes.h^2,length(ii_grid),1);
        
        %update system matrix
        System_Matrix  = System_Matrix + sparse(I,J,V,n,n);
       
    end
end

%% system matrix assembly

% assembly of the stiffness matrix as a differentiation matrix for second order differential operator
% (operator is implicitly defined by sten.difweights used by sten.difmatrix)
% "-" because sten.difmatrix is for a negative definite operator such as Laplace operator Delta u
System_Matrix  = System_Matrix - Lsten.difmatrix(prob,nodes,Lsten,ii_nongrid); 

% %remove numerically zero entries 
% (done in *Weights.m functions instead: looking for weigths of particular size in a sparse matrix is expensive)
% System_Matrix(abs(System_Matrix) < 100*eps) = 0;

% print/store average stencil size (number of nonzero weights per row)
%disp(['Average stencil size: ',num2str(nnz(System_Matrix)/length(ii))])
updateSTATS('set','av_Laplace_stencil_size',nnz(System_Matrix)/length(ii));

% add the mass matrix if prob.c exists
% (the mass matrix is discretized on one node)
if isfield(prob,'c') %otherwise there is no c*u term in the equation, that is c=0
    if isa(prob.c,'function_handle') %prob.c is a scalar function of the type @(x) for multiple points x in R^d, d=size(X,2)
        System_Matrix  = System_Matrix + sparse(ii,ii,prob.c(X(ii,:)),n,n);        
    else % prob.c is a number
        System_Matrix  = System_Matrix + prob.c*sparse(ii,ii,1,n,n); %prob.c*speye(n);
        %the version with  speye(n) also adds c-values at the diagonal entries of ib rows
    end
end


%init rhs by nan
rhs = nan(size(X,1),1); 

% set the right hand side for interior nodes (use the function prob.f)
rhs(ii) = prob.f(X(ii,:));


%%apply boundary conditions by modifying the entries of the system matrix and the right hand side

%Dirichlet boundary
if isfield(nodes,'ibD')
    
    ibD = nodes.ibD; % Dirichlet boundary nodes
    
    % modification of the system matrix entries; System_Matrix(ib,:) are originally zeros by initialization of System_Matrix
    % System_Matrix(sub2ind(size(System_Matrix),ib,ib)) = 1;
    System_Matrix  = System_Matrix + sparse(ibD,ibD,1,n,n); %recommended code for sparse matrices
    
    % set the right-hand-side entries
    rhs(ibD) = prob.g(X(ibD,:));  % boundary condition values
    
end

%Neumann boundary
if isfield(nodes,'ibN') 
    
    ibN = nodes.ibN; % Neumann boundary nodes
    
    %TODO: FD weights for the case 'fd_on_grid'
    % modification of the system matrix entries; System_Matrix(ib,:) are originally zeros by initialization of System_Matrix
    System_Matrix  = System_Matrix + Nsten.difmatrix(prob,nodes,Nsten,ibN);
    
    
    
    % set the right-hand-side entries
    rhs(ibN) = prob.gn(X(ibN,:));  % set boundary condition values for points with well defined normal
    
    %debugging: check the error of numerical differentiation of Neumann condition for the exact solution
    %         resid = System_Matrix(ib,:)*prob.rs(X)-prob.gn(X(ib,:));
    %         max(abs(resid))
    
    % print/store average stencil size (number of nonzero weights per row)
    %disp(['Average stencil size: ',num2str(nnz(System_Matrix)/length(ii))])
    updateSTATS('set','av_Neumann_stencil_size',nnz(System_Matrix(ibN,:))/length(ibN));
    
    
end


%check System_Matrix for spasity
if ~issparse(System_Matrix)
    error('System matrix is not in sparse form')
end


%% Solve the linear system using MATLAB backslash
u = System_Matrix \ rhs;

%% Estimate the condition number and the norm of the inverse if the statistics for condSys is active
if updateSTATS('on','condSys')   
    %estimate of inf-condition number
    condSys = condest(System_Matrix');
    updateSTATS('set','condSys',condSys);  
    
    %a cheap and very good estimate of the inf-norm of the inverse matrix
    invSMnorm = condSys/norm(System_Matrix,Inf); 
    updateSTATS('set','invSMnorm',invSMnorm);
end

%condSys = condest(System_Matrix'); %estimate of the inf-norm condition number 
                                 %(without transpose gives an estimate of the 1-norm condition number)
%alternatives:                                
%condSys = norm(System_Matrix,Inf)*norm(inv(System_Matrix),Inf);  %precise inf-norm condition number but expensive    
%condSys = norm(System_Matrix,1)*norm(inv(System_Matrix),1);  %precise 1-norm condition number but expensive   
%condSys = cond(full(System_Matrix)); %precise 2-norm condition number but very expensive
%condSys = cond(balance(full(System_Matrix))); %to see improvement of 2-norm condition number by diagonal scaling

%norms of the inverse system matrix
%invSMnorm = condSys/norm(System_Matrix,Inf); %inexpensive estimate of inf-norm of the inverse matrix
%     invSM = inv(System_Matrix); %inversion is expensive
%     invSMnorm = norm(invSM,Inf)
%     invSM2 = norm(full(invSM)) %2-norm: this is also expensive



%% Store System_Matrix and rhs if required
updateSTATS('set','System_Matrix',System_Matrix);
updateSTATS('set','rhs',rhs);





