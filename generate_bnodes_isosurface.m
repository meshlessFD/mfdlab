function [nodes] = generate_bnodes_isosurface(bspacing,domain,nodes)
% Function generate_bnodes_isosurface: nodes on the implicit boundary of a domain
%
% Generates boundary nodes by isoline extraction from an implicit equation of the 
% boundary of a 2D domain 
%
% INPUT
% bspacing - target spacing of boundary nodes 
% domain - structure describing the domain we generate the nodes for
% nodes -- structure containing existing interior nodes (in fact we only need thair spacing near the boundary)
%
% OUTPUT
% nodes -- the nodes structure completed with boundary nodes
%
% WARNING: only available for 2D domains
% TODO: add implicit boundaries of 3D domains (isosurfaces)
%
% INFO
%
% uses isoline_nodes.m
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%sparate cases of 2D and 3D
switch domain.dim
    
    case 2 %2D
        
        %set the level set function and bounding box with margin
        switch domain.id
            
            case 'unit ball'
                
                %set boundary level set function (both vector- and xy-versions)
                bls = @(p) sqrt(sum(p.^2,2))-1;
                blsxy = @(x,y) sqrt(x.^2 + y.^2) - 1;
                
                
                %bounding box for the domain (not tight: a box that surely contains the boundary with some margin)
                bb = [-2 -2; 2 2];
                
            otherwise %works whenever domain posesses an sdf function and a bounding box
                
                if ~isfield(domain,'sdf') || ~isfield(domain,'bbm')
                    error('isosurface method not available for this domain.id')
                end
                
                bls = domain.sdf;
                blsxy = @(x,y) reshape(domain.sdf([x(:),y(:)]),size(x));  
                
                %expand the bounding box (to make it non-tight in the case it is tight)
                bb = domain.bbm; 
               
        end
        
    case 3 %3D: use isosurface or fimplicit3
        
        error('isosurface method not yet implemented in 3D')
        
    otherwise
        
        error('isosurface method is used for domains in dimensions 2 or 3')
end



%extract isoline points
Xb = isoline_nodes(bspacing,0,bls,blsxy,bb,0);

                                
%add the boundary points to nodes.X at the end, and store their indices in the nodes structure
%(we assume that nodes.X had no boundary nodes)
nodes.ib = size(nodes.X,1)+(1:size(Xb,1));  %indices of boundary nodes
nodes.X = [nodes.X; Xb];
                           
                                
% %messages/visualization
% disp('boundary nodes: isosurface')
% if dim==2, figure(88), plot(Xb(:,1),Xb(:,2),'*'), axis([-2 2 -2 2]), end

