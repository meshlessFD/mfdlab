function weights = RbfNeumannWeights(nodes,i,si,prob,rbf)
% Function RbfNeumannWeights: RBF weights for normal derivative for the i-th node with set of influence given by si
%
% Compute numerical differentiation weights for the normal derivative at i-th node of nodes.X with the set of influence given by 
% the nodes with indices in si, using the function RbfDer1Stencil.m.
%
% INPUT 
%
% nodes -- the nodes structure
% i - index of the current node into the rows of nodes.X
% si - vector of indices of the set of influence into the rows of nodes.X
% prob - the prob structure
% rbf -- structure that defines RBF and its parameters
%        rbf.type-- a string defining the type of RBF, see frbf.m
%        rbf.par -- parameter of the RBF of particular type
%        rbf.ep -- scaling ("shape") parameter
%        rbf.porder -- order of the polynomial part
%
% OUTPUT
%
% weights -- a column vector of stencil weights for the Laplacian
%
%
% INFO
%
% See RbfDer1Stencil.m and RbfStencil.m for further details
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


z = nodes.X(i,:);
%normal = prob.dom.normalbnd(z); %boundary normal at z (also valid but would not be good to check here nan normals for corner points)
normal = nodes.bnormals(i,:); %use precomputed boundary normal at z
X = nodes.X(si,:);


[weights,condn] = RbfDer1Stencil(z,X,rbf.type,rbf.par,rbf.ep,rbf.porder,normal);

%remove numerically zero weights
weights(abs(weights)<100*eps) = 0;

%collect info about condition numbers
updateSTATS('max','maxcondNeumann',condn);


