function [si, weights]  = Select_NeumannL1(i,prob,nodes,sten,order,mu,initcloud_size)
% Function Select_NeumannL1: set of influence for normal derivative  at the i-th node using L1 stencils
%
% Select a set of influence for the numerical differentiation of the normal derivative at the i-th node by computing 
% the sparse L1-weights using L1Der1Stencil. In addition, the L1 weights are returned that can be used as if
% produced by L1NeumannWeights
% 
% INPUT 
%
% i - index of the current node into the rows of nodes.X
% prob - the prob structure
% nodes -- the nodes structure
% sten -- the sten structure
% porder -- polynomial order (degree+1) (integer >= 1)
% mu -- power of the L1 weights (real number >= 0) 
% initcloud_size -- the size of the initial set of neighbors to apply L1Der1Stencil
%
% OUTPUT
%
% si - vector of indices of the set of influence into the rows of nodes.X
% weights -- a column vector of stencil weights for the Laplacian
%
%
% INFO
%
% See L1Der1Stencil.m, L1Stencil.m and L1NeumannWeights.m for further details
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2019-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


% remove invisible points if the domain is non-convex and domain.dpf is available
[neigh,~] = remove_invisible_neighbors(i, prob,nodes); %returns updated neigh and dists if it updates them
%neigh = nodes.neigh(i,:);

% reduce initcloud_size and update neigh if there are too few neighbors
initcloud_size = min(initcloud_size, length(neigh));
neigh = neigh(1:initcloud_size);


%power parameter
%mu = order;% 0;%

%evaluation point
z = nodes.X(i,:);

%compute boundary normal at the evaluation point
%normal = prob.dom.normalbnd(z); %compute boundary normal at z (also valid but would not be good to check here nan normals for corner points)
normal = nodes.bnormals(i,:); %use precomputed boundary normal at z


%%L1 min stencils on the full set of neighbors in neigh
[w, grf, condn, supp] = L1Der1Stencil(z, nodes.X(neigh,:), order, mu,normal);
 
%collect info about condition numbers
updateSTATS('max','maxcondNeumann_sel',condn);
        
%return support with optional thresholding
if sten.thresflag && length(supp) > sten.num
    
    %thresholding: find the indices into neigh of num weights of nodes with largest absolute values
    [~,ind]=sort(abs(w),1,'descend');
        
    %return the indices of num largest weights; sort to still have nodes ordered according to the distance
    %(the central point has to be first if using least squares weights with mu>0)
    si = neigh(sort(ind(1:sten.num)));
    
    %%alternative thresholding (keeping the central node): return the indices of the current node and num-1 additional nodes into nodes.X
    % [~,ind]=sort(abs(w(2:end)),1,'descend');
    %%return the indices of the current node and num additional nodes into nodes.X
    % si = [neigh(1) neigh(ind(1:num-1)+1)];
    
    
else %otherwise return full support
        
    si = neigh(supp);
    
end

weights = w(supp);

