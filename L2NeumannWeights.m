function weights = L2NeumannWeights(nodes,i,si,prob,porder, mu)
% Function L2NeumannWeights: least squares weights for normal derivative for the i-th node with set of influence given by si
%
% Compute numerical differentiation weights for the normal derivative at i-th node of nodes.X with the set of influence given by 
% the nodes with indices in si, using the function L2Der1Stencil.m.
%
% INPUT 
%
% nodes -- the nodes structure
% i - index of the current node into the rows of nodes.X
% si - vector of indices of the set of influence into the rows of nodes.X
% prob - the prob structure
% porder -- polynomial order (degree+1) (integer >= 1)
% mu -- power of the least square weights (real number >= 0) 
%
% OUTPUT
%
% weights -- a column vector of stencil weights for the Laplacian
%
%
% INFO
%
% See L2Der1Stencil.m and L2Stencil.m for further details
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2019-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


z = nodes.X(i,:);
%normal = prob.dom.normalbnd(z); %boundary normal at z (also valid but would not be good to check here nan normals for corner points)
normal = nodes.bnormals(i,:); %use precomputed boundary normal at z
X = nodes.X(si,:);

[weights, grf2, condn] = L2Der1Stencil(z, X, porder, mu, normal);

%remove numerically zero weights
weights(abs(weights)<100*eps) = 0;

%collect info about condition numbers
updateSTATS('max','maxcondNeumann',condn);


