function [weights,condn] = RbfLaplaceStencil(z,X,type,par,ep,porder)
% Function RbfLaplaceStencil: weights of RBF numerical differentiation formula for Laplacian 
%
% Compute the weights of RBF numerical differentiation formula for Laplacian at a point z\in R^d 
% given function values at X\subset R^d using RBF interpolation, optionally with a polynomial term.
%
% INPUT
%
% z -- a row d-vector representing a point in R^d where Laplacian is evaluated
% X -- an (nx x d)-matrix of nx points in R^d
% type -- a string defining the type of RBF, see frbf.m for type description;
%         in addition to the types of frbf.m accepts 'gQR' (Gauss-QR) for d=2 and porder=0 
% par -- parameter of the RBF of particular type
% ep -- shape parameter
% porder -- order of the polynomial part  
%
% OUTPUT
%
% weights -- an (nx x 1)-vector (column)
% condn -- condition number of the kernel matrix, see  RbfStencil.m 
%          
%
% INFO
%
% See comments in RbfStencil.m and Gauss_QR_diffmat_2D.m
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

dim = size(X,2);


%% Gauss-QR method
if strcmp(type,'gQR')
    
    if porder > 0
        error('porder not implemented')
    end
    
    if dim == 2
        [weights,Psi] = Gauss_QR_diffmat_2D('L',z,X,ep);
        weights = weights';
        condn = cond(Psi.A0);
    else
        error('dim not implemented')
    end
    
    return;
end

%% set up the function for the evaluation of the RBF part of the right hand side
rhsf = @(z,X,type,par,ep) ep^2*frbf_Laplacian(distsqh (z, X)*ep^2, dim, type, par)'; %dim is set here!

%% compute the polynomial part cz of the rhs (assuming evaluation of the weights at the origin)

%dimension of polynomials of given order in d variables
if porder > 0
    polydim = nchoosek(porder+dim-1,dim);
else
    polydim = 0;
end
 
% the vector cz is the rhs for Laplacian
cz = zeros(polydim,1);
if porder > 2 %cz is zero when porder <= 2
    ind = ((dim+1)*(dim+2) - (1:dim).*(2:dim+1))/2 + 1;
    cz(ind,:) = 2;
end

%% call RbfStencil.m to compute the weights
[weights,~,condn] = RbfStencil(z,X,type,par,ep,porder,rhsf,cz,2);

