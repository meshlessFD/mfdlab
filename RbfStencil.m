function [weights,pcoeffs,condn] = RbfStencil(z,X,type,par,ep,porder,rhsf,polypart,dorder)
% Function RbfStencil: weights of RBF numerical differentiation formula for diff. operator(s) 
%
% Compute the weights of RBF numerical differentiation formula for one or more homogeneous differential 
% operator(s) D_i, i=1,...m, of the same order dorder at a point z\in R^d, given function values at X\subset R^d using RBF interpolation, 
% optionally with a polynomial term of order porder. Info about D_i is passed via rhsf and polypart.
%
% INPUT
%
% z -- a row d-vector representing a point in R^d where differential operators are evaluated;
%      it also surves as center for the shift when choosing polynomial basis (it is also scaled!)
%      NOTE: if shift_and_upscale = faulse (see the code), z is also accepted as an (nz x d)-matrix 
%            of nz points in R^d (see e.g. RbfGradientStencil.m)
% X -- an (nx x d)-matrix of nx points in R^d
% type -- a string defining the type of RBF, see frbf.m for type description.
% par -- parameter of the RBF of particular type
% ep -- shape parameter
% porder -- order of the polynomial part  
% rhsf -- m-dim function for the evaluation of the RBF part of the right hand side (specific for the differential operator)
%         syntax: @(z,X,type,par,ep)
% polypart -- column vector or matrix of several (m) column vectors for the polynomial part of the right hand side 
%            (specific for the differential operators) 
% dorder -- the order of the homogeneous differential operators D_i
%
% OUTPUT
%
% weights -- an (nx x m) matrix of column vectors of weights (m is the number of differential operators)
% pcoefs -- an (polydim x 1)-vector (column) of polynomial coefficients needed for RBF interpolation 
% condn -- condition number for the computation of weights
%          If mat is a (symbolic) object, it is converted to double before estimating the cond number.
%
% NOTE: For the scalable stencils (with power and TPS RBF) and for the polynomial part in general we use "upscaling" preconditioning.
% NOTE: that D_i are homogeneous is only important if shift_and_upscale = true (see the code)
% NOTE: We use a robust null space method that works whenever unique weights exist. To switch to the standard (and cheaper) 
%       method of extended matrix set below null_space = false;
%
%
% INFO
%
% For scalable stencils see [1] O. Davydov and R. Schaback, Minimal numerical differentiation formulas, Numer. Math., 140 (2018), 555-592. 
% doi:10.1007/s00211-018-0973-3  and [2] O. Davydov and R. Schaback, Optimal stencils in Sobolev spaces, IMA J. Numer. Anal., 39 (2019), 
% 398--422. doi:10.1093/imanum/drx076 "Upscaling" preconditioning is described in Section 6.1 of [1].
%
% For the null space method, see [3] O. Davydov, Approximation with conditionally positive definite kernels on deficient sets, 
% arXiv:2006.13543 http://arxiv.org/abs/2006.13543 
% TODO: check pseudoinverse pinv as alternative to the null space in the rank deficient case, see a comment containing "pinv" below; 
% this is based on SVD and does not seem faster than the null space code; 
% we would still need to check for solvability as the pinv solution is a minimum norm least squares which always exists;
% with null space method we get in addition a meaningful estimate of the condition number of the problem
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

%Flag to choose whether the weights are computed by the standard method of extended matrix (null_space = false),
% or by a null space method that works whenever unique weights exist (null_space = true).
null_space = true; %false; %

dim = size(X,2);

% %debugging
% persistent nullinequ

%% no polynomial term case
if ~porder
    
    mat = kermat (X, X, type, par, ep);
    
    rhs = rhsf(z,X,type,par,ep);
    weights = mat\rhs; %always uniquely solvable if rbf is p.d.
    %weights = double(sym(mat,'d')\sym(rhs,'d'));
    
    pcoeffs = [];
    

    condn=cond(double(mat));
    
    return;
end

%% set up shifting to origin and upscaling
%"upscaling" preconditioning for the polynomial part and power/tps
if strcmp(type,'p') || (strcmp(type,'tp') && ~mod(par,2))
    
    scalable = true; %flag for scalable stencils
    shift_and_upscale = true; %flag to do shifting and upscaling

else %all other RBFs: not scalable; still advantageous to do shifting and polynomial upscaling from porder > 1;
    %for porder > dorder also allows evaluating D of the monomials at the origin when building the rhs
    %(this is used in the functions RBFFunStencil.m etc. that call this function!)
    
    scalable = false;
    
    if porder > 1
        shift_and_upscale = true; 
    else
        shift_and_upscale = false;
    end
    
end


if shift_and_upscale
    
    %shifting: replace X by Xz = X-z and z by the origin
    X = bsxfun(@minus,X,z);
    z = zeros(1,dim);
    
    %"upscaling": replace Xz by Xz/h
    h = max(sqrt(sum(X.^2,2))); %scaling coefficient
    %h=1; %switch off the preconditioning
    X = X/h;  
    
    %adjust ep for the non-scalable case to undo the effect of upscaling on rbf matrix
    if ~scalable
        ep = ep*h;
    end
    
    
end

%% assemble the system and solve for the weights

%polynomial matrix
polyvals = monomials(X,porder);

%find the number of polynomial entries in the rhs
polydim = size(polyvals,2);

if null_space && porder > 1 %null space method 
    %not needed for porder=1 since in this case the extended system is always uniquely solvable
    %if rbf is p.d. or c.p.d. of order 1 because polyvals is of full rank, and hence we use the classic method in this case
    
    %SVD of the polynomial matrix
    [U,S,V] = svd(polyvals); %compute full rather than economy size because we need the null space matrix for polyvals'
    
    %(numerical) rank of polyvals
    s = diag(S);
    numrank = find(s > 10000*eps,1,'last');
    
    %check consistency of the polynomial system using the null space matrix for polyvals V(:,numrank+1:end)
    if any(any(polypart)) %otherwise polynomial system is homogeneous and always has the trivial solution w=0
    
        %return all NaNs if _any_ column of polypart is inconsistent, even if some of them maybe OK
        if any(any(abs(polypart'*V(:,numrank+1:end)) > 10000*eps)) 
            
            updateSTATS('set','poly_notexact',['(',mfilename,') Stencil is not exact for polynomial order ',int2str(porder)])
           
            
            weights = nan(size(X,1),size(polypart,2));
            pcoeffs = nan(polydim,size(polypart,2));
            condn = nan;
            return;
        end
        
        %COMMENTED OUT HERE AND BELOW AN ALTERNATIVE COMPUTATION METHOD
        %                 %compute a particular weight vector w satisfying the polynomial system only
        %                 w = V(:,1:numrank)'*polypart;
        %                 w = bsxfun(@rdivide,w,s(1:numrank)); %divide the columns of w by s(1:rank)
        %                 w = U(:,1:numrank)*w;
        %
        %             else
        %
        %                 w= zeros(size(X,1),1);
        
    end
    
    %compute pseudoinverse of S, needed for pcoeffs
    Spinv = zeros(size(S,2),size(S,1));
    Spinv(1:numrank,1:numrank) = diag(1./s(1:numrank));
    
    %if polyvals' has full row rank, then its null space is trivial, and there is only one polynomially exact weight vector,
    %which we compute in this case using backslash
    if numrank == size(X,1)
        mat = polyvals';
        weights = mat\polypart;
        pcoeffs = V*Spinv*U'*rhsf(z,X,type,par,ep);
    else
        
        %compute null space matrix for polyvals' and its transpose
        N = U(:,numrank+1:end);
        Nt = N';  %unfortunately MATLAB does not seem to have a transpose multiplication routine for A'*B (see BLAS!)
        
        %(unextended) system matrix and right hand side
        mato = kermat (X, X, type, par, ep);
        rhso = rhsf(z,X,type,par,ep);
        
        %             %find the weights
        %             wz = (Zt*mat*Z) \ (Zt*(rhs-mat*w));
        %             weights = w + Z*wz;
        
        
        %build the partially extended overdetermined system with a unique solution and hence of full rank
        mat = [Nt*mato; polyvals'];
        rhs = [Nt*rhso; polypart];
        
        %find the weights (should be reliable since mat has full rank)
        weights = mat\rhs;
        
        %find the polynomial coefficients using pseudoinverse of S
        pcoeffs = V*Spinv*U'*(rhso-mato*weights);
        
%         %debugging
%         if isempty(nullinequ)
%             nullinequ=norm(Zt*polyvals) %max(max(abs(Zt*polyvals)))
%         else
%             nullinequ=max(nullinequ,norm(Zt*polyvals)) %max(max(abs(Zt*polyvals))))
%         end
    end
    

    
    
else %classic method (always used for porder=1)
    
    %extended system matrix
    mat = [kermat(X, X, type, par, ep) polyvals;
           polyvals' zeros(size(polyvals,2))];
    
    %extended right hand side
    rhs = [rhsf(z,X,type,par,ep); polypart];
    
    %compute the solution  and separate into true weights and polynomial coefficients
    weights = mat\rhs; %pinv(mat)*rhs; %TODO: check pseudoinverse pinv as alternative to the null space in the rank deficient case
    
    pcoeffs = weights(end-polydim+1:end,:); %sometimes called dummy weights, but needed in the case of interpolation
    weights(end-polydim+1:end,:) = [];
    
end



%scale the weights back 
if shift_and_upscale && dorder
    weights = weights/h^dorder;
end

condn=cond(double(mat));

%%some debugging
% %if size(X,1) > 5 && size(X,1) < 10
% if size(X,1) > 12 && size(X,1) < 28
% %if null_space && ~any(isnan(weights)) && numrank < size(X,1) %means really choses weights, more than one poly exact available
%     P = sqrt(Q_Laplacian(z, X, weights, type, par, ep))
%       polydim
%       updateSTATS('set','polydim',polydim);
%       poly_deficiency = polydim - numrank
%       updateSTATS('set','poly_deficiency',poly_deficiency);
%       null_space_dim = size(X,1)-numrank
%       updateSTATS('set','null_space_dim',null_space_dim);
%     figure(1),plotp(X), hold on, plotp(z,'*r'),hold off
%     size(X,1)
%     pause
% end

