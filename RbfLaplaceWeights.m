function weights = RbfLaplaceWeights(nodes,i,si,prob,rbf)
% Function RbfLaplaceWeights: RBF Laplace weights for i-th node with set of influence given by si
%
% Compute numerical differentiation weights for Laplace operator at i-th node of nodes.X with the set of influence given by 
% the nodes with indices in si, using the function RbfLaplaceStencil.m 
%
% INPUT 
%
% nodes -- the nodes structure
% i - index of the current node into the rows of nodes.X
% si - vector of indices of the set of influence into the rows of nodes.X
% prob - the prob structure%computes RBF numerical differentiation weights for the operator Delta u at the 
% rbf -- structure that defines RBF and its parameters
%        rbf.type-- a string defining the type of RBF, see frbf.m
%        rbf.par -- parameter of the RBF of particular type
%        rbf.ep -- scaling ("shape") parameter
%        rbf.porder -- order of the polynomial part
%
% OUTPUT
%
% weights -- a column vector of stencil weights for the Laplacian
%
%
% INFO
%
% See RbfLaplaceStencil.m and RbfStencil.m for further details
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2017-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

z = nodes.X(i,:);
X = nodes.X(si,:);



[weights,condn] = RbfLaplaceStencil(z,X,rbf.type,rbf.par,rbf.ep,rbf.porder);


%collect info about condition numbers
updateSTATS('max','maxcondLaplace',condn);


%remove numerically zero weights
weights(abs(weights)<100*eps) = 0;


% 
% if condn > 1e15
%     warning('condn too big')
% end
% 

% %debugging example 
% if updateSTATS('dbon','debug1') && condn > 1e12
%     updateSTATS('dbset','debug1','condn',struct('z',z,'X',X,'weights',weights,'condn',condn))
% end
