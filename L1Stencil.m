function [w, grf, condn, supp] = L1Stencil(z, X, porder, mu, cz, dorder, positive)
% Function L1Stencil: weights of L1 numerical differentiation 
%
% Compute the weights of (weighted) discrete L1 numerical differentiation formula for a homogenuous linear 
% differential operator D at a point z in R^d, exact for polynomials up to order q=order, 
% given function values at X\subset R^d.  If the flag positive in the beginning of the code is set to a non-zero, 
% then we try to compute a minimal positive stencil in the sense of Seibold, that is, with all positive weigths but 
% the one that corresponds to the first point in X. (This is only done if the first point coincides with z.) 
%
% INPUT
%
% z -- a row d-vector representing a point in R^d where the values D f(z) are estimated
% X -- an (np x d)-matrix of np points in R^d
% porder -- polynomial order (degree+1) (integer >= 1)
% mu -- power of the L1 weights (a real number >= 0)
% cz -- (polydim x 1)- vector of the coefficients alpha!c_alpha(i) (in columns) 
%       for the homogeneous differential operator D = sum_{|alpha|=dorder} c_alpha partial^alpha,
%       where polydim = nchoosek(porder+d-1,d) is the dimension of polynomials of order porder
% dorder -- (scalar) the orders of the homogeneous differential operators D_i
% positive -- flag to try computing a minimal positive stencil (if positive is set to a non-zero).
%             In this case, if the first point is not z, or the computation of a positive weight vector fails, 
%             then we return w = nan(np,1); supp = (1:np)'; grf = Inf;  condn = Inf;
%
% OUTPUT
%
% w -- an (np x 1)-vector of weights 
% grf -- the value of ||.||_{1,mu} growth function for operator D with order q=porder and power mu at z
% condn -- condition number of the Vandermonde polynomial matrix that can be used to solve for the weights
% supp -- the support of the weight vector w (column vector of indices into the rows of X)
%
%
% WARNING: The results will be wrong for cz corresponding to a non-homogeneous differential operator (because of 
% inconsistent scaling). Such operators should be split into a sum of homogeneous ones and the resulting weights summed up.
% WARNING: in contrast to L2Stencil.m, cz corresponds to only one operator D
%
%
% INFO
%
% Weights are obtained by minimizing sum_j |w_j|\|x_j-z\|_2^mu, mu>=0, subject to polynomial exactness of oder q,
% which is recast as a linear programming problem and solved by simplex method, see [1] O. Davydov and R. Schaback, 
% Minimal numerical differentiation formulas, Numer. Math., 140 (2018), 555-592. doi:10.1007/s00211-018-0973-3
%
% Minimal positive stencils have been suggested for Laplacian by B. Seibold. Minimal positive stencils in meshfree finite difference 
% methods for the Poisson equation. Comput. Methods Appl. Mech. Eng., 198(3-4):592–601, 2008. They do not exist for differential operators 
% of order up to 3 if polynomial order is 5 or higher. (See B. Seibold. M-Matrices in Meshless Finite Difference Methods. Dissertation, 
% University of Kaiserslautern, 2006, and Section 4.3 in [1].)
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


nonpositive_warning = 0;
nonexact_warning = 0;

np =size(X,1);


% shift the points to have the center at the origin
X = X - ones(np,1)*z;

%"upscaling" preconditioning to use the unscaled monomial basis
h = max(sqrt(sum(X.^2,2)));
Xs = X/h; %scaled points

%Calculate the matrix of the values of monomials, with rows corresponding to points in Xs
polvalues = monomials(Xs,porder);

%set the weights for l1-minimization 
if mu==0
    l1w=ones(np,1); % non-weighted l_1 norm
else
    l1w = sum(Xs.^2,2).^(mu/2); % weights for the l_1 norm
end

%setup for l_1-minimization by simplex method
options = optimoptions('linprog','Algorithm','dual-simplex','Display', 'off'); %'dual-simplex'

%to allow vpa polvalues: since linprog cannot do VPA
polvaluesd=double(polvalues);
czd=double(cz);
l1wd=double(l1w);

if positive
    
    %check that the first data point coincides with z (that is with zero after subtraction X-z), otherwise positive formulas for Laplacian are not defined
    if max(abs(X(1,:))) > 100*eps
        if nonpositive_warning        
            warning('the first data point is different from Laplace evaluation point')
        end
        w = nan(np,1);
        supp = (1:np)';
        grf = Inf;
        condn = Inf;
        return;        
    end
    
    % build the matrix and rhs for the linprog solver and run it
    A = polvaluesd'; %[polvaluesd',-polvaluesd'];
    A(:,1) = -A(:,1);
    b = czd;
    [xx,fval,exitflag] = linprog(l1wd, [], [], A, b, zeros(np,1),[],[],options);
    
    %actions in case of linprog failure
    if exitflag < 0
        if nonpositive_warning
            warning('positive stencil: simplex algorithm failed to find a solution')
        end
        w = nan(np,1);
        supp = (1:np)';
        grf = Inf;
        condn = Inf;
        return;
        
    else
        
        %output: stencil weights
        w = xx;
        w(1) = -w(1);
        
    end
    
else
    
    %First version
    
    % build the matrix and rhs for the linprog solver and run it
    A = [polvaluesd',-polvaluesd'];
    b = czd;
    [xx,fval,exitflag] = linprog([l1wd;l1wd], [], [], A, b, zeros(2*np,1),[],[],options);
     
    %output: stencil weights
    w = xx(1:np)-xx(np+1:2*np);
    
    % %Second version
    %
%     A=polvalues';
%     b=czd;
%     [xx,fval,exitflag]=linprog([zeros(size(l1wd)); l1wd],...
%                                              [diag(-ones(np,1)) diag(-ones(np,1));...
%                                              diag(ones(np,1)) diag(-ones(np,1));...
%                                              diag(zeros(np,1)) diag(-ones(np,1))], zeros(3*np,1),...
%                                              [A zeros(size(A))], b,...
%                                              [],[],[],options);
%     %output: stencil weights
%     w=xx(1:np);
    
    %actions in case of linprog failure
    if exitflag < 0
        %warning('simplex algorithm failed to find a solution')
        w = nan(np,1);
        supp = (1:np)';
        grf = Inf;
        condn = Inf;
        
        updateSTATS('set','linprog_failure',['(',mfilename,') simplex algorithm failed to find a solution '])
        
        return;
    end
    
end

%stencil support
supp = find(abs(w)>10000*eps);

%condition number as if we solved the linear system on supp
condn = cond(polvalues(supp,:));


%find the stencil on the same support with vpa if polvalues are vpa
if isobject(polvalues) %using vpa    
    
    %just solving linear system, or other method built into the command A\b 
    wi = polvalues(supp,:)'\cz;
    
    w = zeros(np,1);
    w = sym(w,'d');
    w(supp) = wi;
    %if length(wi)> 6, error(' '), end
end


%debugging
print_weights = 0;
if print_weights == 1
    format short e
    double(w)
    maxer = max(abs(double(polvalues'*w-cz)))
    format
    
    %check whether the stencil is positive
    if w(1) < 100*eps && min(w(2:end)) > -100*eps
        disp('positive stencil')
    else
        disp('non-positive stencil')
        
    end
end


%check polynomial exactness by computing the residual.  
defect_tol = 10000*eps;
if max(abs(polvalues'*w-cz)) > defect_tol %norm((polvalues')*w-cz)
    if nonexact_warning
        warning(['the formula is not exact for polynomials, defect = ',num2str(max(abs(polvalues'*w-cz)))])
        %disp(max(abs(polvalues'*w-cz)))
        disp(['return w=NaN, grf = Inf since defect > ',num2str(defect_tol)])
    end
    w = nan(np,1);
    supp = (1:np)';
    grf = Inf; 
    
    updateSTATS('set','poly_notexact',['(',mfilename,') Stencil is not exact for polynomial order ',int2str(porder)])
    
    return;    
end

%scale the weights back 
w = w/h^dorder;



%||.||_{1,mu} growth function with q=order, mu = power
grf = (sum(X.^2,2).^(mu/2))'*abs(w);




