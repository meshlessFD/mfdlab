function domain = set_sdf(domain)
% Function set_sdf: signed distance function and related functions associated to a domian
%
% Sets the (quasi-) signed distance function for the given domain determined by domain.id
% Quasi-sdf returns negative distance inside the domain and some positive function outside. 
% In addition, a nearest point function and/or intersection point in a given direction may be determined.
%
% INPUT
%
% domain -- structure describing the domain
%         domain.id -- indentificator of the domain
%         domain.?? -- various additional parameters of the domain come as additional fields
%
% OUTPUT
%
% domain -- the same structure with the fields sdf and possibly npf and/or dpf assigned:
% domain.sdf -- (quasi-) signed distance function with sintax d = f(p)
%         p -- (n x d)- matrix of points in R^d
%         d -- column n-vector of signed distances to the boundary (negative inside the domain and positive outside)
%         WARNING: in some cases only quasi-sdf: true negative distance inside the domain and some positive values outside
%         See comments in the file for indovidual domains.
%         
% domain.npf -- nearest point function for the domain boundary, with sintax [d,pn] = f(p)
%         (returns the signed distance d and nearest points on the boundary)
%         WARNING: implemented not for all domains where sdf is available
%
% domain.dpf -- function to find directional nearest intersection points on the domain boundary, with sintax [d,dp] = f(p,dir)
%         (returns the the nearest point on the boundary in direction dir and distance to it)
%         WARNINGS: inplemented not for all domains where sdf is available; 
%                  in some cases only quasi-dpf (incorrect for some points outside domain)
%
% NOTE: Use isfield command to check whether npf or dpf has been set.
% TODO: efficient implementation of sdf, etc. for polygonal domains with many segments.
%
%
% INFO
%
% Implementations of some signed distance functions are from  DistMesh developed by Per-Olof Persson and Gilbert Strang, see
% http://persson.berkeley.edu/distmesh/ and Per-Olof Persson and Gilbert
% Strang, "A Simple Mesh Generator in MATLAB," SIAM Review Vol. 46 (2) 2004.
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2004-2012 Per-Olof Persson: functions drectangle0, dpoly, donesegment, dintersect, dcircle and ddiff 
% from DistMesh v1.1 at the end of this file. See also a copy of DistMesh's COPYRIGHT.TXT below.
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%dimensionality of the domain
dim = domain.dim;

switch domain.id
            
    case 'ball' %a ball in R^d with given center and radius
        
        domain.sdf = @(p) sdf_ball(p, domain.center, domain.radius);
        domain.npf = @(p) npf_ball(p, domain.center, domain.radius);    
        domain.dpf = @(p, dir) dpf_ball(p, dir, domain.center, domain.radius);
     
    case 'unit ball' %the unit ball ||x||_2 < 1 in R^d with center at the origin and radius 1
        
        domain.sdf = @(p) sdf_ball(p, zeros(1,dim), 1);
        domain.npf = @(p) npf_ball(p, zeros(1,dim), 1);    
        domain.dpf = @(p, dir) dpf_ball(p, dir, zeros(1,dim), 1);
        
    case 'ball\ball' %ball of radius 2 centered at the origin with a spherical hole of radius 1 centered at 0.31*(1,2,...,d)/||(1,2,...,d)||_2
                     %inner_center is set before calling set_sdf
                     
        %true sdf since ddiff is true when the second domain is contained in the first
        domain.sdf = @(p) ddiff(sdf_ball(p, zeros(1,dim), 2), sdf_ball(p, domain.inner_center, 1));
        
        %true npf since ddiff is true when the second domain is contained in the first
        npf1 = @(p) npf_ball(p, zeros(1,dim), 2);
        npf2 = @(p) npf_ball(p, domain.inner_center, 1);
        domain.npf = @(p) ndiff(p, npf1, npf2);
       
     
    case 'box' %d-dimensional box (hyperrectangle) with diagonal given by the lower and upper corner vectors c1 and c2
        %true sdf everywhere
        
        domain.sdf = @(p) sdf_box(p, domain.c1, domain.c2);
        domain.npf = @(p) npf_box(p, domain.c1, domain.c2);
 
    case 'unit cube' %d-dimensional unit cube (0,1)^dim
        %true sdf everywhere
        
        domain.sdf = @(p) sdf_box(p, zeros(1,dim), ones(1,dim));
        domain.npf = @(p) npf_box(p, zeros(1,dim), ones(1,dim));
        
    case 'rectangle' %rectangle [a,b]x[c,d] in 2D (special case of 'box')

        %true sdf also outside the domain
        domain.sdf = @(p) drectangle0(p, domain.a, domain.b, domain.c, domain.d);
        
        %true npf and dpf: using npoly and  dpf_poly
        pv = [domain.a domain.c; domain.a domain.d; domain.b domain.d; domain.b domain.c; domain.a domain.c];
        domain.npf = @(p) npoly(p,pv);
        domain.dpf = @(p, dir) dpf_poly(p,dir,pv);    
        

    case 'polygon' %(possibly non-convex) polygon with n-1 vertices given by (nx2)-matrix pv (where the first vertex is repeated at the end),
        % true sdf, npf, dpf also outside the domain
        
        domain.sdf = @(p) dpoly(p,domain.pv);
        domain.npf = @(p) npoly(p,domain.pv);
        domain.dpf = @(p, dir) dpf_poly(p,dir,domain.pv);
       
        
    case 'adaptmesh' %2D disk sector with re-entrant corner of angle pi/2: example for adaptmesh of MATLAB's PDE Toolbox 
        % https://de.mathworks.com/help/pde/ug/adaptmesh.html
        %Test Problem 1 in Dang Thi Oanh, Oleg Davydov and Hoang Xuan Phu, Adaptive RBF-FD method for elliptic problems with point singularities in 2D,
        %Applied Mathematics and Computation, 313 (2017), 474-497. 
        
        
        %%sdf
        %Since the polygon is chosen such that the union of the circle and polygon is convex, the resulting sdf is also 
        %correct outside the domain, and the nearest points returned by npf are also correct for any p.
        
        pv = [0 0; -1/sqrt(2) 1/sqrt(2); 1-sqrt(2) 1; 1 1; 1 -1; 1-sqrt(2) -1; -1/sqrt(2) -1/sqrt(2); 0 0];        
        domain.sdf = @(p) dintersect(dcircle(p,0,0,1),dpoly(p,pv)); %[0 0; -1 1; 1 1; 1 -1; -1 -1; 0 0]));
        
        %%npf
        npf1 = @(p) ncircle(p,0,0,1);
        npf2 = @(p) npoly(p,pv); %[0 0; -1 1; 1 1; 1 -1; -1 -1; 0 0]);
        domain.npf = @(p) nintersect(p,npf1,npf2);       
        
        %%quasi-dpf
        dpf1 = @(p, dir) dpf_poly(p,dir,[-1/sqrt(2) -1/sqrt(2); 0 0; -1/sqrt(2) 1/sqrt(2)]); %directional intersection points for two straight segments
        dpf2 = @(p, dir) dpf_ball(p, dir, [0 0], 1);
        domain.dpf = @(p, dir) dpf_intersect(p,dir,dpf1,dpf2); %WARNING: incorrect for some points outside domain
        
        
    otherwise       

        warning(['sdf etc. not implemented for domain.id = ',domain.id])
        
end



%%%%%%%%%%%%%%%%%%%%%%%%%%5555 sdf, npf, dpf functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

% box in R^d

function dist = sdf_box(p, c1, c2)
% Function sdf_box
% true sdf for the d-dimensional box (hyperrectangle) with diagonal 
% given by the lower and upper corner vectors c1 and c2 (c1 < c2).
% 
%
% Syntax dist = sdf_box(p, c1, c2)
%
% OD 28 Oct 2016 and 24 Nov 2018
%
% INPUT
%
% p -- n x d matrix of n points in R^d for which sdf is to be computed
% c1 - 1 x d -vector of the first ("lower") corner of the box
% c2 - 1 x d -vector of the last ("upper") corner of the box
%
% OUTPUT
%
% dist -- n x 1 vector of the signed distances of the points in p 

%this gives quasi-sdf (the same as below)
%dist = -min([bsxfun(@minus,p,c1),bsxfun(@minus,c2,p)],[],2);

%%compute quasi-sdf

%compute differences c1-p and p-c2
diffs = [bsxfun(@minus,c1,p),bsxfun(@minus,p,c2)];

%this gives quasi-sdf (correct sdf for points inside the box, and positive values for points outside)
dist = max(diffs,[],2);

%%compute correct positive values of dist to get true sdf also outside the box

%find logical indices of dist for which sdf is positive and thus points are outside the box
out = dist > 0;

%replace all negative values of diffs by zeros
diffs = max(diffs,0); %we could have replaced negative values just for rows in "out", but presumably doing for all rows is faster

%compute the norms of the vectors given by positive components
ndiffs = sqrt(sum(diffs.^2,2));

%replace dist values for outside points by these norms
dist(out) = ndiffs(out);




function [dist,pn]  = npf_box(p, c1, c2)
% Function npf_box
% find the true signed distances (dist) and nearest points (pn) to each point in p 
% (also correct for points outside the domain) for the d-dimensional box (hyperrectangle) with diagonal 
% given by the lower and upper corner vectors c1 and c2 (c1 < c2).
% 
%
% Syntax [dist,pn]  = npf_box(p, c1, c2)
%
% OD 25 Nov 2018
%
% INPUT
%
% p -- n x d matrix of n points in R^d for which sdf is to be computed
% c1 - 1 x d -vector of the first ("lower") corner of the box
% c2 - 1 x d -vector of the last ("upper") corner of the box
%
% OUTPUT
%
% dist -- n x 1 vector of the signed distances of the points in p 
% pn -- n x d matrix of n points on the boundary, closest to the corresponding points in p 

%%project outside points to the boundary of the box
%(inside and boundary points remain unchanged)

%create pn by replacing the component p_i of any point in p by respective component of c1 if p_i<c1_i, and
%replacing the component p_i of any point in p by respective component of c2 if p_i>c1_2
pn = max(p,c1);
pn = min(pn,c2);

%%compute sdf

%compute differences c1-p and p-c2
diffs = [bsxfun(@minus,c1,p),bsxfun(@minus,p,c2)];

%this gives quasi-sdf (correct sdf for points inside the box, and positive values for points outside)
%and the index of a column of diffs where max is attained. 
%WARNING: The choice of this index may introduce a bias!
[dist, maxI] = max(diffs,[],2);

%correct distances for outside points, leading to true sdf
out = dist>0;
dist(out) = sqrt(sum((p(out,:)-pn(out,:)).^2,2));


%%project inside points 

%for points inside the box (i.e. with dist<0), replace maxI-coordinate of pn by the respective coordinate of c1 
%if maxI is among the first d coordinates of the respective row of diffs, or by the respective coordinate of c2 
%if maxI is among the last d coordinates
d = size(p,2);
in = dist<0;
% for i=1:d %loop over d components
%     pn(maxI==i & in, i) = c1(i);
%     pn(maxI==d+i & in, i) = c2(i);
% end
%%an alternative to the above loop
%prepare c1/c2 components to replace p components
maxIin = maxI(in);
c = [c1,c2];
cin = c(maxIin)';
%linear indices of the components of p to be replaces
maxIinp = mod(maxIin,d); maxIinp(maxIinp==0) = d; %subtract d when the index > d, to get the index for pn
lind = sub2ind(size(p),find(in),maxIinp);
%replace components of pn
pn(lind) = cin;



%%%%%%%%%%%% ball in R^d %%%%%%%%%%%%%%%

function dist = sdf_ball(p,center,radius)
% Function sdf_ball
% true sign distance for the (d-1)-dimensional sphere in R^d with given center and radius. 
% 
%
% Syntax dist = sdf_ball(p,center,radius)
%
% OD 30 May 2017
%
% INPUT
%
% p -- n x d matrix of n points in R^d for which sdf is to be computed
% center -- 1 x d -vector of the center of the sphere
% r -- positive scalar, the radius of the sphere
%
% OUTPUT
%
% dist -- n x 1 vector of the signed distances of the points in p 

dist = sqrt(sum(bsxfun(@minus,p,center).^2,2)) - radius;

function [dist,pn] = npf_ball(p,center,radius)
% Function npf_ball
% find the (true) signed distances (dist) and nearest points (pn) to each point in p 
% (also correct for points outside the domain)
% in the (d-1)-dimensional sphere in R^d with given center and radius. 
% (returns just one nearest point in case there are more than one)
% the signed distances and nearst points are also correct outside of the domain
%
% Syntax [dist,pn] = npf_ball(p,center,radius)
%
% OD 31 May 2017
%
% INPUT
%
% p -- n x d matrix of n points in R^d for which sdf and pn are to be computed
% center -- 1 x d -vector of the center of the sphere
% r -- positive scalar, the radius of the sphere
%
% OUTPUT
%
% dist -- n x 1 vector of the signed distances of the points in p 
% pn -- n x d matrix of n points on the sphere, closest to the corresponding points in p 

%difference vectors to the center
dc = bsxfun(@minus,p,center);

%distances to the origin
d0 = sqrt(sum(dc.^2,2));

%signed distances
dist = d0-radius;

%for p coinciding with the center choose random directions
ic = find(d0<eps);
dc(ic,:) = randi(100,length(ic),length(center))-50.5;
d0(ic) = sqrt(sum(dc(ic).^2,2));  %fake distance to the origin; 
                                  %the true distance for these points is zero up to rounding error

%find nearest points on the boundary
pn = bsxfun(@plus, radius*bsxfun(@rdivide,dc,d0), center);  


function [dist, dp] = dpf_ball(p, dir, center, radius)
% Function dpf_ball
% given points (p) inside (d-1)-dimensional sphere and directions (dir), find the points (dp) of 
% the intersection of the sphere with the rays originating at p in the direction dir; 
% the distances (dist) between p and dp points are also computed;
% the (d-1)-dimensional sphere in R^d is given by its center and radius. 
% WARNINGS: 1) Points p are assumed to be inside the sphere
%           2) The directions in dir are assumed to be non-zero vectors
%
% Syntax [dist,dp] = dpf_ball(p, dir, center, radius)
%
% OD 28 June 2017
%
% Look for the solution in the form dp = p + t*dir, where t are parameter
% values. By substituting into the equation of the sphere and solving a quadratic equation, 
% obtain the formula: t=  sqrt(tmp.^2 +(r^2-||p-c||^2)/||dir||^2) - tmp, 
% where r=radius, c=center and tmp = (p-c,dir)/||dir||^2
%
% INPUT
%
% p -- n x d matrix of n points in R^d for which dist, etc. are to be computed
% dir -- n x d matrix of n direction vectors 
% center -- 1 x d -vector of the center of the sphere
% r -- positive scalar, the radius of the sphere
%
% OUTPUT
%
% dist -- n x 1 vector of the signed distances of the points in p 
% dp -- n x d matrix of n points on the sphere on the ray intersections


%difference vectors to the center
dc = bsxfun(@minus,p,center);

%distances to the origin squared
d0s = sum(dc.^2,2);

%squared length of the direction vectors
dirlsq = sum(dir.^2,2);

%store inner products (dc,dir) divided by dirlsq
tmp = sum(dc.*dir,2)./dirlsq;

%compute the parameter values and the points
t = sqrt(tmp.^2 + (radius^2-d0s)./dirlsq) - tmp; 
dp = p + bsxfun(@times,dir,t);

%compute the distances
dist = sqrt(sum((p-dp).^2,2));


%%%%%%%%%%%% disk %%%%%%%%%%%%%%%

function [d,pn]=ncircle(p,xc,yc,r)
%find the signed distances (d) and nearest points (pn) to each point in p  on the circle defined by the center (xc,yc) and radius r
%(returns just one nearest point in case there are more than one)
% the signed distance is also correct outside of the domain
%OD 23.11.2016

%difference vectors to the center
dx = p(:,1)-xc;
dy = p(:,2)-yc;

%distances to the origin
d0 = sqrt(dx.^2+dy.^2);

%signed distances
d = d0-r;

%for p coinciding with the circle center choose random directions
ic = find(d0<eps);
dx(ic) = randi(100,length(ic),1)-50.5;
dy(ic) = randi(100,length(ic),1)-50.5;
d0(ic) = sqrt(dx(ic).^2+dy(ic).^2); %fake distance; the true distance for these points is zero

%nearest points
pn = [xc + r*dx./d0, yc + r*dy./d0];



%%%%%%%%%%%%%% polygon %%%%%%%%%%%%%%%%%


function [ds,pn] = npf_onesegment(p,pv)
%OD:donesegment modified to return (in pn) for each point in p also the nearest point on the segment
% pv is a 2x2-matrix with endpoints of the segment in rows
% ds is the unsigned distance

e=ones(size(p,1),1);

v=diff(pv,1);
w=p-e*pv(1,:);

c1=sum(w.*v(e,:),2);
c2=sum(v(e,:).^2,2);

ds=0*e;
pn=zeros(size(p));

ix=c1<=0;
pn(ix,:) = pv(1*ones(sum(ix),1),:); %can be reused in the next line
ds(ix)=sqrt(sum((p(ix,:)-pv(1*ones(sum(ix),1),:)).^2,2));


ix=c1>=c2;
pn(ix,:) = pv(2*ones(sum(ix),1),:); %can be reused in the next line
ds(ix)=sqrt(sum((p(ix,:)-pv(2*ones(sum(ix),1),:)).^2,2));


ix=c1>0 & c2>c1;
nix=sum(ix);
if nix>0
    Pb=ones(nix,1)*pv(1,:)+c1(ix)./c2(ix)*v;
    ds(ix)=sqrt(sum((p(ix,:)-Pb).^2,2));
    pn(ix,:) = Pb;
end

function [dist,pn] = dpf_onesegment(p,dir,pv)
% returns in pn the intersection points between the segment defined by pv and the rays starting at points given in p in directions given in dir
% (We assume that the segment has a nonzero length)
%
% OD 27 June 2017
%
% INPUT
%
% p -- nx2-matrix of the coefficients of n points in 2D
% dir -- nx2-matrix of the coefficients of n direction vectors in 2D (must be nonzero vectors)
% pv -- 2x2-matrix containing the coordinates of the endpoints of the segment in rows
%
% OUTPUT
%
% pn -- nx2-matrix of the intersection points for each point in p, with NaNs as both components if there is no intersection
% dist -- nx1-vector of distances between points in p and pn


%initialize the vector of the intersection points by NaNs
pn = NaN*p;  

%compute the coefficients of the implicit equation ax+by=c of the straight line containing the segment
tau = pv(2,:)-pv(1,:); %tangential vector for the segment
a = -tau(2);
b = tau(1);
c = a*pv(1,1)+b*pv(1,2);

%find intersection points of the lines containing the rays p+t*dir with ax+by=c,
%which leads to the equation (a dir1 + b dir2)t = c - (a p1 + b p2) 
w = dir*[a;b];
ind = find(abs(w)>eps); %if abs(w)<=eps, then we assume that there is no intersection with the segment because 
                        %the direction is numerically parallell to the segment
ps = p(ind,:); %remove these points and directions
dir = dir(ind,:);
w = w(ind);
t = (c-ps*[a;b])./w; %parameter values of the intersection points for indices in ind
ind2 = find(t>=0); %exclude points such that intersection is in then opposite direction of the ray
ip = ps(ind2,:) + bsxfun(@times,dir(ind2,:),t(ind2)); %intersection points

%find which of these points are lying in the segment, for that find s such that ip = (1-s) pv1 + s pv2
% Then ip = pv1 + s tau and hence (ip,tau) = (pv1,tau) + s (tau,tau), s = (ip-pv1,tau)/||tau||^2
s = (bsxfun(@minus,ip,pv(1,:))*tau')/(a^2+b^2);
ind3 = find(s >= 0 & s <= 1); %indices into ind2 (and ip) of those points for which ip is in the segment 

%ind3=1:length(ip); %debugging

%store these points into pn
pn(ind(ind2(ind3)),:) = ip(ind3,:);

%compute the distances
dist = sqrt(sum((p-pn).^2,2));

%debugging
% figure(567)
% plot(pv(:,1),pv(:,2))
% hold on
% for i=1:size(p,1)
%    plot(p(i,1),p(i,2),'or')
%    plot(p(i,1)+dir(i,1),p(i,2)+dir(i,2),'xr')
%    plot([p(i,1);p(i,1)+dir(i,1)],[p(i,2);p(i,2)+dir(i,2)],'r')      
% end
% plot(pn(:,1),pn(:,2),'*r')
% hold off
% 
% pv = [1 1.5;2 2.7]; p=bsxfun(@plus,rand(10,2),[1.44 2.3]); dir = rand(10,2)-0.5;


function [d,pn]=npoly(p,pv)
%find the signed distances (d) and nearest points (pn) to each point in p  on the polygon defined by the vertices in pv
%(returns just one nearest point in case there are more than one)
% d and np are also correct outside of the domain
%OD 23 Nov 2016: adapted from dpoly of DistMesh 1.1

np=size(p,1);
nvs=size(pv,1)-1;

ds=zeros(np,nvs);
Pn = zeros(np,2*nvs);
for iv=1:nvs
    [ds(:,iv),Pn(:,[2*iv-1,2*iv])]=npf_onesegment(p,pv(iv:iv+1,:));
end
%minimal distance to segments 
[d,ivn]=min(ds,[],2);
%signed distance
d=(-1).^(inpolygon(p(:,1),p(:,2),pv(:,1),pv(:,2))).*d;
%determine projections
ind1 = sub2ind(size(Pn),(1:np)',2*ivn-1);
ind2 = sub2ind(size(Pn),(1:np)',2*ivn);
pn = [Pn(ind1),Pn(ind2)];


function [dist,pn]=dpf_poly(p,dir,pv)
% returns in pn the closest intersection points between the polygonal line defined by pv and the rays starting at points given in p in directions given in dir
% (We assume that all segments of the polygon have a nonzero length)
%
% OD 28 June 2017
%
% INPUT
%
% p -- nx2-matrix of the coefficients of n points in 2D
% dir -- nx2-matrix of the coefficients of n direction vectors in 2D (must be nonzero vectors)
% pv -- (m+1)x2-matrix containing the coordinates of the endpoints of m segments of the polygonal line, 
%       for a closed polygonal line (in particualr, polygon) the first and last vertex in pv must be the same
%
% OUTPUT
%
% pn -- nx2-matrix of the intersection points for each point in p, with NaNs as both components if there is no intersection
% dist -- nx1-vector of distances between points in p and pn, with NaN if there is no intersection


%initialize the matrices of the distances and intersection points by NaNs
n = size(p,1);
m = size(pv,1)-1;
pn = NaN*zeros(n,2*m);  
dist = NaN*zeros(n,m);

%loop over segments to compute intersection points and distances
for i=1:m
    [dist(:,i), pn(:,[2*i-1,2*i])] = dpf_onesegment(p,dir,pv(i:i+1,:));
end

%find minimum distances and their indices
[dist,ind] = min(dist,[],2,'omitnan');

%find points that deliver these distances
pn = [pn(sub2ind(size(pn),(1:n)',2*ind-1)), pn(sub2ind(size(pn),(1:n)',2*ind))];

%debugging
% figure(567)
% plot(pv(:,1),pv(:,2))
% hold on
% for i=1:size(p,1)
%    plot(p(i,1),p(i,2),'or')
%    plot(p(i,1)+dir(i,1),p(i,2)+dir(i,2),'xr')
%    plot([p(i,1);p(i,1)+dir(i,1)],[p(i,2);p(i,2)+dir(i,2)],'r')      
% end
% plot(pn(:,1),pn(:,2),'*r')
% hold off
% 
% pv = [1 1.5;2 2.7;2.5 1.6;1 1.5]; p=bsxfun(@plus,rand(10,2),[1 1.4]); dir = rand(10,2)-0.5;



%%%%%%%%%%%%%%% general %%%%%%%%%%%%%%%
function [d,pn] = ndiff(p,npf1,npf2)
%%npf for difference of two domains
%find signed distances (d) and nearest boundary points (pn) for the difference of two domains 
%WARNING: correct when the second domain is part of the first; TODO: check what happens otherwise

%find signed distances and projections for the boundaries of both domains
[d,pn] = npf1(p); %this initializes the output
[d2,pn2] = npf2(p);

%update those where distance and projections come from the second domain
d2 = -d2; %change the sign of d2
d1smaller = d < d2; %compare what is larger d=d1 or -d2 (sdf is given by max(d1,-d2))
d(d1smaller) = d2(d1smaller);
pn(d1smaller,:) = pn2(d1smaller,:);


function [d,pn] = nintersect(p,npf1,npf2)
%%find the signed distances (d) and nearest boundary points (pn) to each point in p for the intersection of two domains, 
%from known nearest points for each domain
%WARNING: signed distance and projection always correct for the points inside the domain, signed distance is underestimated
%at certain points outside the intersection if the union of the two domains is not convex. 
%The components of pn for these points are returned as NaN.

%find signed distances and projections for the boundaries of both domains
[d1,pn1] = npf1(p);
[d2,pn2] = npf2(p);

% %signed distance for the intersection
% d=max(d1,d2);
% 
% %find the (unsigned) distances to both boundaries
% d1 = sqrt(sum((p-pn1).^2,2));
% d2 = sqrt(sum((p-pn2).^2,2));

%compute the signed distances and for which domain it is larger
%(in particular this gives the index of the closest of two projections if p is inside the intersection)
[d,ind] = max([d1,d2],[],2);
%ind1 = (ind==1);
ind2 = (ind==2);

%put the corresponding projections into pn
pn = pn1; %zeros(size(pn1))*NaN;
%pn(ind1,:) = pn1(ind1,:);
pn(ind2,:) = pn2(ind2,:);

%%Determine wrong projections and set them to NaNs.
%identify indices of p that lie outside both domains
ind0 = d1 > 100*eps &  d2 > 100*eps;

if any(ind0)  
      
    %flags = ind0 + 2*ind2 has entries 0,1,2,3 as follows:
    % 0 if p lies in the union of the domains with projection on the boundary of the first
    % 1 if p lies outside both domains with projection on the boundary of the first
    % 2 if p lies in the union of the domains with projection on the boundary of the second
    % 3 if p lies outside both domains with projection on the boundary of the second
    flags = ind0 + 2*ind2;
    
    %for flags == 1 find pn not in the second domain and set them to NaNs
    indf1 = find(flags==1);
    [nd2,~] = npf2(pn(indf1,:));
    pn(indf1(nd2>100*eps),:) = NaN;
    
    %for flags == 3 find pn not in the first domain and set them to NaNs
    indf2 = find(flags==3);
    [nd1,~] = npf1(pn(indf2,:));
    pn(indf2(nd1>100*eps),:) = NaN;
    
    if any(isnan(pn))
        warning('some nearest boundary points are wrong, returned as NaN in pn')
    end
   
end


function [dist,pn] = dpf_intersect(p,dir,dpf1,dpf2)
%returns in pn the closest intersection points between points given in p in directions given in dir
%and the union of the curves (e.g. boundaries of domains) with dpf-functions given by dpf1,dpf2. 
%Serves as dpf-function for the intersection of two domains. 
%WARNING: incorrect for some point outside the intersection domain
% OD 28 June 2017

%compute the closest intersection points for both curves
[dist1,pn1] = dpf1(p,dir);
[dist2,pn2] = dpf2(p,dir);

%find minimum of two distances and their indices and convert ind into a logical 
%with 1 for the second set and 0 for the first set
[dist,ind] = min([dist1, dist2],[],2,'omitnan');
ind = logical(ind-1);

%store the points in pn
pn = pn1;
pn(ind,:) = pn2(ind,:);



%% Signed distance functions from DistMesh 1.1
% Homepage: http://persson.berkeley.edu/distmesh/
% COPYRIGHT.TXT
% DistMesh is a collection of MATLAB functions for generation and
% manipulation of unstructured meshes. DistMesh is Copyright (C) 2004-2012
% Per-Olof Persson.
%
% This program is free software; you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation; either version 2 of the License, or (at your
% option) any later version.
%
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program; if not, write to the Free Software Foundation, Inc.,
% 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
%
% If you use DistMesh in any program or publication, please acknowledge
% its authors by adding a reference to: Per-Olof Persson and Gilbert
% Strang, "A Simple Mesh Generator in MATLAB," SIAM Review Vol. 46 (2)
% 2004.



function d=drectangle0(p,x1,x2,y1,y2)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

d1=y1-p(:,2);
d2=-y2+p(:,2);
d3=x1-p(:,1);
d4=-x2+p(:,1);

d5=sqrt(d1.^2+d3.^2);
d6=sqrt(d1.^2+d4.^2);
d7=sqrt(d2.^2+d3.^2);
d8=sqrt(d2.^2+d4.^2);

d=-min(min(min(-d1,-d2),-d3),-d4);

ix=d1>0 & d3>0;
d(ix)=d5(ix);
ix=d1>0 & d4>0;
d(ix)=d6(ix);
ix=d2>0 & d3>0;
d(ix)=d7(ix);
ix=d2>0 & d4>0;
d(ix)=d8(ix);

function d=dpoly(p,pv)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

np=size(p,1);
nvs=size(pv,1)-1;

%ds=dsegment(p,pv); % donesegment MEXED
ds=zeros(np,nvs);
for iv=1:nvs
 ds(:,iv)=donesegment(p,pv(iv:iv+1,:));
end
d=min(ds,[],2);

d=(-1).^(inpolygon(p(:,1),p(:,2),pv(:,1),pv(:,2))).*d;

function ds=donesegment(p,pv)

e=ones(size(p,1),1);

v=diff(pv,1);
w=p-e*pv(1,:);

c1=sum(w.*v(e,:),2);
c2=sum(v(e,:).^2,2);

ds=0*e;

ix=c1<=0;
ds(ix)=sqrt(sum((p(ix,:)-pv(1*ones(sum(ix),1),:)).^2,2));

ix=c1>=c2;
ds(ix)=sqrt(sum((p(ix,:)-pv(2*ones(sum(ix),1),:)).^2,2));

ix=c1>0 & c2>c1;
nix=sum(ix);
if nix>0
 Pb=ones(nix,1)*pv(1,:)+c1(ix)./c2(ix)*v;
 ds(ix)=sqrt(sum((p(ix,:)-Pb).^2,2));
end

function d=dintersect(d1,d2)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

d=max(d1,d2);

function d=dcircle(p,xc,yc,r)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

d=sqrt((p(:,1)-xc).^2+(p(:,2)-yc).^2)-r;


function d=ddiff(d1,d2), d=max(d1,-d2);

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.


