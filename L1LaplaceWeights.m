function weights = L1LaplaceWeights(nodes,i,si,prob,porder, mu,try_positive)
% Function L1LaplaceWeights: L1 Laplace weights for i-th node with set of influence given by si
%
% Compute numerical differentiation weights for Laplace operator at i-th node of nodes.X with the set of influence given by 
% the nodes with indices in si, using the function L1LaplaceStencil.m 
%
% INPUT 
%
% nodes -- the nodes structure
% i - index of the current node into the rows of nodes.X
% si - vector of indices of the set of influence into the rows of nodes.X
% prob - the prob structure
% porder -- polynomial order (degree+1) (integer >= 1)
% mu -- power of the L1 weights (real number >= 0) 
% try_positive -- flag to setup an attempt to compute a positive stencil, see the info in L1Stencil.m.
%             0: no attempt to find positive stencils
%             1: run L1LaplaceStencil again with positive=0 if it fails, i.e., then an ordinary stencil is computed. 
%             NOTE: since positive stencils for Laplacian do not exist when porder > 4, this flag should be set to 0 in this case
%
% OUTPUT
%
% weights -- a column vector of stencil weights for the Laplacian
%
%
% INFO
%
% See L1LaplaceStencil.m and L1Stencil.m for further details
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.



z = nodes.X(i,:);
X = nodes.X(si,:);


[weights,~,condn,~]  = L1LaplaceStencil(z, X, porder, mu, try_positive);
if try_positive  %if positive stencil cannot be found, then drop this condition and recompute;
    if any(isnan(weights)) 
        [weights, ~, condn, supp] = L1LaplaceStencil(z, X, porder, mu,0);
        if any(weights(2:end)<-100*eps) %~any(isnan(weights))
            %update the number of non-positive stencils
            updateSTATS('sum','np_stencils',1);
            %disp(['positive stencil not found for i=',int2str(i)])
        end
    end
end
    
%collect info about condition numbers
updateSTATS('max','maxcondLaplace',condn);

%set to exact zero all numerically zero weights
weights(abs(weights)<100*eps) = 0;

