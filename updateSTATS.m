function [varargout] = updateSTATS(type,varargin)
% Function updateSTATS: maintain statistics
%
% Create and update auxiliary information (statistics, debugging info, etc.) in the fields of the persistent structure STATS.
% Only updates a field if STATS has been initialized by calling updateSTATS with type='init', and
% the respective field has been created beforehand by calling updateSTATS with type 'initf' or 'debug'
%
% INPUT
%
% type -- a string to choose an update option
% varargin -- see individual types
%
% OUTPUT
% varargout -- see individual types
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%declare the persistent variable for statistics.  
%TODO: If STATS is very big and returning persistent when type='return' means copying it (check this), 
% then a global declaration may be more useful then persistent(?)
persistent STATS


switch lower(type)
    
    case 'init' %(re)initialize STATS as a struct (all data previously stored in STATS is destroyed)
        
        %set title and initialization date and generate file name for storing with 'save'
        start_datetime = char(datetime('now','Format','uuuu-MM-dd_HH:mm:ss')); %date and time string
        STATS = struct('title','STATS','date',start_datetime,'filename',['STATS_',start_datetime,'.mat']); 
        
        %optional actions
        if ~isempty(varargin)
            
            %set title to the string in varargin{1}
            STATS.title = varargin{1};
            
            %insert into filename an optinal string given by varargin{2}.
            %A completely different file name can be set by updateSTATS('set','filename',new_filename),
            %where new_filename is a string
            if length(varargin) > 1
                STATS.filename = [STATS.title,'_',varargin{2},'.mat'];
            end
            
        end
        
        
     case 'initf'  %init field(s): create or reinitialize a field or several fields listed in varargin{1} and initialize by the same init_value in varargin{2}
                   %STATS must exist as structure
                   %init_value may be for example NaN, Inf, -Inf, 0, []
                   
        %do nothing if 'init' has not been run (otherwise STATS is anyway created)
        if ~isfield(STATS,'title')
            return;
        end
                   
        fieldname = varargin{1};
        init_value = varargin{2};
        
        %(re)initialize STATS with the fields defined by fieldname and set them to NaN
        %(all data previously stored in it is destroyed)
                
        if iscell(fieldname)
            
            for i=1:length(fieldname)
                STATS.(fieldname{i}) = init_value;
            end
            
        elseif ischar(fieldname)
            STATS.(fieldname) = init_value;
        else
            error('second argument must be either a string or a cell array of strings')
        end
        
    case 'on'   %check whether statistics with name varargin{1} have been initialized (by calling this function with type='initf') 
        
        fieldname = varargin{1}; %name of statistics field
        
        if isfield(STATS,fieldname)
            varargout{1} = true;
        else
            varargout{1} = false;
        end
        
        
    case 'return'
        
        %return the structure STATS
        if isfield(STATS,'title')
            varargout{1} = STATS;
        else
            varargout{1} = [];
        end

        
    case 'save'
        
        %save STATS into file
        save(STATS.filename,'STATS')
        
    case 'clear'
        
        %clear the persistent variable STATS
        clear STATS
               
        
    case 'debug' %initialize/switch on debugging: create or reinitialize a field or several fields listed in varargin{1} and initialize them as structs 
                 %with no fields 
                 %the existence of these names serves as flags to switch on particular debugging streams
                 %example: updateSTATS('debug','debug1')
                 
        %do nothing if 'init' has not been run (otherwise STATS is anyway created)
        if ~isfield(STATS,'title')
            return;
        end
        
        fieldname = varargin{1};
        if iscell(fieldname)
            
            for i=1:length(fieldname)
                STATS.(fieldname{i}) = struct;
            end
            
        elseif ischar(fieldname)
            STATS.(fieldname) = struct;
        else
            error('second argument must be either a string or a cell array of strings')
        end
        
    case 'dbon' %check whether the struct field was created (by calling this function with type='debug') meaning debugging of this stream is 'on' 
                %example: if updateSTATS('dbon','debug1'), ...arbitrary code..., end -- meaning that the code will only be executed if debugging 
                %under the name 'debug1' is on, that is it has been switched on by updateSTATS('debug','debug1')
        
        dbname = varargin{1}; %name of debugging stream
        
        if isfield(STATS,dbname)
            if isstruct(STATS.(dbname)) %check that varargin{1} is a field in STATS
                varargout{1} = true;
            else
                varargout{1} = false;
            end
        else
            varargout{1} = false;
        end

        
    case 'dbset' %create a field in the debugging structure STATS.(dbname) if not already exists, and store there any values
        %example: updateSTATS('dbset','debug1','matrixA',A) then A can be found as debug1.matrixA in STATS structure
        %if the field already exists, then its content is ovewritten
        
        dbname = varargin{1}; %name of debugging structure
        fieldname = varargin{2}; %name of a field to be introduced and filled into debugging structure (only if it does not exist already)
        value = varargin{3}; %variable to store at this field (the field must not exist, it is created here)
        
        if isfield(STATS,dbname) %check that debugging of this stream is on
            %store the value at fieldname
            STATS.(dbname).(fieldname) = value; %create the field of this name in structure STATS.(dbname) if needed, and store the value there
            
        end
        
    case 'dbget' %return the value stored at fieldname of the debugging structure STATS.(dbname) 
                 %returns [] if dbname is not on or fieldname does not exist
                 %(can be used in particular to check the value of a flag)
        
        varargout{1} = [];
        dbname = varargin{1}; %name of debugging structure
        fieldname = varargin{2}; %name of a field in the debugging structure 
        
        if isfield(STATS,dbname) %check that debugging of this stream is on
            if isfield(STATS.(dbname),fieldname) %check whether this field exists 
                varargout{1} = STATS.(dbname).(fieldname); %return its content
            end
        end
        
       
    case 'dbstorecell' %create a field in the debugging structure STATS.(dbname) if not already exists,
        %and store there (as elements of a one-dimensional cell) any multiple values by repeated calls;
        %example: updateSTATS('dbsetcell','debug1','data',A); updateSTATS('dbsetcell','debug1','data',B);
                 %then A and B can be found in the cell debug1.data in STATS structure

        
        dbname = varargin{1}; %name of debugging structure
        fieldname = varargin{2}; %name of a field to be introduced and filled into debugging structure (only if it does not exist already)
        value = varargin{3}; %variable to store at this field (the field must not exist, it is created here if needed)                            
        
        if isfield(STATS,dbname) %check that debugging of this stream is on
            
            if isfield(STATS.(dbname),fieldname) %check whether this field exists already
                
                f = STATS.(dbname).(fieldname); %copy of the field to do checks                
                if iscell(f)  && sum(size(f)>1) < 2 %check that fieldname is a one-dimensional cell; 
                                                    %the length check (&& length(size(c)) < 3) does seem necessary
                    %the field exists and is a cell
                    STATS.(dbname).(fieldname){end+1} = value; %store the new value at the end of the same cell
                else
                    %otherwise nothing happen
                    warning('Storing failed')
                    disp(['since the field ''',fieldname,''' in the debugging structure ''',dbname,''''])
                    disp(' is neither a cell nor a one-dimensional cell array')
                end
                
            else %the field does not exist
                STATS.(dbname).(fieldname){1} = value; %create the field of this name in structure STATS.(dbname) and store the value there
                                                       %as the first element of a cell
                
            end
            
        end
        
    case 'dbstorearray' %create a field in the debugging structure STATS.(dbname) if not already exists,
        %and store there (as an array of higher dimension) any (multivariate) arrays of the same size by repeated calls;
        %similar to 'dbstorecell' but results in an array
        %NOTE: extension of the higher dimensional array perfomed by concatenation along its last dimension, as in the example:
        %a=rand(2,3,2), b=rand(2,3), a=cat(length(size(a)),a,b), where b has one dimension less then a and all the same first sizes as a;
        %or a=rand(2,5), b=rand(2,3), a=cat(length(size(a)),a,b), where b has the same dimension as a and all but last sizes the same;
        %for matrices a=cat(length(size(a)),a,b) is equivalent to a=[a b], e.g.  a=rand(2,3), b=rand(2,1), [a b]; or a=rand(2,3), b=rand(2,5), [a b];
        %the array does not have to be numerical (may be a cell array of arbitrary objects as well); however a and b must be compatible,
        %in particular if a is numerical, than b must be numerical, too, and if a is a cell, then b must be a cell array, too.
        
        
        dbname = varargin{1}; %name of debugging structure
        fieldname = varargin{2}; %name of a field to be introduced and filled into debugging structure (only if it does not exist already)
        value = varargin{3}; %variable to store at this field (the field must not exist, it is created here if needed)
        
        if isfield(STATS,dbname) %check that debugging of this stream is on
            
            if isfield(STATS.(dbname),fieldname) %check whether this field exists already
                
                sf = size(STATS.(dbname).(fieldname)); %size of the array in fieldname
                sv = size(value); %size of the array in value
                if sv == sf(1:end-1) || sv(1:end-1) == sf(1:end-1) %check that the sizes of fieldname and value are compatible

                    STATS.(dbname).(fieldname) = cat(length(sf),STATS.(dbname).(fieldname),value); %concatenate the array, 
                                                                                                   %appending value at the "end" of fieldname
                else
                    %otherwise nothing happen
                    warning('Storing failed')
                    disp(['since the field ''',fieldname,''' in the debugging structure ''',dbname,'''',' is not compatible with this object'])
                end
                
            else %the field does not exist
                
                STATS.(dbname).(fieldname) = value; %create the field of this name in structure STATS.(dbname) and store the value there

                
            end
            
        end
        
    case 'set' %store value stored at fieldname (only if the respective field is already created (by 'init' or 'initf'));
              %storing again will overwrite
        
        fieldname = varargin{1};
        value = varargin{2};
        
        if isfield(STATS,fieldname)            
            
            %store the value at fieldname
            STATS.(fieldname) = value;
        end
    
    case 'get'
        
        %return the value at fieldname (can be used in particular to check the value of a flag)
        %returns [] if fieldname does not exist
        fieldname = varargin{1};
        if isfield(STATS,fieldname)
            varargout{1} = STATS.(fieldname);
        else
            varargout{1} = [];
        end

        
      
    case 'max' %update stored_value if current value is greater
        
        fieldname = varargin{1};
        value = varargin{2};
        
        %does the job only if the respective field is already created (by 'init' or 'initf')
        if isfield(STATS,fieldname)
    
            %get the current value stored in the field given by fieldname
            stored_value = STATS.(fieldname);
            
            %update using the current value
            STATS.(fieldname) = max(stored_value, value);
            
        end
        
     case 'sum' %update stored_value by adding the current value 
                %(must be initialized by zero)
        
        fieldname = varargin{1};
        value = varargin{2};
        
        %does the job only if the respective field is already created (by 'init' or 'initf')
        if isfield(STATS,fieldname)
    
            %get the current value stored in the field given by fieldname
            stored_value = STATS.(fieldname);
            
            %update using the current value
            STATS.(fieldname) = stored_value + value;
            
        end 
        
      case 'fillm' %fill matrix: update stored_value (a matrix) by overwriting one or more of its columns by the current value 
                   %(a matrix with the same number of rows as stored_value) using the command stored_value(:,i) = value, 
                   % where i is either the array of indices of the columns to be updated, or the logical indices.
                   %WARNING: size and type of the matrices and i must match so that  stored_value(:,i) = value is a valid command.
        
        fieldname = varargin{1};
        value = varargin{2};
        i = varargin{3}; %the indices of the columns which we update
        
        %does the job only if the respective field is already created (by 'init' or 'initf')
        if isfield(STATS,fieldname)
            
            %update using the current value
            STATS.(fieldname)(:,i) = value;
            
        end  
        
      case 'extm' %extend matrix: update stored_value (a matrix) by extending it by the current value (a matrix with the same number of rows)
                  %using the command [stored_value, value]. Initialize by [] or by an array. 
                  %WARNING: size and type of the matrices must match so that [stored_value, value] is a valid command.
        
        fieldname = varargin{1};
        value = varargin{2};
        
        %does the job only if the respective field is already created (by 'init' or 'initf')
        if isfield(STATS,fieldname)
    
            %get the current value stored in the field given by fieldname
            stored_value = STATS.(fieldname);
            
            %update using the current value
            STATS.(fieldname) = [stored_value, value];
            
        end  
        
      case 'extc' %extend cell vector: update stored_value (row cell vector) by extending by the current value (any object that becomes the next entry in the cell vector)
                  %(must be initialized by [], by a cell, or by a row cell vector)
                  %
        
        fieldname = varargin{1};
        value = varargin{2};
        
        %does the job only if the respective field is already created (by 'init' or 'initf')
        if isfield(STATS,fieldname)
    
            %get the current value stored in the field given by fieldname
            stored_value = STATS.(fieldname);
            
            %update using the current value
            STATS.(fieldname) = [stored_value {value}];
            
        end   
       
    otherwise
        error('unknown type')
end


