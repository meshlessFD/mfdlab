function prob = setup_problem_Laplace(dim,domain_id,problem_id,ccoef,bctype)
% Function setup_problem_Laplace: elliptic problems of type -Delta u + cu = f
%
% Set up the prob structure for elliptic test problems of type -Delta u + cu = f
%
% INPUT
%
% dim -- the number of space variables
% domain_id -- domain identifier
% problem_id -- problem identifier
% ccoef -- the c coefficient either as a number or a scalar function of the type @(x) for multiple points x in R^dim
% bctype -- type of boundary conditions
% 
% OUTPUT
%
% prob -- structure that stores the definition of the problem
%        prob.id -- identifier
%        prob.type -- set to 'Laplace'
%        prob.bctype -- type of the boundary condition (if not implicitly set in prob.id)
%        prob.dom -- a structure that describes the domain
%                  dom.id -- domain identification string
%                             Options:
%                             'unit cube' %unit cube (0,1)^d
%                             'box' -- d-dimensional hyperrectangle with lower and upper corners given by dom.c1 and dom.c2
%                  dom.dim -- domain dimensionality
%                  dom.sdf -- (quasi-) signed distance function that gives negative distance inside the domain and negative
%                             values outside
%                             Syntax:  @(p), see set_sdf.m
%        prob.f -- function in the RHS of the equation -Delta u + cu = f in Omega
%                  syntax: y=f(x), x an (nx x d)-matrix of points in R^d, y an (nx x 1)-vector of values of the force term at these points
%        prob.g -- function that defines the Dirichlet boundary conditions u|_Gamma = g (Gamma: boundary of Omega)
%                  syntax: y=g(x), x an (nx x d)-matrix of points in R^d, y an (nx x 1)-vector of values of u at these points
%        prob.gn -- function that defines the Neumann boundary condition
%        prob.c -- defines the coefficient c of u: either a constant or a function with syntax:
%                  y=c(x), x an (nx x d)-matrix of points in R^d, y an (nx x 1)-vector of values
%                  NOTE: prob.c does not exist if c=0 to prevent assembling the mass matrix in mFD_Elliptic.m
%
% TODO: add reading user-defined problems (to be described in separate files)
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%% setup the domain
% ('bb' is a boundung box defined by its diagonal points: a 2 x d matrix)
switch domain_id
    
    case 'unit cube' %unit cube (0,1)^d
        
        domain = struct('id','unit cube','dim',dim,'convex',1);
        domain.bb = [zeros(1,dim); ones(1,dim)]; %tight bounding box
        domain.normalbnd = @(x) normalbnd_box(x,zeros(1,dim),ones(1,dim));
        
    case 'unit ball'%unit ball in R^d

        domain = struct('id','unit ball','dim',dim,'center',zeros(1,dim),'radius',1,'convex',1);
        %add the tight bounding box
        domain.bb = [domain.center - domain.radius*ones(1,dim); domain.center + domain.radius*ones(1,dim)];
        domain.normalbnd = @(x) x;
        
    case 'ball\ball' %"ball minus ball": ball of radius 2 centered at the origin with a spherical hole of radius 1 
                     %centered at inner_center=0.31*(1,2,...,d)/||(1,2,...,d)||_2
        
        %compute inner_center
        inner_center = 1:dim;
        inner_center = 0.31*inner_center/norm(inner_center);

        domain = struct('id','ball\ball','dim',dim,'inner_center',inner_center,'convex',0);
        domain.bb = [-2*ones(1,dim); 2*ones(1,dim)];
        domain.normalbnd = @(x) normalbnd_ball_m_ball(x,inner_center);
        
        
    case 'adaptmesh' %2D disc sector with re-entrant corner of angle pi/2: domain in the example for adaptmesh 
                      %of MATLAB's PDE Toolbox, see set_sdf.m
        
        domain = struct('id','adaptmesh','dim',2,'convex',0);
        %add the tight bounding box 
        domain.bb = [-1/sqrt(2) -1; 1 1];
        

         
    otherwise
        error('unknown domain_id')
end

%expand bounding box
domain.bbm = [3 -1; -1 3]/2*domain.bb; %bounding box with margin (needed e.g. for isosurface extraction)

%assign the appropriate sign distance function as domain.sdf
domain = set_sdf(domain);

% %add it to the problem structure
% prob.dom = domain;


%% setup the problem
prob = struct('id',problem_id,'type','Laplace','dom',domain,'c',ccoef,'bctype',bctype);

%fill out the remaining fields of prob
switch problem_id
    
    case 'u = prod(sin(pi*x),2)'
        
        %assign reference solution given by the exact solution
        prob.rs = @(x) prod(sin(pi*x),2);
        
        %assign function to evaluate Dirichlet boundary conditions (given by reference solution)
        prob.g = prob.rs;
        
        %assign function to evaluate gradient of reference solution
        %(needed for evaluation of the error of gradients of numerical solution
        %and can be used to set Neumann boundary conditions)
        prob.gradrs = @(x) gradrs (x,problem_id);    
        
        %assign right hand side 
        dpi2 = dim*pi^2;
        if isa(prob.c,'function_handle') %c is a function
            prob.f = @(x) (dpi2 + prob.c(x)).*prod(sin(pi*x),2);
        elseif prob.c %prob.c is a nonzero coefficient
            prob.f = @(x) (dpi2 + prob.c)*prod(sin(pi*x),2);
        else
            prob.f = @(x) dpi2*prod(sin(pi*x),2);
            prob = rmfield(prob,'c'); %remove the field c to prevent unnecessary computations in mFD_Elliptic.m
        end
        
    case 'u = sin(10*sum(x,2))'
        
        %assign reference solution given by the exact solution
        prob.rs = @(x) sin(10*sum(x,2));
        
        %assign function to evaluate Dirichlet boundary conditions (given by reference solution)
        prob.g = prob.rs;
        
        %assign function to evaluate gradient of reference solution
        %(needed for evaluation of the error of gradients of numerical solution
        %and can be used to set Neumann boundary conditions)
        prob.gradrs = @(x) gradrs (x,problem_id);    
        
        %assign right hand side 
        coef = 100*dim;
        if isa(prob.c,'function_handle') %c is a function
            prob.f = @(x) (coef + prob.c(x)).*sin(10*sum(x,2));
        elseif prob.c %prob.c is a nonzero coefficient
            prob.f = @(x) (coef + prob.c)*sin(10*sum(x,2));
        else
            prob.f = @(x) coef*sin(10*sum(x,2));
            prob = rmfield(prob,'c'); %remove the field c to prevent unnecessary computations in mFD_Elliptic.m
        end
      
    case 'adaptmesh'
        
        %assign reference solution given by the exact solution
        prob.rs = @(x) (x(:,1).^2+x(:,2).^2).^(1/3).*cos(2/3.*atan2(x(:,2),x(:,1)));
        
        %assign function to evaluate Dirichlet boundary conditions (given by reference solution)
        prob.g = prob.rs;
        
        %assign right hand side (force term)
        prob.f = @(x) zeros(size(x,1),1);
        
        %remove the field c
        prob = rmfield(prob,'c'); %the value of ccoef won't be used
       
        
    otherwise
        error('unknown problem_id')
        
end

%% additional setup for Neumann and mixed boundary

%assign function to evaluate Neumann boundary conditions (given by reference solution)
if strcmp(bctype,'Neumann') || strcmp(bctype,'mixed')
    
    %check that domain boundary normal function is assigned
    if ~isfield(prob.dom,'normalbnd') &&  ~isfield(prob,'gradrs')
        error('need domain boundary normal function')
    end
    
    %function to evaluate Neumann boundary conditions
    prob.gn = @(x) sum(prob.gradrs(x).*prob.dom.normalbnd(x),2); %inner products of gradients with normals at x
    
end

%determine which part of the boundary will be Neumann
if strcmp(bctype,'mixed')
    
    switch domain_id
        
        case 'ball\ball' %Neumann on inner boundary and Dirichlet on outer boundary
            
            %we set up a function that returns true for boundary nodes on Newmann boundary and false for Dirichlet boundary nodes
            %WARNING: it returns mock true and false values also for interior nodes, but this has no meaning
            prob.neumannbnd = @(x) neumannbnd_ball_m_ball(x);
            
        otherwise
            error('mixed problem is not defined for this domain')
        
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Auxiliary functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function g = gradrs (x,problem_id)
% computes the gradient of the reference solution for various test problems
%
% INPUT
% x - n x d matrix with coordinates of n points in R^d
% problem_id - identificator of the problem
%
% OUTPUT
% g - n x d matrix with coordinates of the gradients at those n points in R^d

switch problem_id
    
    case 'u = prod(sin(pi*x),2)'
        
%         if size(x,2)==2
%             g=pi*[cos(pi*x(:,1)).*sin(pi*x(:,2)) sin(pi*x(:,1)).*cos(pi*x(:,2))];
%             return;
%         end
        
        Co = pi*cos(pi*x); %cosines of all components multiplied by pi (derivative of sine)
        F = sin(pi*x); %init as matrix of sines of all components
        
        g = nan(size(x)); %init graent matrix
        for i=1:size(x,2) %loop over number of variables
            sini = F(:,i); %temporarily store the i-th component of the sine matrix
            F(:,i) = Co(:,i); %replace sines by pi*cosines in the i-th component
            g(:,i) = prod(F,2); %compute the i-th component of the gradient
            F(:,i) = sini; %restore the sine matrix
        end
        
    case 'u = sin(10*sum(x,2))'
        
        g = repmat(10*cos(10*sum(x,2)),1,size(x,2));
        
    otherwise
        
        error('gradient of reference solution is not implemented for this problem')
        
end

function n = normalbnd_box (x,c1,c2)
%computes the normal vector for the boundary of the domain
%WARNING: produces mock values for points x that do not belong to the boundary
%WARNING: this routine has not yet been debugged
%
% INPUT
% x - m x d matrix with coordinates of m points in R^d
% c1,c2 - parameters of the box
%
% OUTPUT
% n - m x d matrix with coordinates of the normals at those m points in R^d
%     WARNING: all components are nan for points numerically not inside facets of the box
%              (for example corners of the rectangle in 2D)

dim = size(x,2);

%subtract the center of the box to obtain the vector pointing from the center to the point
x = bsxfun(@minus,x,0.5*(c1+c2));

%find the index of largest absolute value component of this vector
%(shifted by dim if normal is a positive coordinate unit vector)
xx = [-x x];
[xmax, imax] = max(xx,[],2);

%find all points numerically not inside facets of the box
%(they have at least 2 components of numerically the same absolute value)
nofacets = sum(abs(bsxfun(@minus,xx,xmax)) < 100*eps,2) > 1;

%determine the sign of the normal
sign = imax>dim;

%correct the index of the positive normals
%WARNING: imax is chaged here
imax(sign) = imax(sign)-dim;

%set the normal as a positive or negative coordinate unit vector
n = zeros(size(x)); %init by zeros
n(sub2ind(size(n),1:size(n,1),imax')) = 2*sign-1; %replace in each row one zero by 1 or -1 as appropriate

%replace normals by nan for points numerically not inside facets of the box
n(nofacets,:) = nan;

        

function n = normalbnd_ball (x,center,radius)
%computes the normal vector for the boundary of the domain
%WARNING: produces mock values for points x that do not belong to the boundary
%
% INPUT
% x - m x d matrix with coordinates of m points in R^d
% center,radius - parameters of the ball
%
% OUTPUT
% n - m x d matrix with coordinates of the normals at those m points in R^d

n = bsxfun(@minus,x,center)/radius;

function n = normalbnd_ball_m_ball (x,inner_center)
%computes the outer normal vector for the boundary of the domain 'ball\ball'
%WARNING: produces mock values for points x that do not belong to the boundary
%
% INPUT
% x - m x d matrix with coordinates of m points in R^d
%
% OUTPUT
% n - m x d matrix with coordinates of the normals at those m points in R^d

%for all points determine whether they belong to outer or inner boundary
inner = sum(x.^2,2) < 3; %distance to the origin < sqrt(3) distinguishes both

%first use the formula for outer boundary on all points
n = x/2;

%correct for inner boundary
n(inner,:) = bsxfun(@minus,inner_center,x(inner,:));

function neumann = neumannbnd_ball_m_ball(x)
%for boundary nodes, returns true for those on the inner boundary (where we will use Neumann condition for 'ball\ball'),
%and false for nodes on the outer boundary 
%WARNING: for any non-boundary points returns mock values
%
% INPUT
% x - m x d matrix with coordinates of m points in R^d
%
% OUTPUT
% neumann - m x 1 logical vector with true for Neumann boundary nodes and false for Dirichlet boundary nodes

neumann = sum(x.^2,2) < 3; %see comment in normalbnd_ball_m_ball 




