function   [weights,condn] = RbfGradientStencil(Z,X,type,par,ep,porder)
% Function RbfGradientStencil:  weights of RBF numerical differentiation formula for gradient
%
% Compute the weights of RBF numerical differentiation formula for the gradient at a set of points Z in R^d,
% given function values at X\subset R^d using RBF interpolation, optionally with a polynomial term.
%
%
% INPUT
%
% Z -- an (nz x d)-matrix of nz points in R^d where the gradient is evaluated
% X -- an (nx x d)-matrix of nx points in R^d where function values are known
% type -- a string defining the type of RBF, see frbf.m for type description;
%         in addition to the types of frbf.m accepts 'gQR' (Gauss-QR) for d=2 and porder=0 
% par -- parameter of the RBF of particular type
% ep -- shape parameter
% porder -- oder of the polynomial part
%
% OUTPUT
%
% weights -- an (nx x d*nz)-matrix whose columns are the stencil weights for
%            the corresponding partial derivatives evaluated at all points z in Z one after another
%            according to the following ordering of columns:
%            [partial^1 f(z_1) ... partial^d f(z_1) ... ... partial^1 f(z_nz) ... partial^d f(z_nz)]
% condn -- condition number of the kernel matrix, see  RbfStencil.m
%
% TODO: in the case when shifting and upscaling are used we run a loop over points in Z,
% solving a linear system each time. Improve this with the help of a routine for the evaluation of the gradients 
% of monomials at arbitrary points. (Currently we evaluate them only at the origin thanks to shifting.)
%
%
% INFO
%
% See comments in RbfStencil.m and Gauss_QR_diffmat_2D.m
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2014-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


[nx, dim] = size(X);
nz = size(Z,1);
% if size(Z,2) ~= dim
%     error('points of unequal dimensions')
% end


%% Gauss-QR weights
if strcmp(type,'gQR')
    
    if porder > 0
        error('porder not implemented')
    end
      
    if dim == 2   
        
        [wcell,Psi] = Gauss_QR_diffmat_2D({'x','y'},Z,X,ep);    
        weights = cell2mat(wcell)'; 
        weights = reshape(weights,nx,[]); 
        
        condn = cond(Psi.A0);
               
    else        
        error('dim not implemented')        
    end
    
    return;
end


%% set up the function for the evaluation of the RBF part of the right hand side
rhsf = @(Z,X,type,par,ep) gradkermatX(Z,X,type,par,ep);

%% two cases where no loop over points in Z is used

%1) no polynomial term 
if ~porder    
    [weights,~,condn] = RbfStencil(Z,X,type,par,ep,porder,rhsf,[],1);
    return
end

%2) constant polynomial term with non-scalable RBF;
%this is the case with constant polynomial part where no shifting and upscaling is done in RbfStencil.m
%(in fact we could ignore the second condition because power/TPS together with porder == 1 
% will not give approximation of the gradient; include it to just avoid unnecessary error messages)
if porder == 1 && ~(strcmp(type,'p') || (strcmp(type,'tp') && ~mod(par,2))) 
    
    [weights,~,condn] = RbfStencil(Z,X,type,par,ep,porder,rhsf,zeros(1,dim*nz),1);
    return
    
end

%% compute the polynomial part cz of the rhs for a single point of Z 
%(assuming evaluation of the weights at the origin, for each point in Z separately)
cz = [zeros(1,dim); ...
      eye(dim); ...
      zeros(nchoosek(porder+dim-1,dim)-dim-1,dim)];
  
%% call RbfStencil.m in a loop to compute the weights for all points in Z
weights = nan(nx,dim*nz); %init the array for weights
condn = NaN; %init condition number
for i=1:nz
    
    [weightsz,~,condnz] = RbfStencil(Z(i,:),X,type,par,ep,porder,rhsf,cz,1);
    
    %fill in the corresponding fields of the array of the output weights
    weights(:,(i-1)*dim+1:i*dim) = weightsz;
    
    %update condn
    condn = max(condn,condnz);
end

