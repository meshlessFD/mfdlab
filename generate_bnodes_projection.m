function [nodes] = generate_bnodes_projection(spacing_coef,domain,nodes)
% Function generate_bnodes_projection: boundary nodes by projecting interior nodes
%
% Generates boundary nodes by projecting some of exisiting interior nodes to the boundary
%
% INPUT
%
% spacing_coef - factor to determine how far from the boundary we choose the nodes to project
% domain - structure describing the domain we generate the nodes for
% nodes -- structure containing existing interior nodes
%
% OUTPUT
%
% nodes -- the nodes structure completed with boundary nodes
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2018-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

%dimensionality of the domain
dim = domain.dim;

if dim == 1
    error('projection method is not used for 1D domains')
end

%determine spacing of interior nodes
if strcmp(nodes.itype,'grid')
    h = nodes.h; %this uses step size of the uniform grid
else
    h = nodes.spacing; %approximate spacing (assuming quasi-uniform nodes)
end

%project to the boundary all nodes at distance at most spacing_coef*h from the boundary, and make them boundary nodes
lind =  domain.sdf(nodes.X) > -spacing_coef*h + 100*eps; %logical indices of points to be projected
[~,Xb] =  domain.npf(nodes.X(lind,:));

%add the boundary points to nodes structure
nodes.X = [nodes.X; Xb];
nodes.ib = length(nodes.ii)+1 : size(nodes.X,1); %indices of boundary nodes

% %messages/visualization
% disp('boundary nodes: projection')
% disp(['  project interior nodes with distance from boundary < ',num2str(spacing_coef),'*h'])
% if dim==2, figure(88), plot(Xb(:,1),Xb(:,2),'*'), axis([-2 2 -2 2]), end

