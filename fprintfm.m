function fprintfm (formatSpec,A)
% Function fprintfm: frintf applied to the columns of matrix A
%   
% Display formatted data stored in a matrix columnwise. Output is similar to disp(A'),
% but formatting is controlled via formatSpec. Uses fprintf and passes formatSpec to it. 
%
% INPUT
% 
% formatSpec - format string for the output fields, see fprintf
% A          - matrix
%
%
% Written by Oleg Davydov, 2020


fprintf('\n');
for i=1:size(A,2)
    fprintf(formatSpec,A(:,i));
    fprintf('\n');
end

