function [X] = isoline_nodes(spacing,v,ls,lsxy,bb,fign)
% Function isoline_nodes: choose points on isolines defined by ls=v
%
% Given a level set function ls choose points on its isolines ls=v with approximate distance "spacing" 
%
% INPUT
%
% spacing -- target spacing on the isoline
% v -- row vector of levels for isolines
% ls -- level set function
% lsxy -- the same level set function as function of x, y, where x,y may be matrices (as produced by meshgrid)
% bb -- bounding box in R^2 (not tight: a box in ambient space that surely contains the implicit curve and any other isolines)
% fign -- either 0 not to plot or a integer number of the figure to visualize the points 
%
% OUTPU
%
% X - (n x 2)-matrix of points in 2D lying on the isolines
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2017-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

%choose a method for isoline extraction
method = 'contour'; %'hexcontour'; %'fimplicit'; %

%preparation
if length(v) == 1
    w = [v v];
else
    w=v;
end

bbinterval = reshape(bb,[],1)';

switch method
    
    case 'contour' %contouring from values on a Cartesian grid
                
        [Xc,Yc] = meshgrid(bb(1,1):spacing:bb(2,1),bb(1,2):spacing:bb(2,2));
        lsc = lsxy(Xc,Yc);
        
        %make sure 'contour' does not produce a plot 
        h=figure('visible','off');
        %[C,~] = contour(bb(1,1):spacing:bb(2,1),bb(1,2):spacing:bb(2,2))
        [C,~] = contour(Xc,Yc, lsc,w);   %could call contourc instead but it would require vectors defining meshgrid instead of Xc, Yc
        close(h)
        
        %plot the points
        if fign
            figure(fign)
            plot(C(1,3:end),C(2,3:end),'*')
            axis(bbinterval)
            axis equal
        end
        
        %size(C,2)-2
        
        %X=C(:,3:end)';
        [X, levels] = C2X(C);
        
    case 'hexcontour' %mapping so that the image of an integer lattice used for coutouring is a hexagonal mesh
        
        %
        %hexagonal transformation matrix and inverse
        HT = spacing*[1 1/2; 0 sqrt(3)/2]'; %transposed since we multiply point matrices from the right to transform
        HTi = [1 -sqrt(3)/3; 0 2*sqrt(3)/3]'/spacing;
        
        %create bounding box for the integer lattice
        bbHv = [bb; bb(1,1) bb(2,2); bb(2,1) bb(1,2)]*HTi; %vertices of the preimage of bb
        bbH = [floor(min(bbHv)); ceil(max(bbHv))];  %integer bounding box for it
        
        %create level set function values at the integer lattice
        [XH,YH] = meshgrid(bbH(1,1):bbH(2,1),bbH(1,2):bbH(2,2));
        lsH = reshape(ls([XH(:) YH(:)]*HT),size(XH));
        
        
        %make sure 'contour' does not produce a plot
        h=figure('visible','off');
        [C,~] = contour(XH,YH, lsH,w); %contour lines for the integer lattice
        close(h)
        [X, levels]  = C2X(C);
        %uncomment to plot the points in the coordinate system of the integer lattice
        %     plot(X(:,1),X(:,2),'*')
        %     figure(2)
        X = X*HT; %transform points
        if fign           
            plot(X(:,1),X(:,2),'*')
            axis(bbinterval)
            axis equal
        end
        %size(X,1)
        
    case 'fimplicit' % version based on fimplicit: works for each level separately

        MeshDensity = floor((bbinterval(2)-bbinterval(1))/spacing); %mesh density is the same for x and y
        
        X = [];
        levels = []; %[v; nan(size(v))];
        
        %make sure 'fimplicit' does not produce a plot
        h=figure('visible','off');
        for i=1:length(v)
            
            
            fp = fimplicit(@(x,y) lsxy(x,y)-v(i),bbinterval,'MeshDensity',MeshDensity);
            Xnew = [fp.XData', fp.YData']; %see XData, YData and other properties of fp by clicking "Show all properties" after disp(fp)
            Xnew = uniquetol(Xnew,'ByRows',true);
            X = [X; Xnew];
            %Cinfo(2,i) = size(Xnew,1);
            levels = [levels; repmat(v(i),size(Xnew,1),1)]; %levels has the same meaning as in the function C2X
            
        end
        close(h)
        
        if fign
            plot(X(:,1),X(:,2),'*') 
            axis equal
            %axis tight
        end
        
              
end

%% Improve nodes in X by placing them exactly on the level sets by the secant method

%a crude estimate of the gradient of the level set function at each point in X
lsX = ls(X);
sp = spacing^2; %spacing for the gradient estimate and second starting point of the secant method
direct = [ls(X+[sp,0]) - lsX, ls(X+[0,sp]) - lsX]/sp; 

%find direction unit vectors for the search of the intersection points with the level set
%(direction of the gradient if the value of the level set function in a point in X is smaller than the level)
direct = (2*(lsX < levels)-1).*direct./sqrt(sum(direct.^2,2));

%define starting points
X1 = X;
f1 = lsX - levels;
X2 = X + sp*direct;
f2 = ls(X2) - levels;

%run secant iterations
j=1;
while any(abs(f2) > 100*eps) && j <= 20
    toupdate = abs(f2-f1) > eps;
    f1_toupdate = f1(toupdate);
    f2_toupdate = f2(toupdate);
    X(toupdate,:) = (X1(toupdate,:).*f2_toupdate-X2(toupdate,:).*f1_toupdate)./(f2_toupdate-f1_toupdate);
    X1 = X2;
    f1 = f2;
    X2 = X;
    f2 = ls(X) - levels;
    j=j+1;
end

%% remove repetitions in X if any remain 
X = uniquetol(X,'ByRows',true); %WARNING: not sure whether this may change the ordering of points
        


%% Auxiliary functions

function [X, levels] = C2X(C)
%convert the countour matrix C (output of contourc) into point set X
%also returns in levels the level set values of corresponding points in X 
%
% Copyright (C) 2017 Oleg Davydov <oleg.davydov@math.uni-giessen.de>

%number of columns in C
n=size(C,2);

%init levels
%Cinfo = nan(size(C)); %too big, but we remove extra entries at the end
levels =[];

%init X
X=[];

% return empty X if there are no points in the contours
if n<2
    return
end    

i=1;
j=1;
while i<n
    %store Cinfo for this contour curve
    %Cinfo(:,j) = C(:,i);
    
    %number of points corresponding to the current piece of the contour
    m = C(2,i);
    if max(abs(C(:,i+1)-C(:,i+m))) < 100*eps
        X = [X; C(:,i+1:i+m-1)']; %remove repetition for closed curves
        %Cinfo(2,j) = m-1; %update Cinfo
        levels = [levels; repmat(C(1,i),m-1,1)];
    else
        X = [X; C(:,i+1:i+m)'];
        levels = [levels; repmat(C(1,i),m,1)];
    end
    i = i+m+1;
    j = j+1;
end

%remove extroneous entries
%Cinfo(:,j:end) = [];






