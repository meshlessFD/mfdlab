function dst = distsqh(X,Y) 
% Function distsqh: the matrix of halved squares of point distances
%
% Calculates the matrix of halved squares of paiwise point distances 
% for two sets X and Y in R^d. The i,j entry of the output matrix is  || x_i -y_j ||^2 /2.
%
% INPUT
%
% X - (m x d)-matrix of points in R^d
% Y - (n x d)-matrix of points in R^d
%
% OUTPUT
%
% dst - (m x n)-matrix of halved squares of point distances
%
% WARNING: points in X and Y must have the same spacial dimension d
% TODO: add a separate function for the case X=Y since this is frequently needed.
%
%
% INFO
%
% Syntax coincides with Robert Schaback's routine distsqh in [1] R. Schaback, MATLAB Programming for Kernel-Based Methods, technical report, 2011,
% available from http://num.math.uni-goettingen.de/schaback/research/group.html
% The implementation uses the formula || x_i -y_j ||^2=sum_k (x_ik^2 + y_jk^2 - 2x_ik y_jk) which is easy to vectorize, see [1] for details.
%
% See discussion on efficient MATLAB implementation of the distance matrix in [2] G. Fassauer, M. McCourt, Kernel-based Approximation Methods 
% using MATLAB, World Scientific, 2015, Section 4.1.1. 
%
% With Statistics and Machine Learning Toolbox the same result is obtained by dst = pdist2(X,Y,'squaredeuclidean')/2;
% and in the case X=Y by dst = squareform(pdist(X,'squaredeuclidean')/2); 
%
% Code to check the efficiency of different versions:
% n=100; X=rand(n,3);Y=rand(n,3);
% m= 10000; tic, for i=1:m,  distsqh(X,Y); end, toc
%
% COPYRIGHT & LICENSE
% 
% Copyright (C) 2011 Robert Schaback
% Copyright (C) 2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab which is released under GNU General Public License v2.0.
% See the file README for further details.
%

% compute dst 
dst = (sum(X.^2,2)+sum(Y.^2,2)')/2-X*Y'; 
%WARNING: ths line may produce an error on older versions of MATLAB;
%equivalent code which seems to execute somewhat slower:  dst = bsxfun(@plus,sum(X.^2,2),sum(Y.^2,2)')/2-X*Y';

% replace by zeros any negative values of t (possible in the subtraction above due to rounding errors)
dst = max(dst,0); %alternative dst(dst<0)=0; seems a bit slower

