function mat = gradkermatX(X,Y,type,par,ep)
% Function gradkermatX: kernal matrices for two point sets X and Y corresponding to the full gradient wrt. X
%
% Create kernal matrices of gradients of K(x,Y) at all x in X. The result is an (ny x d*nx)-matrix. 
%
% INPUT
%
% X -- an (nx x d)-matrix of nx points in R^d
% Y -- an (ny x d)-matrix of ny points in R^d
% type -- a string defining the type of RBF, see frbf.m for type description
% par -- parameter of the RBF of particular type
% ep -- shape parameter
%
% OUTPUT
%
% mat -- an (ny x d*nx)-matrix of gradients of K(x,Y) at all x in X
%        The columns (i-1)*dx+1:i*dx contain the gradients at the i-th point in X
%
% WARNING: points in X and Y must have the same spacial dimension d
%
%
% INFO
%
% See [1] R. Schaback, MATLAB Programming for Kernel-Based Methods, technical report, 2011,
% available from http://num.math.uni-goettingen.de/schaback/research/group.html
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2011 Robert Schaback
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
% Code derived from Robert Schaback's routine gradkermatX [1], with 3-dimensional array output replaced by a matrix.
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

[nx, d] = size(X);
ny = size(Y,1);
 

fmat = ep^2*frbf(distsqh(ep*X,ep*Y),1,type,par);
      
mat = zeros(nx,ny,d);

for dim = 1:d
    mat(:,:,dim) = fmat.*(repmat(X(:,dim),1,ny)-repmat(Y(:,dim)',nx,1));
end

% The array mat is in the following format:
% mat -- an (nx x ny x d)-array with x-gradients of the kernel evaluated at
%        (x_i,y_j) stored in mat(i,j,:)
%        Comment: mat(:,:,k) is an (nx x ny)-matrix of partial derivatives
%        in the k-th coordinate of x.

% Since this format is incovenient for our applications (see RbfGradientStencil.m), 
% we convert it to the form described in OUTPUTS (OD)
mat = reshape(shiftdim(mat,1),ny,[]); % (ny times dx*nx)-matrix of gradients of K(x,Y) at all x in X
 


 
 
