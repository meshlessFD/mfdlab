function [p,t,niter]=distmesh2d_npf(fd,fh,h0,bbox,pfix,npf,maxiter,use_npf,final_retr,varargin)
% Function distmesh2d_npf: DistMesh 2D mesh generator using signed distance functions
%
% DistMesh is a MATLAB code for generation of unstructured triangular and tetrahedral meshes. 
% It was developed by Per-Olof Persson  and Gilbert Strang.
%
% distmesh2d_npf.m is a modified version of distmesh2d.m v1.1, with additional parameters:
% INPUT
% npf -- nearest point function for the domain boundary (to disable, use a dummy input, e.g. [] and input 0 for use_npf)
% maxiter -- maximum number of iterations (set to inf to disable)
% use_npf -- flag to switch npf on/off (0: off 1: on 2: an experimental code using npf)
% final_retr -- flag whether to do a final retriangulation
% OUTPUT
% niter -- number of iterations
%
% Original syntax of distmesh2d:
%
% [P,T]=DISTMESH2D(FD,FH,H0,BBOX,PFIX,FPARAMS)
% 
% P:         Node positions (Nx2)
% T:         Triangle indices (NTx3)
% FD:        Distance function d(x,y)
% FH:        Scaled edge length function h(x,y)
% H0:        Initial edge length
% BBOX:      Bounding box [xmin,ymin; xmax,ymax]
% PFIX:      Fixed node positions (NFIXx2)
% FPARAMS:   Additional parameters passed to FD and FH
%
% WARNING: the mesh is visualized using the current figure; to control this, 
%          issue a figure command before calling distmesh2d_npf
%
% INFO
%
% distmesh2d.m is part of DistMesh v1.1 developed by Per-Olof Persson and Gilbert Strang, see
% http://persson.berkeley.edu/distmesh/ and Per-Olof Persson and Gilbert
% Strang, "A Simple Mesh Generator in MATLAB," SIAM Review Vol. 46 (2) 2004.
%
% Changes from distmesh2d.m to distmesh2d_npf.m
%    1) copy some auxiliary functions from the DISTMESH package into this file
%    2) additional input npf (nearest point function for the domain boundary)
%       Needed to ensure that the points are placed exactly on the boundary.
%    3) additional input maxiter to terminate the iterations (set to inf to disable)
%    4) additional input final_retr: flag whether to do a final retriangulation
%    5) delaunayn is surrounded by try..catch
%    6) other small changes in the code (indicated by OD marks)
%    7) some | and & replaced by || and && as recommended by MATLAB
%    8) add a changing title to the figures
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2004-2012 Per-Olof Persson. DistMesh v1.1 
% See a copy of DistMesh's COPYRIGHT.TXT at the end of this file for details.
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de> 
% above listed changes from distmesh2d.m to distmesh2d_npf.m
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%OD: added default uniform density (scaled edge length function)
if isempty(fh)
    fh = @huniform;
end

%OD: reset the random number generator
rng(0,'twister') %default at start of MATLAB for 2017b

dptol=.001; ttol=.1; Fscale=1.2; deltat=.2; geps=.001*h0; deps=sqrt(eps)*h0;
densityctrlfreq=30;

% 1. Create initial distribution in bounding box (equilateral triangles)
[x,y]=meshgrid(bbox(1,1):h0:bbox(2,1),bbox(1,2):h0*sqrt(3)/2:bbox(2,2));
x(2:2:end,:)=x(2:2:end,:)+h0/2;                      % Shift even rows
p=[x(:),y(:)];                                       % List of node coordinates

% 2. Remove points outside the region, apply the rejection method
p=p(feval(fd,p,varargin{:})<geps,:);                 % Keep only d<0 points
r0=1./feval(fh,p,varargin{:}).^2;                    % Probability to keep point
p=p(rand(size(p,1),1)<r0./max(r0),:);                % Rejection method
if ~isempty(pfix), p=setdiff(p,pfix,'rows'); end     % Remove duplicated nodes
pfix=unique(pfix,'rows'); nfix=size(pfix,1);
p=[pfix; p];                                         % Prepend fix points
N=size(p,1);                                         % Number of points N

niter=0;
disp(['DistMesh iterations (maxiter=',int2str(maxiter),')'])
pold=inf;                                            % For first iteration
clf,view(2),axis equal,axis off
while niter < maxiter %OD: stop after maxniter iterations to avoid infinite loop
  niter=niter+1;
  %display niter to see what's going on (added by OD)
  if ~isinf(maxiter)
      if ~mod(niter,floor(maxiter/5))
          disp(['  niter= ',int2str(niter)])
          title(['DistMesh iterations:  niter= ',int2str(niter),' out of max ',int2str(maxiter)])
          drawnow
      end
  end
  % 3. Retriangulation by the Delaunay algorithm
  if max(sqrt(sum((p-pold).^2,2))/h0)>ttol           % Any large movement?
    pold=p;                                          % Save current positions
    try
        t=delaunayn(p);                                  % List of triangles
    catch ME
        p=[];
        t=[];
        return
    end
    pmid=(p(t(:,1),:)+p(t(:,2),:)+p(t(:,3),:))/3;    % Compute centroids
    t=t(feval(fd,pmid,varargin{:})<-geps,:);         % Keep interior triangles
    % 4. Describe each bar by a unique pair of nodes
    bars=[t(:,[1,2]);t(:,[1,3]);t(:,[2,3])];         % Interior bars duplicated
    bars=unique(sort(bars,2),'rows');                % Bars as node pairs
    % 5. Graphical output of the current mesh
    cla,patch('vertices',p,'faces',t,'edgecol','k','facecol',[.8,.9,1]);
    drawnow
  end

  % 6. Move mesh points based on bar lengths L and forces F
  barvec=p(bars(:,1),:)-p(bars(:,2),:);              % List of bar vectors
  L=sqrt(sum(barvec.^2,2));                          % L = Bar lengths
  hbars=feval(fh,(p(bars(:,1),:)+p(bars(:,2),:))/2,varargin{:});
  L0=hbars*Fscale*sqrt(sum(L.^2)/sum(hbars.^2));     % L0 = Desired lengths
  
  % Density control - remove points that are too close
  if mod(niter,densityctrlfreq)==0 && any(L0>2*L)
      p(setdiff(reshape(bars(L0>2*L,:),[],1),1:nfix),:)=[];
      N=size(p,1); pold=inf;
      continue;
  end
  
  F=max(L0-L,0);                                     % Bar forces (scalars)
  Fvec=F./L*[1,1].*barvec;                           % Bar forces (x,y components)
  Ftot=full(sparse(bars(:,[1,1,2,2]),ones(size(F))*[1,2,1,2],[Fvec,-Fvec],N,2));
  Ftot(1:size(pfix,1),:)=0;                          % Force = 0 at fixed points
  p=p+deltat*Ftot;                                   % Update node positions

  % 7. Bring outside points back to the boundary
  d=feval(fd,p,varargin{:}); ix=d>0;                 % Find points outside (d>0)

  if use_npf == 1
      %OD: improve projections to make sure the points are on the boundary up to rounding error
      [~,p(ix,:)]=npf(p(ix,:));
      %OD: remove repetitions in p caused by the projection
      [p, ~, J] = uniquetol(p,'ByRows',true);
      p(J(1:nfix),:) = [];%remove fixed points in p (because uniquetol may have changed 
                      %their positions and/or make their values less inaccurate) 
      p = [pfix; p]; %restore the fixed points
      if size(p,1) < N %update N and set pold to inf in case points are removed
          N = size(p,1);
          pold=inf;
      end
  else %original distmesh code 
      dgradx=(feval(fd,[p(ix,1)+deps,p(ix,2)],varargin{:})-d(ix))/deps; % Numerical
      dgrady=(feval(fd,[p(ix,1),p(ix,2)+deps],varargin{:})-d(ix))/deps; %    gradient
      dgrad2=dgradx.^2+dgrady.^2;
      p(ix,:)=p(ix,:)-[d(ix).*dgradx./dgrad2,d(ix).*dgrady./dgrad2];    % Project
      
  end
  % 8. Termination criterion: All interior nodes move less than dptol (scaled)
  if max(sqrt(sum(deltat*Ftot(d<-geps,:).^2,2))/h0)<dptol
      
      %OD: project nodes near the boundary to the boundary
      %WARNING: experimental code, does not work well in the moment
      if use_npf == 2
          %find all points outside or too near to the boundary to become interior nodes
          inear = feval(fd,p,varargin{:}) > -h0;
          %project them to the boundary
          [~,p(inear,:)]=npf(p(inear,:));
          %remove repetitions in p caused by the projection
          p = uniquetol(p,'ByRows',true);
      end
      
      break;
  end
end
%display final number of iterations (OD)
disp(['  final niter= ',int2str(niter)])

% Clean up and plot final mesh 
if final_retr  %OD: a final retrangulation seems to be needed in some cases (this however is not good for non-convex domains)
    try
        t=delaunayn(p);
    catch ME
        p=[];
        t=[];
        return
    end
    pmid=(p(t(:,1),:)+p(t(:,2),:)+p(t(:,3),:))/3;    % Compute centroids
    t=t(feval(fd,pmid,varargin{:})<-geps,:);         % Keep interior triangles
    [p,t]=fixmesh(p,t);                              % Remove duplicated/unused nodes and fix element orientation
    cla,patch('vertices',p,'faces',t,'edgecol','k','facecol',[.8,.9,1]);
    title(['DistMesh iterations:  final niter= ',int2str(niter),' out of max ',int2str(maxiter)])
    drawnow
end
% [p,t]=fixmesh(p,t);
% simpplot(p,t)


%% auxiliary functions from the package DistMesh 

function h=huniform(p,varargin)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

h=ones(size(p,1),1);

function [p,t,pix]=fixmesh(p,t,ptol)
%FIXMESH  Remove duplicated/unused nodes and fix element orientation.
%   [P,T]=FIXMESH(P,T)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

if nargin<3, ptol=1024*eps; end
if nargin>=2 && (isempty(p) || isempty(t)), pix=1:size(p,1); return; end

snap=max(max(p,[],1)-min(p,[],1),[],2)*ptol;
[foo,ix,jx]=unique(round(p/snap)*snap,'rows');
p=p(ix,:);

if nargin>=2
    t=reshape(jx(t),size(t));
    
    [pix,ix1,jx1]=unique(t);
    t=reshape(jx1,size(t));
    p=p(pix,:);
    pix=ix(pix);
    
    if size(t,2)==size(p,2)+1
        flip=simpvol(p,t)<0;
        t(flip,[1,2])=t(flip,[2,1]);
    end
end

function v=simpvol(p,t)
%SIMPVOL Simplex volume.
%   V=SIMPVOL(P,T)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

switch size(p,2)
 case 1
  d12=p(t(:,2),:)-p(t(:,1),:);
  v=d12;
 case 2
  d12=p(t(:,2),:)-p(t(:,1),:);
  d13=p(t(:,3),:)-p(t(:,1),:);
  v=(d12(:,1).*d13(:,2)-d12(:,2).*d13(:,1))/2;
 case 3
  d12=p(t(:,2),:)-p(t(:,1),:);
  d13=p(t(:,3),:)-p(t(:,1),:);
  d14=p(t(:,4),:)-p(t(:,1),:);
  v=dot(cross(d12,d13,2),d14,2)/6;
 otherwise
  v=zeros(size(t,1),1);
  for ii=1:size(t,1)
    A=zeros(size(p,2)+1);
    A(:,1)=1;
    for jj=1:size(p,2)+1
      A(jj,2:end)=p(t(ii,jj),:);
    end
    v(ii)=det(A);
  end
  v=v/factorial(size(p,2));
end

function simpplot(p,t,expr,bcol,icol,nodes,tris)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

dim=size(p,2);
switch dim
 case 2
  if nargin<4 || isempty(bcol), bcol=[.8,.9,1]; end
  if nargin<5 || isempty(icol), icol=[0,0,0]; end
  if nargin<6, nodes=0; end
  if nargin<7, tris=0; end
  
  trimesh(t,p(:,1),p(:,2),0*p(:,1),'facecolor',bcol,'edgecolor','k');
  if nodes==1
    line(p(:,1),p(:,2),'linest','none','marker','.','col',icol,'markers',24);
  elseif nodes==2
    for ip=1:size(p,1)
      txtpars={'fontname','times','fontsize',12};
      text(p(ip,1),p(ip,2),num2str(ip),txtpars{:});
    end
  end
  if tris==2
    for it=1:size(t,1)
      pmid=mean(p(t(it,:),:),1);
      txtpars={'fontname','times','fontsize',12,'horizontala','center'};
      text(pmid(1),pmid(2),num2str(it),txtpars{:});
    end
  end
  view(2)
  axis equal
  axis off
  ax=axis;axis(ax*1.001);
 case 3
  if nargin<4 || isempty(bcol), bcol=[.8,.9,1]; end
  if nargin<5 || isempty(icol), icol=[.9,.8,1]; end
  
  if size(t,2)==4
    tri1=surftri(p,t);
    if nargin>2 && ~isempty(expr)
      incl=find(eval(expr));
      t=t(any(ismember(t,incl),2),:);
      tri1=tri1(any(ismember(tri1,incl),2),:);
      tri2=surftri(p,t);
      tri2=setdiff(tri2,tri1,'rows');
      h=trimesh(tri2,p(:,1),p(:,2),p(:,3));
      set(h,'facecolor',icol,'edgecolor','k');
      hold on
    end
  else
    tri1=t;
    if nargin>2 && ~isempty(expr)
      incl=find(eval(expr));
      tri1=tri1(any(ismember(tri1,incl),2),:);
    end
  end
  h=trimesh(tri1,p(:,1),p(:,2),p(:,3));
  hold off
  set(h,'facecolor',bcol,'edgecolor','k');
  axis equal
  cameramenu
 otherwise
  error('Unimplemented dimension.');
end


function tri=surftri(p,t)
%SURFTRI Find surface triangles from tetrahedra mesh
%   TRI=SURFTRI(P,T)

%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

% Form all faces, non-duplicates are surface triangles
faces=[t(:,[1,2,3]);
       t(:,[1,2,4]);
       t(:,[1,3,4]);
       t(:,[2,3,4])];
node4=[t(:,4);t(:,3);t(:,2);t(:,1)];
faces=sort(faces,2);
[foo,ix,jx]=unique(faces,'rows');
vec=histc(jx,1:max(jx));
qx=find(vec==1);
tri=faces(ix(qx),:);
node4=node4(ix(qx));

% Orientation
v1=p(tri(:,2),:)-p(tri(:,1),:);
v2=p(tri(:,3),:)-p(tri(:,1),:);
v3=p(node4,:)-p(tri(:,1),:);
ix=find(dot(cross(v1,v2,2),v3,2)>0);
tri(ix,[2,3])=tri(ix,[3,2]);


% COPYRIGHT.TXT of  distmesh2d.m v1.1
% DistMesh is a collection of MATLAB functions for generation and
% manipulation of unstructured meshes. DistMesh is Copyright (C) 2004-2012
% Per-Olof Persson.
% 
% This program is free software; you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation; either version 2 of the License, or (at your
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program; if not, write to the Free Software Foundation, Inc.,
% 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
% 
% If you use DistMesh in any program or publication, please acknowledge
% its authors by adding a reference to: Per-Olof Persson and Gilbert
% Strang, "A Simple Mesh Generator in MATLAB," SIAM Review Vol. 46 (2) 2004.

