function rbf = set_rbf(rbftype,porder,dim,ep,par)
% Funktion set_rbf: Generate rbf structure
%
% Store parameters of an RBf kernel in the structure rbf. In particular, set "dummy" parameters and 
% ensure valid ranges in some cases, e.g. that 'Power/TPS' gets porder at least their minimum cpd order.
% Display the info.
%
% INPUT
%
% rbftype -- 'Power/TPS', 'Gauss', 'Gauss-QR', 'Matern', 'MQ', 'IMQ', 'Wendland' as used in script_mFD_Laplace.m
% porder -- the order of the polynomial part
% dim -- the number of space variables
% ep -- shape parameter
% par -- additional parameters for certain rbfs
%
% OUTPUT
%
% rbf - structure to store RBF parameters
%      rbf.type: 'p/tp', 'g', 'ms', 'mq', 'w' as in frbf.m and 'gQR' for Gauss-QR
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2016-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
% Adapted from kermat of [1].
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

%%Examples for rbf structure:
%Matern kernel
%rbf =  struct('type','ms','par',4,'ep',2,'porder',0);
%Gauss kernel direct
%rbf_Laplace =  struct('type','g','par',1,'ep',1,'porder',0);
%Gauss kernel with QR preconditioning (WARNING: works only for d=2 with porder=0)
%rbf =  struct('type','gQR','par',1,'ep',1e-5,'porder',0);
%polyharmonic/tps kernels
%par = 5; porder = floor(par/2)+1; if mod(par,2) type='p'; else type ='tp'; end,  rbf =  struct('type',type,'par',par,'ep',1,'porder',porder);


switch rbftype
    
    case 'Power/TPS'
        rbfpow = par(1);
        %ensure that power/tps get porder at least their minimum cpd order
        if porder < floor(rbfpow/2)+1
            porder = floor(rbfpow/2)+1;
            warning(['porder changed to the minimum cpd order ',int2str(porder)])
        end
        %determine rbftype 'p' or 'tp
        if mod(rbfpow,2)
            type='p';
        else
            type ='tp';
        end
        par(1) = rbfpow;

        disp(['  Polyharmonic/TPS with power=',num2str(rbfpow),' and porder=',int2str(porder)])
        
        %set ep to 1 since it has litle if any influence for 'p/tp'
        if ep ~= 1
            disp('  shape parameter set to 1')
        end        
        ep = 1;
        
    case 'Gauss'
        
        type = 'g';

        disp(['  Gauss with ep=',num2str(ep),' and porder=',int2str(porder)])
        
    case 'Gauss-QR'
        
        type = 'gQR';

        if dim ~= 2
            error('''gQR'' is only available in 2D')
        end
        if porder > 0
            warning(['porder changed to zero since porder=',int2str(porder),' is not available for ''gQR'''])
            porder = 0;
        end
        disp(['  Gauss-QR with ep=',num2str(ep),' and porder=',int2str(porder)])
        
    case 'Matern'
        
        type = 'ms';
        
        disp(['  Matern with  power=',num2str(par(1)),', ep= ',num2str(ep),' and porder=',int2str(porder)])
        
    case 'MQ'
        
        type = 'mq';
        
        rbfpow = par(1);
        %ensure that multiquadric get porder at least their minimum cpd order
        if rbfpow > 1 && porder < floor(rbfpow/2)+1
            porder = floor(rbfpow/2)+1;
            warning(['porder changed to the minimum cpd order ',int2str(porder)])
        end
        
        disp(['  Multiquadric with  power=',num2str(par(1)),', ep= ',num2str(ep),' and porder=',int2str(porder)])
        
    case 'IMQ'
        
        type = 'mq';
              
        disp(['  Inverse multiquadric with  power=',num2str(par(1)),', ep= ',num2str(ep),' and porder=',int2str(porder)])
        
        par(1) = -par(1); %change into parameter of multiquadric 
        
    case 'Wendland'
        
        type = 'w';
        
        disp(['  Wendland with ep=',num2str(ep),', d=',int2str(par(1)),' and k=',int2str(par(2))])
        
    otherwise
        error('unknown rbftype')
end

rbf =  struct('type',type,'par',par,'ep',ep,'porder',porder);
