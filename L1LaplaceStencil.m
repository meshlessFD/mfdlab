function [w, grf, condn, supp] = L1LaplaceStencil(z, X, porder, mu, positive)
% Function L1LaplaceStencil: weights of L1 numerical differentiation for Laplace operator
%
% Compute the weights of (weighted) discrete L1 numerical differentiation formula for Laplace operator at a point z in R^d, 
% exact for polynomials up to order q=order, given function values at X\subset R^d.  If the flag positive in the beginning 
% of the code is set to a non-zero, then we try to compute a minimal positive stencil in the sense of Seibold, that is, with all positive weigths but 
% the one that corresponds to the first point in X. (This is only done if the first point coincides with z.) 
%
% INPUT
%
% z -- a row d-vector representing a point in R^d where the Laplacian is estimated
% X -- an (np x d)-matrix of np points in R^d where function values are known
% porder -- polynomial order (degree+1) (integer >= 1)
% mu -- power of the L1 weights (real number >= 0) 
% positive -- flag to try computing a minimal positive stencil (if positive is set to a non-zero).
%             In this case, if the first point is not z, or the computation of a positive weight vector fails, 
%             then we return w = nan(np,1); supp = (1:np)'; grf = Inf;  condn = Inf;
%
% OUTPUT
%
% w -- an (np x 1)-vector of stencil weights for the Laplacian
% grf -- the  ||.||_{1,mu} growth functions for the Laplacian with order q=order and power mu 
% condn -- condition number of the Vandermonde polynomial matrix that can be used to solve for the weights
% supp -- the support of the weight vector w (column vector of indices into the rows of X)
%
%
% INFO
%
% See L1Stencil.m for further details
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

if porder < 3
    error('polynomial order must be at least 3 for Laplacian')
end

%number of variables
d = size(z,2);

%dimension of polynomials of given order in d variables
polydim = nchoosek(porder+d-1,d);
 
% the vector cz is the rhs for the Laplacian
cz = zeros(polydim,1);
ind = ((d+1)*(d+2) - (1:d).*(2:d+1))/2 + 1;
cz(ind,:) = 2;

%order of the derivative 
dorder = 2;

%run L2Stencil.m
[w, grf, condn, supp] = L1Stencil(z, X, porder, mu, cz, dorder,positive);
 
 
 
