function si  = Select_knearest(i,prob,nodes,k)
% Function Select_knearest: k nearest nodes for the i-th node
%
% Select a set of influence by choosing the indices of k nearest nodes for the i-th node of nodes.X.
% "Invisible" neighbors  are removed for non-convex domains.
%
% INPUT 
%
% i - index of the current node into the rows of nodes.X
% prob - the prob structure
% nodes -- the nodes structure
% k -the target number of neighbors
%
% OUTPUT
%
% si - vector of indices of the set of influence into the rows of nodes.X
%
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2017-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.

%remove invisible neighbors if the domain is non-convex and domain.dpf is available
[neigh,~] = remove_invisible_neighbors(i, prob,nodes); %returns updated neigh 

k = min(k,length(neigh)); %reduce k to the number of neighbors if there are too few of them
si = neigh(1:k); %nodes.neigh(i,1:k);


%remove NaNs that may be in the array in the case of selecting only interior nodes as neighbors, when k is too big
si = si(~isnan(si)); 

