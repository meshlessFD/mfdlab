function [nodes] = generate_inodes_hexgrid(a,spacing,domain)
% Function generate_inodes_hexgrid: interior nodes from a hexagonal grid
%
% Generates interior nodes as a uniform hexagonal grid with grid step 'spacing'.
% WARNING: only 2D
%
% INPUT
%
% a - minimum distance from the boundary for the interior nodes to be created 
% spacing - target grid spacing
% domain - structure describing the domain we generate the nodes for
%
% OUTPUT
%
% nodes -- the nodes structure
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2019-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%read domain dimensionality
if domain.dim ~= 2
    error('hexgrid is only available in 2D')
end


%% generate a hexagonal grid covering the domain

%check that bounding box is available
if isfield(domain,'bb')
    bb = domain.bb; %get bounding box
else
    error('need more info to determine grid size for this domain')
end

% %set hexagonal transformation matrix and inverse
% HT = [1 1/2; 0 sqrt(3)/2]'; %transposed since we multiply point matrices from the right to transform
% HTi = [1 -sqrt(3)/3; 0 2*sqrt(3)/3]';
% 
% %create bounding box for the integer lattice
% bbHv = [bb; bb(1,1) bb(2,2); bb(2,1) bb(1,2)]*HTi; %vertices of the preimage of bb
% bbH = [floor(min(bbHv)/spacing); ceil(max(bbHv)/spacing)];  %integer bounding box for it
% 
% %integer cartesian grid as preimage
% [XH,YH] = meshgrid(bbH(1,1):bbH(2,1),bbH(1,2):bbH(2,2));
% 
% %hex grid by tranformation
% X = spacing*[XH(:) YH(:)]*HT;

%code above replaced by a function (with additional feature that the bottom left corner of bb contains a grid point)
X = hexgrid(bb,spacing);

% actual grid step
h = spacing;


%% Filter out nodes outside the domain 
%(we also remove nodes that are too close to the boundary).

if isfield(domain,'sdf')
    
    %retain as interior only nodes at the distance at least a*h from the boundary (h is the grid stepsize).
    %Some distance is needed at least for RBF methods that are sensitive to the separation distance of nodes.
    %a = .25; %0.01;% 1/4;%
    lind_nodes = domain.sdf(X) < -a*h - 100*eps; %logical indices of grid nodes to be retained (into rows of the full grid)
    X = X(lind_nodes,:);
    
    disp(['  remove nodes with distance from boundary < ',num2str(a),'*h'])
    
else
    
    error('domain.sdf required')
    
end

%indices of interior nodes (all retained grid points)
ii = 1:size(X,1);


%% generate 'nodes' structure
% h - actual grid step
% spacing  - target spacing of the nodes, may be slightly different from h
nodes = struct('itype','hexgrid','spacing',spacing,'h',h,'X',X,'ii',ii);

disp(['interior nodes: hexgrid with step size h=',num2str(h)])

