function y = frbf (t, k, type, par)
% Function frbf: the k-th derivative of the f-form of the standard RBF kernels
%
% Computes the k-th derivative f^(k)(t) of the f-form f(t), t\in R, of several standard RBF kernels.
% The f-form of an RBF kernel K is defined by K(x,y)= f(t), where t=||x-y||^2/2, x,y\in R^d.
% To accomodate for the shape parameter ep, use t=ep^2||x-y||^2/2.
%
% INPUT
%
% t -- array of nonnegative input real values e.g. ||x-x_j||^2/2.
%      If t is an object as determined by isobject(t), then t is assumed a variable precision object
%      and computations are made using VPA with the current number of digits. 
%      Negative values of t (due to rounding errors for example) are replaced by zeros.
% k -- the (integer) order of the derivative (including the function value when k=0)
% type -- a string defining the type of RBF
%           'g'  -- Gaussian kernel exp(-||x-y||^2), vs. exp(-||x-y||^2/2) in original frbf.m (OD)
%           'ms' -- Matern/Sobolev kernel
%           'w'  -- Wendland functions
%           'mq' -- Multiquadric, inverse or not...
%           'p'  -- powers
%           'tp' -- thin-plate spline
% par -- specific parameter(s) of the RBF of particular type (NO shape parameter)
%
% OUTPUT
%
% y -- array of output real values (the same size as t)
%
% 
% INFO
%
% See [1] R. Schaback, MATLAB Programming for Kernel-Based Methods, technical report, 2011,
% available from http://num.math.uni-goettingen.de/schaback/research/group.html
%
% COPYRIGHT & LICENSE
%
% Copyright (C) 2011 Robert Schaback 
% Copyright (C) 2012-2020 Oleg Davydov <oleg.davydov@math.uni-giessen.de>
% Derived from Robert Schaback's routine frbf [1], with slight changes and optional usage of VPA.
%
% This file is part of mFDlab released under GNU General Public License v2.0.
% See the file README for further details.


%%Compute localeps needed in the implementation of some kernels
% which are mathematically defined at the origin by their limit as
% r --> 0+ . As in [1] We just move slightly away form the origin by adding localeps.
%
% NOTE: A more sophisticated approach would be to calculate the local Taylor polynomial 
% around zero and implement it locally. (Robert Schaback [1])
%
if isobject(t) % t is a variable precision object
    vpad = digits();
    localeps= eps*10^(16-vpad);
    vpaf = 1; %flag to use vpa to be passed to subroutines
else           % t  is a double precision variable
    localeps= eps;
    vpaf = 0;
end

%%replace any negative values of t by zeros
t = max(t,0);

%%choose the kernel
switch lower(type)
    
    case('g') %Gaussian (p.d.)
        
        if mod(k,2) == 0
            y = 2^k*exp(-2*t);
        else
            y = -2^k*exp(-2*t);
        end
        
        
        
    case ('mq') % 'mq' = Multiquadric, inverse or not... (1+||x-y||^2)^(par/2), 
        % par ~= 0 (p.d. if par<0, c.p.d. of order floor(par/2)+1 if par>0)
        
        fac=1;
        ord=k;
        while ord>0
            ord=ord-1;
            fac=fac*par;
            par=par-2;
        end
        y=fac*(1+2*t).^(par/2);
        
    case ('p') % powers ||x-y||^par, par>0, not an even integer (c.p.d. of order floor(par/2)+1)
        
        fac=1;
        ord=k;
        while ord>0
            ord=ord-1;
            fac=fac*par;
            par=par-2;
        end
        
        y=fac*(2*t+localeps).^(par/2);
        
    case ('tp') % thin-plate ||x-y||^par *log||x-y||, par >0 (c.p.d. of order floor(par/2)+1)
        
        fac=1;
        ord=k;
        su=0;
        while ord>0
            ord=ord-1;
            if ord==k-1
                su=1;
            else
                su=su*par+fac;
            end
            fac=fac*par;
            par=par-2;
        end
        y=(2*t+localeps).^(par/2);
        y=fac*y.*log(2*t+localeps)/2 +su*y;
        
    case('ms') %Matern kernel K_par(||x-y||)||x-y||^par  (p.d.)
        
        % NOTE: To obtain the standard kernel of the Sobolev space H^s(R^d) for s>0,
        % set par = s - d/2 and divide the resulting y by 2^(s-1)*gamma(s).
        
        % WARNING: If par-k <=0, then the f-form is infinite at the origin.
        % Instead of NaN this function will return some mock value, giving zero if multiplied by zero.
        % The actual behaviour near zero is t^(par-k) if par is fractional and t^(par-k) log(t) for a integer par.
        % Therefore the expressions like t^a*frbf (t, k, type, par) are correctly evaluated by this function
        % to zero at the origin if a > k - par. 
        
        % WARNING: for a small par-k > 0 the evaluation near the origin is inaccurate (less than double precision)
                
        x = sqrt(2*t) + localeps; %localeps is needed because besselk is infinite at the origin
        
        if mod(k,2) == 0
            y = (x.^(par-k)).*besselk(par-k,x);
        else
            y = -(x.^(par-k)).*besselk(par-k,x);
        end
         
        
    case('w') %Wendland functions phi_{par(1),par(2)}, smoothness C^{2*par(2)} (p.d.)
        %WARNING: positive definite in R^d with d <= par(1) only, when par(1) is odd.
        %         When p1=par(1) is even, then the same function is given by par(1)=p1+1       
        %WARNING: functions are scaled with a nonstandard coefficient
       

        %compute the coefficients of the polynomial part of Wendland fuction
        [coeff, expon] = wendcoeff(par(1) + 2*k, par(2) - k, vpaf); %for higher spatial dimensions, choose par(1)\ge d
        
        %evaluate the r argument of the Wendland function.
        r = sqrt(2*t);
        if isobject(t)
            ind = find (abs(1-r) == 1-r);
            u = sym(zeros(size(ind)),'d');
            sp = sym(ones(size(ind)),'d');
            y = sym(zeros(size(t)),'d');
        else
            ind = find (r<1);
            u = zeros(size(ind));
            sp = ones(size(ind));
            y = zeros(size(t));
        end
        sloc = r(ind);
        
        %evaluate for r<1 the polynomial part u = coeff(1) + coeff(2)*r + coeff(3)*r^2 + ... 
        %of Wendland function
        for i=1:length(coeff)
            u = u + coeff(i)*sp;
            sp = sp.*sloc;
        end
        
        %evaluate y by multiplying u by (1-r)^expon
        if mod(k,2) == 0
            y(ind) = u.*(1 - sloc).^expon;
        else
            y(ind) = -u.*(1 - sloc).^expon;
        end        
    otherwise
        error('RBF type not implemented')
        
end

%% %%%%%%%%%%%%%% auxiliary routines %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [coeff, expon] = wendcoeff(d,k,vpaf)
% calculates coefficients and exponent for polynomial part p_{d,k}(r)
% of Wendland function phi_{d,k}(r) = p_{d,k}(r)*(1-r)^expon
% vpaf  - VPA flag (0 or 1)

expon = floor(d/2) + k + 1;


if vpaf == 1
    coeff(1,1) = sym(1);
    for n = 0:k-1
        
        coeff(n+2,1) = sym(coeff(n+1,1)/(n + expon + 2));
        
        for j = n+1:-1:2
            coeff(j,1) = sym((j*coeff(j+1,1) + coeff(j-1,1))/(expon+j));
        end
        
        expon = expon + 1;
        coeff(1,1) = sym(coeff(2,1)/expon);
        
    end
else
    
    coeff(1,1) = 1;
    for n = 0:k-1
        
        coeff(n+2,1) = coeff(n+1,1)/(n + expon + 2);
        
        for j = n+1:-1:2
            coeff(j,1) = (j*coeff(j+1,1) + coeff(j-1,1))/(expon+j);
        end
        
        expon = expon + 1;
        coeff(1,1) = coeff(2,1)/expon;
        
    end
end
